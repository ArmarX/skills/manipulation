/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_manipulation
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       11.08.22
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/libraries/armem/client/util/SimpleWriterBase.h>
#include <armarx/manipulation/core/forward_declarations.h>
#include <armarx/manipulation/meta/is_stl_container.h>

namespace armarx::manipulation::memory::client::core
{
    template <typename BusinessObj>
    struct WriterImplementation;

    template <typename BusinessObj>
    class Writer : virtual public armem::client::util::SimpleWriterBase
    {
        using ImplT = WriterImplementation<BusinessObj>;

    public:
        using armem::client::util::SimpleWriterBase::SimpleWriterBase;

        bool store(const BusinessObj& bo, const std::string& clientID,
                   const std::optional<std::string>& entityName = std::nullopt)
        {
            armem::Commit commit;

            auto update = makeUpdate(bo, clientID, entityName);
            commit.add(update);
            armem::CommitResult updateResult;
            {
                std::lock_guard g{memoryWriterMutex()};
                updateResult = memoryWriter().commit(commit);
            }
            ARMARX_DEBUG << updateResult;

            if (not updateResult.allSuccess())
            {
                ARMARX_ERROR << updateResult.allErrorMessages();
            }
            return updateResult.allSuccess();
        }

        template <typename Container>
        typename std::enable_if<meta::is_stl_container<Container>::value, bool>::type
        store(const Container& c, const std::string& clientID)
        {
            armem::Commit commit;


            for (const auto& entry: c)
            {
                auto update = makeUpdate(entry, clientID);
                commit.add(update);
            }

            armem::CommitResult updateResult;
            {
                std::lock_guard g{memoryWriterMutex()};
                updateResult = memoryWriter().commit(commit);
            }
            ARMARX_DEBUG << updateResult;

            if (not updateResult.allSuccess())
            {
                ARMARX_ERROR << updateResult.allErrorMessages();
            }
            return updateResult.allSuccess();
        }

        std::string propertyPrefix() const override
        {
            return std::string(ImplT::propertyPrefix);
        }

        Properties defaultProperties() const override
        {
            return {.memoryName = std::string(ImplT::memoryName), .coreSegmentName = std::string(
                    ImplT::coreSegmentName), .providerName = std::string(ImplT::providerName)};
        }

    private:
        armem::EntityUpdate makeUpdate(const BusinessObj& bo, const std::string& clientID,
                                       const std::optional<std::string>& entityName = std::nullopt)
        {
            return ImplT::makeUpdate(bo, clientID, entityName);
        }
    };
}