/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_manipulation
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       24.02.22
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <armarx/manipulation/client/core/Writer.h>


namespace armarx::manipulation::memory::client
{
    namespace core
    {
        template <>
        struct WriterImplementation<armarx::manipulation::core::ExecutableAction>
        {
            static constexpr std::string_view propertyPrefix = "mem.manip.executable_action.";
            static constexpr std::string_view memoryName = "Manipulation";
            static constexpr std::string_view coreSegmentName = "Actions_Executable";
            static constexpr std::string_view providerName = "";

            static armem::EntityUpdate
            makeUpdate(const armarx::manipulation::core::ExecutableAction& bo, const std::string& clientID,
                       const std::optional<std::string>& entityName);
        };

        extern template
        struct WriterImplementation<armarx::manipulation::core::ExecutableAction>;

        extern template
        class Writer<armarx::manipulation::core::ExecutableAction>;
    }

    namespace executable_action
    {
        using Writer = core::Writer<armarx::manipulation::core::ExecutableAction>;
    }

}
