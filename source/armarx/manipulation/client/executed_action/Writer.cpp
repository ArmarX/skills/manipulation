/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_manipulation
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       24.02.22
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <IceUtil/Time.h>
#include <armarx/manipulation/core/ExecutedAction.h>
#include <armarx/manipulation/core/aron_conversions.h>
#include <armarx/manipulation/core/aron/ExecutedAction.aron.generated.h>

#include "Writer.h"

namespace armarx::manipulation::memory::client::core
{
    armem::EntityUpdate WriterImplementation<armarx::manipulation::core::ExecutedAction>::makeUpdate(
            const manipulation::core::ExecutedAction& bo, const std::string& clientID,
            const std::optional<std::string>& entityName)
    {
        armem::Time ts = *bo.endTime;
        armarx::armem::EntityUpdate update;
        update.entityID = armem::MemoryID().withMemoryName(std::string(memoryName))
                                           .withCoreSegmentName(std::string(coreSegmentName))
                                           .withProviderSegmentName(clientID).withEntityName(
                        entityName.has_value() ? entityName.value() : "ExecutedAction").withTimestamp(ts);
        update.timeCreated = *bo.startTime;
        auto dto = toAron(bo);
        update.instancesData = {dto.toAron()};
        return update;
    }

    template
    struct WriterImplementation<armarx::manipulation::core::ExecutedAction>;

    template
    class Writer<armarx::manipulation::core::ExecutedAction>;
}