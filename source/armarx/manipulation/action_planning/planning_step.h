/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_manipulation
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       02.05.22
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <vector>
#include <armarx/manipulation/core/forward_declarations.h>
#include <armarx/manipulation/core/ExecutableAction.h>
#include <armarx/manipulation/meta/transform_container.h>
#include <armarx/manipulation/action_planning/ExecutableActionFactory.h>

#include "tree.h"

namespace armarx::manipulation::action_planning
{
    struct planning_step_interface
    {
        virtual void plan(std::vector<core::ExecutableAction>& actions);

        virtual void plan(core::ExecutableAction& action);

        virtual void plan(core::ExecutableAction::Common& common);

        virtual void plan(core::ExecutableAction::Unimanual& actions, const core::ExecutableAction::Common& common);

        virtual ~planning_step_interface() = default;
    };

    class planning_pipeline : public tree<std::unique_ptr<planning_step_interface>>
    {
        using BaseT = tree<std::unique_ptr<planning_step_interface>>;
    public:
        std::vector<core::ExecutableAction> plan(const std::vector<core::ActionHypothesis>& hypotheses);

        // planning_step_interface
        std::vector<core::ExecutableAction> plan(const std::vector<core::ExecutableAction>& actions);

        std::vector<core::ExecutableAction> plan(std::vector<core::ExecutableAction>&& actions);

        template <typename PlanningStepT, typename IterT, typename... Args>
        IterT append_child(IterT iter, Args&&... args)
        {
            return BaseT::append_child(iter, std::make_unique<PlanningStepT>(std::forward<Args>(args)...));
        }
        using BaseT::append_child;

        template <typename PlanningStepT, typename... Args>
        pre_order_iterator set_head(Args&&... args)
        {
            return BaseT::set_head(std::make_unique<PlanningStepT>(std::forward<Args>(args)...));
        }
        using BaseT::set_head;
    };
}