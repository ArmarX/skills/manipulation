/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_manipulation
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       07.07.22
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ExecutableActionFactory.h"

namespace armarx::manipulation::action_planning
{


    ExecutableActionFactory::ExecutableActionFactory(
            Factory<ExecutableActionFactory, std::string_view, const core::ActionHypothesis*>::Key)
    {
    }

    template <KnownAffordances AffordanceT>
    core::ExecutableAction ExecutableActionProduct<AffordanceT>::buildAction()
    {
        return core::ExecutableAction{{*hypothesis}, {}};
    }

    template <KnownAffordances AffordanceT>
    ExecutableActionProduct<AffordanceT>::ExecutableActionProduct(const core::ActionHypothesis* h) :
            RegistrarT(typename RegistrarT::Registration()),
            hypothesis(std::experimental::make_observer(h))
    {}

    template struct ExecutableActionProduct<KnownAffordances::Grasp>;
    template struct ExecutableActionProduct<KnownAffordances::Push>;
    template struct ExecutableActionProduct<KnownAffordances::Place>;
    template struct ExecutableActionProduct<KnownAffordances::Pull>;
}