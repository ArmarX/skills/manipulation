/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_manipulation
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       07.07.22
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <experimental/memory>
#include <ArmarXCore/util/CPPUtility/SelfRegisteringFactory.h>

#include <armarx/manipulation/core/ExecutableAction.h>
#include <armarx/manipulation/core/AffordanceTypes.h>


namespace armarx::manipulation::action_planning
{
    using KnownAffordances = armarx::manipulation::core::arondto::KnownAffordances::ImplEnum;

    // This could be done way easier at the moment. But in case the Affordance is treated as blueprint, this might change
    struct ExecutableActionFactory : Factory<ExecutableActionFactory, std::string_view,
                                                   const core::ActionHypothesis*>
    {
        using BaseT = Factory<ExecutableActionFactory, std::string_view, const core::ActionHypothesis*>;

        static auto make(const core::ActionHypothesis& h)
        {
            return BaseT::make(h.getAffordanceType().toString(), &h)->buildAction();
        }
        virtual core::ExecutableAction buildAction() = 0;

        explicit ExecutableActionFactory(Key);
    };

    template <KnownAffordances AffordanceT>
    struct ExecutableActionProduct : ExecutableActionFactory::Registrar<ExecutableActionProduct<AffordanceT>>
    {
        using RegistrarT = ExecutableActionFactory::Registrar<ExecutableActionProduct<AffordanceT>>;
        static constexpr std::string_view id = core::detail::toString<AffordanceT>::value;
        explicit ExecutableActionProduct(const core::ActionHypothesis* h);
        core::ExecutableAction buildAction() override;
        std::experimental::observer_ptr<const core::ActionHypothesis> hypothesis;
    };

    extern template
    struct ExecutableActionProduct<KnownAffordances::Grasp>;
    extern template
    struct ExecutableActionProduct<KnownAffordances::Place>;
    extern template
    struct ExecutableActionProduct<KnownAffordances::Pull>;
    extern template
    struct ExecutableActionProduct<KnownAffordances::Push>;
}