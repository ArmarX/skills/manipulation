/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_manipulation
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       02.05.22
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// test includes and other stuff
#define BOOST_TEST_MODULE Manipulation::ArmarXLibraries::planning
#define ARMARX_BOOST_TEST

#include <armarx/manipulation/Test.h>
#include <armarx/manipulation/action_planning/planning_step.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include <ArmarXCore/core/PackagePath.h>
#include <armarx/manipulation/core/Affordance.h>
#include <armarx/manipulation/core/FramedUncertainPose.h>
#include <RobotAPI/libraries/armem_robot/types.h>
#include <armarx/manipulation/core/aron_conversions.h>

struct cout_redirect {
    // https://stackoverflow.com/questions/5405016/can-i-check-my-programs-output-with-boost-test
    cout_redirect( std::streambuf * new_buffer )
            : old( std::cout.rdbuf( new_buffer ) )
    { }

    ~cout_redirect( ) {
        std::cout.rdbuf( old );
    }

private:
    std::streambuf * old;
};


namespace armarx::manipulation::action_planning
{
    class plan_head : virtual public planning_step_interface {};
    class plan_unimanual : virtual public planning_step_interface
    {
        void plan(core::ExecutableAction::Unimanual& actions, const core::ExecutableAction::Common& common) override
        {
            std::cout << "1";
        }
    };

    class plan_common : virtual public planning_step_interface
    {
        void plan(core::ExecutableAction::Common& common) override
        {
            std::cout << "2";
        }
    };

    class plan_single : virtual public planning_step_interface
    {
        void plan(core::ExecutableAction& action) override
        {
            std::cout << "3";
        }
    };

    class plan_vector : virtual public planning_step_interface
    {
        void plan(std::vector<core::ExecutableAction>& action) override
        {
            std::cout << "4";
        }
    };

    class plan_remove : virtual public planning_step_interface
    {
        void plan(std::vector<core::ExecutableAction>& action) override
        {
            int toRemove = -1;
            for (auto it = action.begin(); it != action.end(); )
            {
                if (toRemove == 0)
                {
                    it = action.erase(it);
                    std::cout << "r";
                } else
                {
                    std::cout << "k";
                    plan(*it);
                    it++;
                }
                toRemove++;
            }
        }

        void plan(core::ExecutableAction& action) override
        {
            std::cout << "5";
        }
    };
}

BOOST_AUTO_TEST_CASE(executable_action_builder)
{
    using namespace armarx::manipulation::action_planning;
    using namespace armarx::manipulation::core;


    armarx::CMakePackageFinder project("armar6_rt");
    armarx::PackagePath xml_robot_file_path{project.getName(), "robotmodel/Armar6-SH/Armar6-SH.xml"};
    armarx::armem::robot::RobotDescription robot_description{"Armar6", xml_robot_file_path};
    std::string action_type = "Grasp";
    Affordance affordance(action_type, robot_description);
    armarx::FramedPose pose(Eigen::Matrix4f::Identity(), armarx::GlobalFrame, "Armar6");
    FramedUncertainPose fp_w_cov(pose, Eigen::Matrix6f::Identity());
    ActionHypothesis hypothesis(affordance, fp_w_cov, 0);

    auto b2 = ExecutableActionFactory::make(hypothesis);
    BOOST_CHECK(toAron(b2.unimanual.begin()->hypothesis.value()) == toAron(hypothesis));
}

BOOST_AUTO_TEST_CASE(planning_pipeline_test_actions)
{
    using namespace armarx::manipulation::action_planning;
    using namespace armarx::manipulation::core;

    armarx::CMakePackageFinder project("armar6_rt");
    armarx::PackagePath xml_robot_file_path{project.getName(), "robotmodel/Armar6-SH/Armar6-SH.xml"};
    armarx::armem::robot::RobotDescription robot_description{"Armar6", xml_robot_file_path};
    std::string action_type = "Grasp";
    Affordance affordance(action_type, robot_description);
    armarx::FramedPose pose(Eigen::Matrix4f::Identity(), armarx::GlobalFrame, "Armar6");
    FramedUncertainPose fp_w_cov(pose, Eigen::Matrix6f::Identity());
    ActionHypothesis hypothesis(affordance, fp_w_cov, 0);
    ActionHypothesis hypothesis2(affordance, fp_w_cov, 100, arondto::Handedness::Left);


    std::vector<ExecutableAction> actions;
    actions.emplace_back(ExecutableAction::Unimanual{}, ExecutableAction::Common{});
    actions.emplace_back(ExecutableAction::Unimanual{}, ExecutableAction::Common{});
    actions.emplace_back(ExecutableAction::Unimanual{}, ExecutableAction::Common{});

    std::vector<ActionHypothesis> hypotheses;
    hypotheses.emplace_back(affordance, fp_w_cov, 0);
    hypotheses.emplace_back(affordance, fp_w_cov, 100, arondto::Handedness::Left);
    hypotheses.emplace_back(Affordance("Push", robot_description), fp_w_cov, 0);


    auto check = [](const auto& input) -> void
    {
        planning_pipeline pipeline;
        auto head = pipeline.set_head<plan_head>();
        auto um = pipeline.append_child<plan_unimanual>(head);
        auto common = pipeline.append_child<plan_common>(head);
        pipeline.append_child<plan_single>(head);
        pipeline.append_child<plan_vector>(head);
        boost::test_tools::output_test_stream output;
        {
            cout_redirect guard(output.rdbuf());
            pipeline.plan(input);
        }
        BOOST_CHECK(output.is_equal("1112223334"));

        pipeline.append_child<plan_single>(um);
        output.clear();
        {
            cout_redirect guard(output.rdbuf());
            pipeline.plan(input);
        }
        BOOST_CHECK(output.is_equal("1113332223334"));
        pipeline.append_child<plan_vector>(common);
        pipeline.append_child<plan_unimanual>(common);
        output.clear();
        {
            cout_redirect guard(output.rdbuf());
            pipeline.plan(input);
        }
        BOOST_CHECK(output.is_equal("11133322241113334"));

        pipeline.append_child(head, std::make_unique<plan_unimanual>());
        output.clear();
        {
            cout_redirect guard(output.rdbuf());
            pipeline.plan(input);
        }
        BOOST_CHECK(output.is_equal("11133322241113334111"));

        pipeline.append_child(head, std::make_unique<plan_remove>());
        std::vector<armarx::manipulation::core::ExecutableAction> result;
        output.clear();
        {
            cout_redirect guard(output.rdbuf());
            result = pipeline.plan(input);

        }
        BOOST_CHECK_EQUAL(result.size(), 2);
        BOOST_CHECK_EQUAL(input.size(), 3);
        BOOST_CHECK(output.is_equal("11133322241113334111k5rk5"));
    };

    check(actions);
    check(hypotheses);

}

