/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_manipulation
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       02.05.22
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "planning_step.h"

namespace armarx::manipulation::action_planning
{
    std::vector<core::ExecutableAction> planning_pipeline::plan(
            const std::vector<core::ActionHypothesis>& hypotheses)
    {

        auto local_actions = meta::transform_container(hypotheses, [](const auto& h)
        {
            return ExecutableActionFactory::make(h);
        });
        return plan(std::move(local_actions));
    }

    std::vector<core::ExecutableAction> planning_pipeline::plan(const std::vector<core::ExecutableAction>& actions)
    {
        // TODO: does copying this make sense here? needs to be if it should be called from within plan(hypotheses)
        auto local_actions = actions;
        return plan(std::move(local_actions));
    }

    std::vector<core::ExecutableAction> planning_pipeline::plan(std::vector<core::ExecutableAction>&& actions)
    {
        // TODO: does copying this make sense here? needs to be if it should be called from within plan(hypotheses)
        for (auto it = begin(); it != end(); it++)
        {
            (*it)->plan(actions);
        }
        return actions;
    }

    void planning_step_interface::plan(std::vector<core::ExecutableAction>& actions)
    {
        for (auto& a: actions)
        {
            plan(a);
        }
    }

    void planning_step_interface::plan(core::ExecutableAction& action)
    {
        plan(action.common);
        for (auto& um : action.unimanual)
        {
            plan(um, action.common);
        }
    }

    void planning_step_interface::plan(core::ExecutableAction::Common& common)
    {
        // implement in child class
    }

    void planning_step_interface::plan(core::ExecutableAction::Unimanual& actions,
                                       const core::ExecutableAction::Common& common)
    {
        // implement in child class
    }
}
