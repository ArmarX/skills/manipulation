#pragma once

#include <RobotAPI/interface/aron.ice>
#include <RobotAPI/interface/armem/client/MemoryListenerInterface.ice>

module armarx
{
    module manipulation
    {
        module action_execution
        {
            interface ActionExecutorInterface extends armarx::armem::client::MemoryListenerInterface
            {
                armarx::aron::data::dto::Dict executeAction(armarx::aron::data::dto::Dict executableAction);
                void recover();
                void abort();
            }
        }
    }
}
