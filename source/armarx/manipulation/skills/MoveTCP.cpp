#include "MoveTCP.h"

#include <chrono>
#include <memory>
#include <thread>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <VirtualRobot/MathTools.h>

#include <armarx/manipulation/core/arm.h>
#include <armarx/manipulation/core/aron_conversions.h>
#include <armarx/manipulation/core/types.h>
#include <armarx/manipulation/skills/aron/GraspHandleParams.aron.generated.h>
#include <armarx/manipulation/skills/config.h>


namespace armarx::manipulation::skills
{
    MoveTCP::MoveTCP(const Context& c) :
        armarx::skills::SpecializedSkill<arondto::MoveTCPParams>(DefaultSkillDescription()),
        ctx(c)
    {
    }


    bool
    MoveTCP::initialize(const arondto::MoveTCPParams& params)
    {
        ARMARX_INFO << "initialize";

        core::Hand hand{};
        fromAron(params.hand, hand);

        const auto armRns = ctx.robot.arm(core::getArm(hand));

        armarx::NJointCartesianVelocityControllerWithRampConfigPtr config =
            new armarx::NJointCartesianVelocityControllerWithRampConfig(
                armRns->getName(),
                armRns->getTCP()->getName(),
                armarx::CartesianSelectionMode::eAll,
                500,
                1,
                2,
                1,
                2);

        velocityController.reset(new armarx::VelocityControllerHelper(
            ctx.robot.robotUnit(), config, "VelocityController"));

        positionController.reset(new armarx::PositionControllerHelper(
            armRns->getTCP(), velocityController, armRns->getTCP()->getPoseInRootFrame()));


        return true;
    }


    armarx::skills::Skill::MainResult
    MoveTCP::main(const SpecializedMainInput& in)
    {

        ARMARX_INFO << "initialize";
        initialize(in.params);

        // move to desired pose
        draw(Pose(in.params.robot_T_tcp_desired));

        ARMARX_INFO << "setting target";
        positionController->setTarget(in.params.robot_T_tcp_desired);
        positionController->update();

        // parameterize position controller (thresholds)
        positionController->thresholdPositionReached = 1; // [mm]
        positionController->thresholdOrientationReached = VirtualRobot::MathTools::deg2rad(1);

        ARMARX_INFO << "Waiting until target is reached";

        // run controller
        while (not positionController->isFinalTargetReached())
        {
            ARMARX_CHECK(ctx.robot.synchronize());
            positionController->update();

            // FIXME
            armarx::Clock::WaitFor(armarx::Duration::MilliSeconds(10));
        }

        ARMARX_INFO << "Goal reached";

        stop();

        return {.status = armarx::skills::TerminatedSkillStatus::Succeeded};
    }

    void
    MoveTCP::draw(const core::Pose& robot_T_tcp_desired)
    {
        const core::Pose global_T_robot(ctx.robot.robot()->getGlobalPose());

        auto layer = ctx.arviz.layer("skills.move_tcp");

        layer.add(armarx::viz::Sphere("tcp_target")
                      .position((global_T_robot * robot_T_tcp_desired).translation())
                      .radius(parameters.sphereVisuRadius)
                      .color(0, 0, 255, parameters.visuAlpha));

        layer.add(armarx::viz::Pose("tcp_target_pose")
                      .pose(global_T_robot * robot_T_tcp_desired)
                      .color(0, 0, 255, parameters.visuAlpha));

        // layer.add(armarx::viz::Arrow("movement")
        //               .fromTo(ctx.tcp->getGlobalPosition(),
        //                       (global_T_robot * robot_T_tcp_desired).translation())
        //               .width(params.arrowVisuWidth));

        ctx.arviz.commit(layer);
    }

    bool
    MoveTCP::stop()
    {
        ARMARX_INFO << "MoveTCP::stop";

        positionController->immediateHardStop();
        velocityController->cleanup();

        return true;
    }


} // namespace armarx::manipulation::skills
