/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <VirtualRobot/VirtualRobot.h>

#include "RobotAPI/libraries/skills/provider/SpecializedSkill.h"

#include <armarx/manipulation/core/Robot.h>
#include <armarx/manipulation/skills/aron/GraspHandleParams.aron.generated.h>

namespace armarx::manipulation::skills
{
    class GraspHandle : public armarx::skills::SpecializedSkill<arondto::GraspHandleParams>
    {
    public:
        struct Context
        {
            Robot& robot;
        };

        GraspHandle(const Context& c);

    private:
        armarx::skills::Skill::MainResult main(const SpecializedMainInput& in) final;

    public:
        static armarx::skills::SkillDescription DefaultSkillDescription()
        {
            return {"GraspHandle", "", {}, armarx::core::time::Duration::MilliSeconds(999999), // ??
                    ParamType::ToAronType()};
        }

    private:
        void activateImpedanceControl(const arondto::GraspHandleParams&);

        Context ctx;
    };

} // namespace armarx::manipulation::skills
