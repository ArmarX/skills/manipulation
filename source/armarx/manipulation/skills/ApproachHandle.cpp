#include "ApproachHandle.h"

#include <memory>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <IceUtil/Time.h>

#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <ArmarXCore/util/CPPUtility/trace.h>

#include "RobotAPI/libraries/skills/provider/PeriodicSpecializedSkill.h"
#include <RobotAPI/components/ArViz/Client/Elements.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/VelocityControllerHelper.h>
#include <RobotAPI/libraries/core/CartesianVelocityController.h>
#include <RobotAPI/libraries/core/CartesianWaypointController.h>
#include <RobotAPI/libraries/core/FramedPose.h>

#include "armarx/manipulation/core/arm.h"
#include "armarx/manipulation/geometric_planning/ArticulatedObjectDoorHelper.h"
#include <armarx/manipulation/core/aron_conversions.h>

namespace armarx::manipulation::skills
{
    ApproachHandle::ApproachHandle(const ApproachHandle::Context& ctx) :
            Base(DefaultSkillDescription(), armarx::Frequency(armarx::Duration::MilliSeconds(20))),
            ctx(ctx)
    {
    }

    ApproachHandle::~ApproachHandle() = default;


    armarx::skills::PeriodicSkill::StepResult
    ApproachHandle::step(const SpecializedMainInput& in)
    {
        const Eigen::Vector3f robot_V_tcp_handle = approachDirection(in.params);

        Eigen::Vector6f velocity;
        velocity << robot_V_tcp_handle.x(), robot_V_tcp_handle.y(), robot_V_tcp_handle.z(), 0, 0, 0;

        velocityController->setTargetVelocity(in.params.approachVelocity * velocity);

        draw(in.params);

        if (finished(in.params))
        {
            return {.status = armarx::skills::ActiveOrTerminatedSkillStatus::Succeeded};
        }

        return {.status = armarx::skills::ActiveOrTerminatedSkillStatus::Running};
    }

    armarx::skills::Skill::ExitResult ApproachHandle::exit(const SpecializedExitInput& in)
    {        
        stop();
        return {.status = armarx::skills::TerminatedSkillStatus::Succeeded};
    }

    armarx::skills::Skill::InitResult ApproachHandle::init(const SpecializedInitInput& in)
    {
        ARMARX_INFO << "ApproachHandle::initialize";

        const std::string controllerName = "ApproachHandleController";

        core::Hand hand{};
        fromAron(in.params.hand, hand);

        const auto armRns = ctx.robot.arm(core::getArm(hand));

        velocityController = std::make_shared<armarx::VelocityControllerHelper>(ctx.robot.robotUnit(),
                                                                                armRns->getName(), controllerName);

        VirtualRobot::RobotPtr object; // FIXME
        geometric_planning::ArticulatedObjectDoorHelper::Params doorHelperParams;
        geometric_planning::ArticulatedObjectDoorHelper doorHelper(object, doorHelperParams);

        interactionInfo = doorHelper
                .planInteractionExtended(in.params.objectNodeSet, Pose(armRns->getTCP()->getGlobalPose()));

        draw(in.params);

        return {.status = armarx::skills::TerminatedSkillStatus::Succeeded};
    }

    void ApproachHandle::draw(const ApproachHandle::ParamsT& params)
    {
        core::Hand hand{};
        fromAron(params.hand, hand);

        const Pose global_T_tcp_at_handle = Pose(interactionInfo.rns.handleSurfaceProjection->getGlobalPose()) *
                                            Pose(interactionInfo.handleSurfaceProjection_T_tcp_at_handle);

        const Eigen::Vector3f global_P_tcp_at_handle = global_T_tcp_at_handle.translation();
        const Eigen::Vector3f global_P_tcp = ctx.robot.arm(core::getArm(hand))->getTCP()->getGlobalPosition();

        auto layer = ctx.arviz.layer("skills.approach_handle");

        layer.add(armarx::viz::Sphere("tcp").position(global_P_tcp).radius(parameters.sphereVisuRadius)
                                            .color(255, 0, 0, parameters.visuAlpha));

        layer.add(armarx::viz::Sphere("tcp_at_handle").position(global_P_tcp_at_handle)
                                                      .radius(parameters.sphereVisuRadius)
                                                      .color(0, 255, 0, parameters.visuAlpha));

        layer.add(armarx::viz::Arrow("movement_direction").fromTo(global_P_tcp, global_P_tcp + approachVector(params))
                                                          .width(parameters.arrowVisuWidth));

        ctx.arviz.commit(layer);
    }

    Eigen::Vector3f ApproachHandle::approachVector(const ApproachHandle::ParamsT& params)
    {
        core::Hand hand{};
        fromAron(params.hand, hand);

        const Pose global_T_tcp_at_handle = Pose(interactionInfo.rns.handleSurfaceProjection->getGlobalPose()) *
                                            Pose(interactionInfo.handleSurfaceProjection_T_tcp_at_handle);

        // TODO apply offset in tcp_at_handle frame ... => y direction...
        const Eigen::Vector3f global_P_tcp_at_handle =
                global_T_tcp_at_handle.translation() + Eigen::Vector3f::UnitZ() * 100;

        const Eigen::Vector3f global_P_tcp = ctx.robot.arm(getArm(hand))->getTCP()->getGlobalPosition();
        const Eigen::Vector3f global_V_tcp_handle = (global_P_tcp_at_handle - global_P_tcp);

        // -> tcp frame
        armarx::FramedDirection framedDirection(global_V_tcp_handle, armarx::GlobalFrame, ctx.robot.name());

        Eigen::Vector3f robot_V_tcp_handle = framedDirection.toRootFrame(ctx.robot.robot())->toEigen();

        return robot_V_tcp_handle;
    }

    Eigen::Vector3f ApproachHandle::approachDirection(const ApproachHandle::ParamsT& params)
    {
        return approachVector(params).normalized();
    }

    bool ApproachHandle::stop()
    {
        ARMARX_INFO << "ApproachHandle::initialize";

        velocityController->cleanup();

        return true;
    }

    bool ApproachHandle::finished(const ApproachHandle::ParamsT& params)
    {
        // Force limit

        core::Hand hand{};
        fromAron(params.hand, hand);

        const auto ft = ctx.robot.getForceTorqueInHand(hand);
        ARMARX_CHECK(ft.has_value()) << "No force/torque data available for hand " << core::HandNames.to_name(hand);

        const armarx::FramedDirection framedForce = ft->force;

        const Eigen::Vector3f global_V_force = framedForce.toGlobalEigen(ctx.robot.robot());
        const Eigen::Vector3f global_V_approach_dir = approachDirection(params);

        // define a frame where x is pointing into the movement direction
        const Eigen::Matrix3f global_R_approach = Eigen::Quaternionf::FromTwoVectors(Eigen::Vector3f::UnitX(),
                                                                                     global_V_approach_dir)
                .toRotationMatrix();

        // Now the contact can be evaluated along the x-axis. Other forces should be minimal.
        const Eigen::Vector3f approach_V_force = global_R_approach.inverse() * global_V_force;

        const float force = std::abs(approach_V_force.x()); // x
        const float forcePerturbation = approach_V_force.tail<2>().norm(); // y and z

        ARMARX_DEBUG << VAROUT(force);
        ARMARX_DEBUG << VAROUT(forcePerturbation);

        const bool forceLimitReached = force > params.contactForceThreshold;

        // TODO(fabian.reister): Evaluate position -> success / failure
        const bool positionLimitReached = approachVector(params).norm() < (params.positionLimit + 100);

        return forceLimitReached and positionLimitReached;
    }


} // namespace armarx::manipulation::skills
