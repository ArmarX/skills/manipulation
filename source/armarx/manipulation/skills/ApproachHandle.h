/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <VirtualRobot/VirtualRobot.h>

#include "RobotAPI/components/ArViz/Client/Client.h"
#include "RobotAPI/libraries/RobotStatechartHelpers/VelocityControllerHelper.h"
#include "RobotAPI/libraries/skills/provider/PeriodicSpecializedSkill.h"

#include "armarx/manipulation/geometric_planning/ArticulatedObjectDoorHelper.h"
#include "armarx/manipulation/skills/aron/ApproachHandleParams.aron.generated.h"
#include <armarx/manipulation/core/Robot.h>


namespace armarx::manipulation::skills
{

    class ApproachHandle : virtual public armarx::skills::PeriodicSpecializedSkill<arondto::ApproachHandleParams>
    {
    public:
        using ParamsT = arondto::ApproachHandleParams;
        using Base = armarx::skills::PeriodicSpecializedSkill<arondto::ApproachHandleParams>;

        struct Context
        {
            Robot& robot;

            // debugging / visu
            armarx::viz::Client& arviz;
        };

        struct Parameters
        {
            // float approachVel = 10.F;
            // float positionLimit = 50.F; // TODO reduce but only if using impedance control
            // float contactForceTh = 10.F; // TODO update

            // visu
            float sphereVisuRadius = 30.F;
            float arrowVisuWidth = 5.F;
            int visuAlpha = 128.F;
        };

        ApproachHandle(const Context& ctx);

        ~ApproachHandle() override;


    private:
        armarx::skills::Skill::InitResult init(const SpecializedInitInput& in) override;

        armarx::skills::PeriodicSkill::StepResult step(const SpecializedMainInput& in) override;

        armarx::skills::Skill::ExitResult exit(const SpecializedExitInput& in) override;

    public:
        static armarx::skills::SkillDescription DefaultSkillDescription()
        {
            return {"ApproachHandle", "", {}, armarx::core::time::Duration::MilliSeconds(999999), // ??
                    ParamType::ToAronType()};
        }

    protected:
        bool stop();

        void draw(const ParamsT& params);

        bool finished(const ParamsT& params);

        Eigen::Vector3f approachDirection(const ParamsT& params);

        Eigen::Vector3f approachVector(const ParamsT& params);

    private:
        Context ctx;
        Parameters parameters;

        // FIXME use impedance controller instead
        armarx::VelocityControllerHelperPtr velocityController;

        geometric_planning::ArticulatedObjectDoorHelper::DoorInteractionContextExtended interactionInfo;
    };

} // namespace armarx::manipulation::skills
