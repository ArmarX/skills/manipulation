#include "ShapeHand.h"

#include "ArmarXCore/core/time/Duration.h"

#include <armarx/manipulation/core/arm.h>
#include <armarx/manipulation/core/aron_conversions.h>
#include <armarx/manipulation/core/types.h>
#include <armarx/manipulation/skills/aron/ShapeHandParams.aron.generated.h>
#include <armarx/manipulation/skills/config.h>

using namespace std::chrono_literals;

namespace armarx::manipulation::skills
{
    ShapeHand::ShapeHand(const Context& c) :
            armarx::skills::SpecializedSkill<arondto::ShapeHandParams>(DefaultSkillDescription()),
            ctx(c)
    {
    }


    armarx::skills::Skill::MainResult ShapeHand::main(const SpecializedMainInput& in)
    {
        core::Hand hand{};
        fromAron(in.params.hand, hand);

        const std::map<std::string, float> targetJointAngles{{"Fingers", in.params.fingers}, {"Thumb",   in.params.hand}};
        ctx.robot.handUnit(hand)->setJointAngles(targetJointAngles);

        if (in.params.timeoutMs > 0)
        {
            armarx::Clock::WaitFor(armarx::Duration::MilliSeconds(in.params.timeoutMs));
        }

        return {.status = armarx::skills::TerminatedSkillStatus::Succeeded, .data = nullptr};
    }

} // namespace armarx::manipulation::skills
