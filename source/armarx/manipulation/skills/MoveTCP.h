/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/ManagedIceObject.h>
#include <ArmarXCore/statechart/ParameterMapping.h>

#include "RobotAPI/components/ArViz/Client/Client.h"
#include "RobotAPI/libraries/skills/provider/SkillDescription.h"
#include "RobotAPI/libraries/skills/provider/SpecializedSkill.h"
#include <RobotAPI/components/ArViz/Client/Client.h>
#include <RobotAPI/interface/units/RobotUnit/RobotUnitInterface.h>
#include <RobotAPI/interface/units/RobotUnit/TaskSpaceActiveImpedanceControl.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/ForceTorqueHelper.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/PositionControllerHelper.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/VelocityControllerHelper.h>

#include <armarx/manipulation/core/Robot.h>
#include <armarx/manipulation/core/types.h>
#include <armarx/manipulation/skills/aron/MoveTCPParams.aron.generated.h>


namespace armarx::manipulation::skills
{
    class MoveTCP : virtual public armarx::skills::SpecializedSkill<arondto::MoveTCPParams>
    {
    public:
        struct Context
        {
            Robot& robot;

            // debugging / visu
            armarx::viz::Client& arviz;
        };

        struct Parameters
        {
            // float vel = 30.F;

            // visu
            float sphereVisuRadius = 30.F;
            float arrowVisuWidth = 5.F;
            int visuAlpha = 128;
        };


        MoveTCP(const Context& c);

    private:
        Skill::MainResult main(const SpecializedMainInput& in) final;

    public:
        static armarx::skills::SkillDescription DefaultSkillDescription()
        {
            return {"MoveTCP", "", {}, armarx::core::time::Duration::MilliSeconds(999999), // ??
                    ParamType::ToAronType()};
        }

    protected:
        bool initialize(const arondto::MoveTCPParams&); // Fabian PK: Why not using the init method?!

        void draw(const Pose& robot_T_tcp_desired);

        bool stop();

    private:
        Context ctx;
        Parameters parameters;

        Pose tcp_T_grasp_initial = Pose::Identity();

        armarx::VelocityControllerHelperPtr velocityController;
        armarx::PositionControllerHelperPtr positionController;
    };

} // namespace armarx::manipulation::skills
