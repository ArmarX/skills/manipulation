#include "EstablishDoorContact.h"

#include <memory>

#include <Eigen/Geometry>

#include <IceUtil/Time.h>

#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <ArmarXCore/util/CPPUtility/trace.h>

#include "RobotAPI/libraries/ArmarXObjects/ObjectID.h"
#include "RobotAPI/libraries/ArmarXObjects/aron_conversions/armarx.h"
#include <RobotAPI/components/ArViz/Client/Elements.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/VelocityControllerHelper.h>
#include <RobotAPI/libraries/core/CartesianVelocityController.h>
#include <RobotAPI/libraries/core/CartesianWaypointController.h>
#include <RobotAPI/libraries/core/FramedPose.h>

#include "armarx/manipulation/core/arm.h"
#include "armarx/manipulation/geometric_planning/ArticulatedObjectDoorHelper.h"
#include <armarx/manipulation/core/aron_conversions.h>
#include <armarx/manipulation/skills/aron/EstablishDoorContactParams.aron.generated.h>

namespace armarx::manipulation::skills
{
    EstablishDoorContact::EstablishDoorContact(const EstablishDoorContact::Context& ctx) :
            Base(DefaultSkillDescription(), armarx::core::time::Frequency::Hertz(100)),
            ctx(ctx),
            isSimulation(ctx.robot.robotUnit()->isSimulation())
    {
    }

    EstablishDoorContact::~EstablishDoorContact() = default;

    armarx::skills::Skill::InitResult EstablishDoorContact::init(const SpecializedInitInput& in)
    {
        ARMARX_INFO << "EstablishDoorContact::initialize";

        core::Arm arm{};
        fromAron(in.params.arm, arm);

        // ctx.forceTorqueHelper->setZero();

        const std::string controllerName = "EstablishDoorContactController";

        velHelper = std::make_shared<armarx::VelocityControllerHelper>(ctx.robot.robotUnit(), ctx.robot.arm(arm)->getName(), controllerName);

        ObjectID objectID;
        fromAron(in.params.object, objectID);


        ARMARX_INFO << "Querying descriptions";
        const auto descriptions = ctx.articulatedObjectReader.queryDescriptions(Clock::Now());

        ARMARX_INFO << "Available descriptions are:";
        for (const auto& description: descriptions)
        {
            ARMARX_INFO << description;
        }

        const std::string dishwasherName = objectID.dataset() + "/" + objectID.className();
        ARMARX_INFO << "Searching for object name `" << dishwasherName << "`";

        const auto isMatchingDescription = [&objectID](const auto& description) -> bool
        {
            return objectID.equalClass(ObjectID(description.name));
        };

        const auto descriptionIt = std::find_if(descriptions.begin(), descriptions.end(), isMatchingDescription);

        ARMARX_CHECK(descriptionIt != descriptions.end()) << "Description " << objectID << " not available";

        ARMARX_INFO << "getting object";
        std::optional<armarx::armem::articulated_object::ArticulatedObject> dishwasherOpt = ctx.articulatedObjectReader
                                                                                               .get(*descriptionIt,
                                                                                                    Clock::Now(), "0");


        ARMARX_CHECK(dishwasherOpt);

        armarx::armem::articulated_object::ArticulatedObject dishwasher = *dishwasherOpt;
        dishwasher.instance = "0"; // first available dishwasher FIXME objectID available


        const std::string xmlFilename = armarx::ArmarXDataPath::resolvePath(
                dishwasher.description.xml.serialize().path);
        ARMARX_INFO << "Loading (virtual) robot '" << dishwasher.description.name << "' from XML file '" << xmlFilename
                    << "'";

        object = VirtualRobot::RobotIO::loadRobot(xmlFilename);
        ARMARX_CHECK_NOT_NULL(object);

        object->setJointValues(dishwasher.config.jointMap);
        object->setGlobalPose(dishwasher.config.globalPose.matrix());
        object->setName(objectID.str()); // this is optional, name not used directly


        geometric_planning::ArticulatedObjectDoorHelper::Params doorHelperParams;
        geometric_planning::ArticulatedObjectDoorHelper doorHelper(object, doorHelperParams);

        ARMARX_INFO << "Planning interaction";
        interactionInfo = doorHelper.planInteraction(in.params.objectNodeSet);

        ARMARX_INFO << "drawing";
        draw(in.params);

        ARMARX_INFO << "done drawing";

        return {armarx::skills::TerminatedSkillStatus::Succeeded};
    }

    armarx::skills::PeriodicSkill::StepResult
    EstablishDoorContact::step(const SpecializedMainInput& in)
    {
        const Eigen::Vector3f robot_V_tcp_handle = approachDirection(in.params);

        Eigen::Vector6f velocity;
        velocity << robot_V_tcp_handle.x(), robot_V_tcp_handle.y(), robot_V_tcp_handle.z(), 0, 0, 0;

        ARMARX_CHECK_NOT_NULL(velHelper);
        velHelper->setTargetVelocity(in.params.approachVelocity * velocity);

        draw(in.params);

        if (finished(in.params))
        {
            return {.status = armarx::skills::ActiveOrTerminatedSkillStatus::Succeeded};
        }

        return {.status = armarx::skills::ActiveOrTerminatedSkillStatus::Running};
    }

    armarx::skills::Skill::ExitResult EstablishDoorContact::exit(const SpecializedExitInput &in)
    {
        stop();
        return {armarx::skills::TerminatedSkillStatus::Succeeded};
    }

    void EstablishDoorContact::draw(const Params& params)
    {
        core::Arm arm{};
        fromAron(params.arm, arm);

        const auto tcp = ctx.robot.arm(arm)->getTCP();

        ARMARX_CHECK_NOT_NULL(interactionInfo.rns.handleSurfaceProjection);
        ARMARX_CHECK_NOT_NULL(tcp);

        // global frame
        const Pose global_T_door_initial_contact = Pose(interactionInfo.rns.handleSurfaceProjection->getGlobalPose()) *
                                                   interactionInfo.handleSurfaceProjection_T_door_initial_contact;

        const Eigen::Vector3f global_P_door_initial_contact = global_T_door_initial_contact.translation();
        const Eigen::Vector3f global_P_tcp = tcp->getGlobalPosition();

        auto layer = ctx.arviz.layer("skills.establish_door_contact");
        layer.add(armarx::viz::Sphere("handle").position(global_P_door_initial_contact)
                                               .radius(parameters.sphereVisuRadius)
                                               .color(0, 0, 255, parameters.visuAlpha));
        layer.add(armarx::viz::Sphere("tcp").position(global_P_tcp).radius(parameters.sphereVisuRadius)
                                            .color(255, 0, 0, parameters.visuAlpha));
        layer.add(armarx::viz::Arrow("direction_" + tcp->getName() + "_contact")
                          .fromTo(global_P_tcp, global_P_door_initial_contact).width(parameters.arrowVisuWidth));

        ctx.arviz.commit(layer);
    }

    Eigen::Vector3f EstablishDoorContact::approachDirection(const Params& params)
    {
        core::Arm arm{};
        fromAron(params.arm, arm);

        const auto& tcp = ctx.robot.arm(arm)->getTCP();

        ARMARX_CHECK_NOT_NULL(interactionInfo.rns.handleSurfaceProjection);
        ARMARX_CHECK_NOT_NULL(tcp);
        ARMARX_CHECK_NOT_NULL(ctx.robot.robot());

        // global frame
        const Pose global_T_door_initial_contact = Pose(interactionInfo.rns.handleSurfaceProjection->getGlobalPose()) *
                                                   interactionInfo.handleSurfaceProjection_T_door_initial_contact;

        const Eigen::Vector3f global_P_door_initial_contact = global_T_door_initial_contact.translation();
        const Eigen::Vector3f global_P_tcp = tcp->getGlobalPosition();

        const Eigen::Vector3f global_V_tcp_handle = (global_P_door_initial_contact - global_P_tcp).normalized();

        // -> tcp frame
        armarx::FramedDirection framedDirection(global_V_tcp_handle, armarx::GlobalFrame, ctx.robot.name());
        const Eigen::Vector3f robot_V_tcp_handle = framedDirection.toRootFrame(ctx.robot.robot())->toEigen();

        return robot_V_tcp_handle;
    }

    bool EstablishDoorContact::stop()
    {
        ARMARX_INFO << "EstablishDoorContact::initialize";

        if (velHelper)
        {
            velHelper->cleanup();
        }

        return true;
    }

    bool EstablishDoorContact::finished(const Params& params)
    {
        core::Arm arm{};
        fromAron(params.arm, arm);

        const auto ft = ctx.robot.getForceTorqueInHand(core::getHand(arm));
        ARMARX_CHECK(ft.has_value()) << "No force/torque data available for hand "
                                     << core::HandNames.to_name(core::getHand(arm));

        const armarx::FramedDirection framedForce = ft->force;
        const Eigen::Vector3f force = framedForce.toRootFrame(ctx.robot.robot())->toEigen();

        return force.norm() > params.contactForceThreshold;
    }


} // namespace armarx::manipulation::skills
