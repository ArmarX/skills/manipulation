/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/ManagedIceObject.h>
#include <ArmarXCore/statechart/ParameterMapping.h>

#include "RobotAPI/libraries/ArmarXObjects/ObjectPoseClient.h"
#include "RobotAPI/libraries/armem/client/MemoryNameSystem.h"
#include "RobotAPI/libraries/armem_robot_state/client/common/VirtualRobotReader.h"
#include "RobotAPI/libraries/skills/provider/PeriodicSpecializedSkill.h"
#include <RobotAPI/components/ArViz/Client/Client.h>
#include <RobotAPI/interface/units/RobotUnit/RobotUnitInterface.h>
#include <RobotAPI/interface/units/RobotUnit/TaskSpaceActiveImpedanceControl.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/ForceTorqueHelper.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/PositionControllerHelper.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/VelocityControllerHelper.h>
#include <RobotAPI/libraries/skills/provider/Skill.h>

#include "armarx/manipulation/control/TrajectoryController.h"
#include "armarx/manipulation/core/Robot.h"
#include "armarx/manipulation/geometric_planning/ArticulatedObjectDoorHelper.h"
#include "armarx/manipulation/geometric_planning/ParametricPath.h"
#include "armarx/manipulation/skills/aron/ParametricPathExecutorParams.aron.generated.h"

namespace armarx
{
    class VelocityControllerHelper;
}

namespace armarx::manipulation
{
    namespace control
    {
        class TrajectoryController;
    }

    class ParametricPath;
} // namespace armarx::manipulation

namespace armarx::manipulation::skills
{
    class ParametricPathExecutor : virtual public armarx::skills::PeriodicSpecializedSkill<
            arondto::ParametricPathExecutorParams>
    {
    public:
        using Params = arondto::ParametricPathExecutorParams;
        using Base = armarx::skills::PeriodicSpecializedSkill<Params>;


        struct Context
        {
            Robot& robot;

            objpose::ObjectPoseClient objectPoseClient;

            viz::Client& arviz;
        };

        struct Parameters
        {
            // visu
            float sphereVisuRadius = 30.F;
            float arrowVisuWidth = 5.F;
            int visuAlpha = 128;
        };

        ParametricPathExecutor(const Context& ctx);

        ~ParametricPathExecutor() override;

    private:
        armarx::skills::Skill::InitResult init(const SpecializedInitInput& in) override;

        armarx::skills::PeriodicSkill::StepResult step(const SpecializedMainInput& in) override;

        armarx::skills::Skill::ExitResult exit(const SpecializedExitInput& in) override;

    public:
        static armarx::skills::SkillDescription DefaultSkillDescription()
        {
            return {"ParametricPathExecutor", "", {}, armarx::core::time::Duration::MilliSeconds(999999), // ??
                    ParamType::ToAronType()};
        }

    protected:
        Eigen::Vector3f approachDirection();

        void activateImpedanceControl(const Params&);

        void draw(const control::TrajectoryController::ControlData& controlData);

        bool stop();

        bool finished();

    private:
        Context ctx;
        Parameters params;

        std::shared_ptr<geometric_planning::ParametricPath> parametricPath;
        std::unique_ptr<control::TrajectoryController> trajController;

        core::Pose tcp_T_grasp_initial = core::Pose::Identity();

        armarx::NJointTaskSpaceImpedanceControlInterfacePrx taskSpaceImpedanceController;

        armarx::VelocityControllerHelperPtr velocityController;
        armarx::PositionControllerHelperPtr positionController;

        std::optional<core::Pose> robot_T_tcp_initial;

        // struct Stuff
        // {
        //     // the robot's state
        //     VirtualRobot::RobotPtr robot;
        //     VirtualRobot::RobotNodeSetPtr rns;
        //     VirtualRobot::RobotNodePtr tcp;

        //     // the scene's state
        //     VirtualRobot::RobotPtr object;
        //     VirtualRobot::RobotNodePtr grasp; // "node"
        //     VirtualRobot::RobotNodePtr joint;
        // };


        // Stuff stuff;

        const bool isSimulation;

        geometric_planning::ArticulatedObjectDoorHelper::DoorInteractionContextExtended interactionInfo;
    };

} // namespace armarx::manipulation::skills
