/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <VirtualRobot/VirtualRobot.h>

#include "RobotAPI/components/ArViz/Client/Client.h"
#include "RobotAPI/libraries/ArmarXObjects/ObjectPoseClient.h"
#include "RobotAPI/libraries/RobotStatechartHelpers/VelocityControllerHelper.h"
#include "RobotAPI/libraries/skills/provider/PeriodicSpecializedSkill.h"
#include <RobotAPI/libraries/armem_objects/client/articulated_object/Reader.h>

#include "armarx/manipulation/geometric_planning/ArticulatedObjectDoorHelper.h"
#include "armarx/manipulation/skills/aron/EstablishDoorContactParams.aron.generated.h"
#include <armarx/manipulation/core/Robot.h>


namespace armarx::manipulation::skills
{

    class EstablishDoorContact : public armarx::skills::PeriodicSpecializedSkill<
            arondto::EstablishDoorContactParams>
    {
    public:
        using Params = arondto::EstablishDoorContactParams;
        using Base = armarx::skills::PeriodicSpecializedSkill<Params>;

        struct Context
        {
            Robot& robot;

            // objpose::ObjectPoseClient objectPoseClient;
            ::armarx::armem::articulated_object::Reader& articulatedObjectReader;

            armarx::viz::Client& arviz;
        };


        struct Parameters
        {
            float contactForceTh = 5.F;
            float approachVel = 30.F;

            // visu
            float sphereVisuRadius = 30.F;
            float arrowVisuWidth = 5.F;
            int visuAlpha = 128;
        };

        EstablishDoorContact(const Context& ctx);

        ~EstablishDoorContact() override;

    private:
        armarx::skills::Skill::InitResult init(const SpecializedInitInput& in) override;

        armarx::skills::PeriodicSkill::StepResult step(const SpecializedMainInput& in) override;

        armarx::skills::Skill::ExitResult exit(const SpecializedExitInput& in) override;

    public:
        static armarx::skills::SkillDescription DefaultSkillDescription()
        {
            return {"EstablishDoorContact", "", {}, armarx::core::time::Duration::MilliSeconds(999999), // ??
                    ParamType::ToAronType()};
        }


    protected:
        Eigen::Vector3f approachDirection(const Params& params);

        bool stop();

        void draw(const Params& params);

        bool finished(const Params& params);

    private:
        Context ctx;
        Parameters parameters;

        armarx::VelocityControllerHelperPtr velHelper;

        const bool isSimulation;

        VirtualRobot::RobotPtr object;

        geometric_planning::ArticulatedObjectDoorHelper::DoorInteractionContext interactionInfo;
    };

} // namespace armarx::manipulation::skills
