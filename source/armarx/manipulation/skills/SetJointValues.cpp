#include "SetJointValues.h"

#include <chrono>
#include <memory>
#include <thread>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include "armarx/manipulation/core/types.h"


namespace armarx::manipulation::skills
{
    SetJointValues::SetJointValues(const Context& ctx) : Base(DefaultSkillDescription()), ctx(ctx)
    {
    }


    armarx::skills::Skill::MainResult
    SetJointValues::main(const arondto::SetJointValuesParams& params, const CallbackT& callback)
    {

        ARMARX_INFO << "initialize";

        // move to desired pose
        draw();

        std::map<std::string, float> targetJointVelocities;
        std::map<std::string, armarx::ControlMode> controlModes;

        for (const auto& [jName, _] : params.targetJointValues)
        {
            controlModes[jName] = armarx::ePositionControl;
        }

        auto kinematicUnit = ctx.robot.robotUnit()->getKinematicUnit();

        ARMARX_INFO << "Setting control mode";
        kinematicUnit->switchControlMode(controlModes);

        ARMARX_INFO << "setting target";
        kinematicUnit->setJointAngles(params.targetJointValues);

        while (not jointTargetReached())
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
        }

        ARMARX_INFO << "Goal reached";

        return {.status = armarx::skills::TerminatedSkillStatus::Succeeded, .data = nullptr};
    }

    bool
    SetJointValues::jointTargetReached()
    {
        ARMARX_CHECK(ctx.robot.synchronize());
        const auto jointValues = ctx.robot.robot()->getJointValues();

        for (const auto& [jName, jVal] : params.targetJointValues)
        {
            const float jDiff = std::abs(jVal - params.targetJointValues.at(jName));

            if (jDiff > params.jointEps)
            {
                return false;
            }
        }

        return true;
    }

    void
    SetJointValues::draw()
    {
        const auto& robot = ctx.robot.robot();

        const Pose global_T_robot(robot->getGlobalPose());

        auto layer = ctx.arviz.layer("skills.move_tcp");

        std::map<std::string, float> jointValues = robot->getJointValues();

        // override the current joint values if target is available
        for (const auto& [jName, jVal] : params.targetJointValues)
        {
            jointValues[jName] = jVal;
        }

        layer.add(armarx::viz::Robot("joint_target")
                      .overrideColor(armarx::viz::Color(0, 0, 255, params.visuAlpha))
                      .joints(jointValues));

        ctx.arviz.commit(layer);
    }


} // namespace armarx::manipulation::skills
