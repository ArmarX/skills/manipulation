/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <VirtualRobot/MathTools.h>
#include <VirtualRobot/VirtualRobot.h>

#include "RobotAPI/components/ArViz/Client/Client.h"
#include "RobotAPI/libraries/skills/provider/SpecializedSkill.h"

#include <armarx/manipulation/core/Robot.h>
#include <armarx/manipulation/skills/aron/SetJointValuesParams.aron.generated.h>


namespace armarx::manipulation::skills
{
    class SetJointValues : public armarx::skills::SpecializedSkill<arondto::SetJointValuesParams>
    {
    public:
        using Base = SpecializedSkill<arondto::SetJointValuesParams>;

        struct Context
        {
            Robot& robot;
            armarx::viz::Client& arviz;
        };

        struct Parameters
        {
            std::map<std::string, float> targetJointValues;

            float jointEps = VirtualRobot::MathTools::deg2rad(1);

            // float vel = 30.F;
            float jointVelocity = 0.1;

            bool isSimulation;

            // visu
            float sphereVisuRadius = 30.F;
            float arrowVisuWidth = 5.F;
            int visuAlpha = 128;
        };

        SetJointValues(const Context& ctx);

        ~SetJointValues() override = default;

        VirtualRobot::RobotNodeSetPtr requiredRobotNodeSet(const VirtualRobot::RobotPtr& robot) const;

        // bool canBeExecuted() const override;
        bool jointTargetReached();


        bool initialize();

        virtual Skill::MainResult main(const arondto::SetJointValuesParams& params,
                               const CallbackT& callback = [](const aron::data::DictPtr& returnValue)
                               { (void) returnValue; }) = 0;


        void draw();


        // float progress() const override;

        // IceUtil::Time eta() const override;

        // float predictSuccess() const override;

        static armarx::skills::SkillDescription DefaultSkillDescription()
        {
            return {"SetJointValues", "", {}, armarx::core::time::Duration::MilliSeconds(999999), // ??
                    ParamType::ToAronType()};
        }

    private:
        Context ctx;
        Parameters params;
    };

} // namespace armarx::manipulation::skills
