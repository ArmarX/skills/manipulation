#include "ParametricPathExecutor.h"

#include <memory>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <IceUtil/Time.h>

#include <SimoxUtility/color/Color.h>
#include <VirtualRobot/XML/RobotIO.h>
#include <VirtualRobot/math/Helpers.h>

#include "ArmarXCore/core/time/Duration.h"
#include "ArmarXCore/core/time/Frequency.h"
#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <ArmarXCore/util/CPPUtility/trace.h>

#include "RobotAPI/libraries/ArmarXObjects/aron_conversions/armarx.h"
#include "RobotAPI/libraries/ArmarXObjects/aron_conversions/objpose.h"
#include "RobotAPI/libraries/skills/provider/SpecializedSkill.h"
#include <RobotAPI/components/ArViz/Client/Elements.h>
#include <RobotAPI/interface/units/RobotUnit/TaskSpaceActiveImpedanceControl.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/VelocityControllerHelper.h>
#include <RobotAPI/libraries/core/CartesianVelocityController.h>
#include <RobotAPI/libraries/core/CartesianWaypointController.h>
#include <RobotAPI/libraries/core/FramedPose.h>

#include "armarx/manipulation/core/arm.h"
#include "armarx/manipulation/geometric_planning/ArticulatedObjectDoorHelper.h"
#include "armarx/manipulation/geometric_planning/ArticulatedObjectGeometricPlanningHelper.h"
#include <armarx/manipulation/control/TrajectoryController.h>
#include <armarx/manipulation/core/aron_conversions.h>
#include <armarx/manipulation/geometric_planning/ArticulatedObjectGeometricPlanningHelper.h>
#include <armarx/manipulation/geometric_planning/ParametricPath.h>
#include <armarx/manipulation/skills/aron/ParametricPathExecutorParams.aron.generated.h>
#include <armarx/manipulation/skills/config.h>


namespace armarx::manipulation::skills
{
    ParametricPathExecutor::ParametricPathExecutor(const ParametricPathExecutor::Context& ctx) :
        Base(DefaultSkillDescription(), armarx::Frequency(armarx::Duration::MilliSeconds(20))),
        ctx(ctx),
        isSimulation(ctx.robot.robotUnit()->isSimulation())
    {
    }

    ParametricPathExecutor::~ParametricPathExecutor() = default;


    armarx::skills::Skill::InitResult
    ParametricPathExecutor::init(const SpecializedInitInput& in)
    {
        ARMARX_INFO << "initialize";
        core::Arm arm{};
        fromAron(in.params.arm, arm);

        const auto armRns = ctx.robot.arm(arm);
        const auto tcp = armRns->getTCP();

        const std::string controllerName = "DoorHandleGraspImpedanceController";

        ObjectID objectID;
        fromAron(in.params.object, objectID);

        // TODO use ArticulatedObjectReader instead
        const std::optional<objpose::ObjectPose> objectPose =
            ctx.objectPoseClient.fetchObjectPose(objectID);

        VirtualRobot::RobotPtr object = VirtualRobot::RobotIO::loadRobot(
            objectPose->articulatedSimoxXmlPath->toSystemPath(), VirtualRobot::RobotIO::eStructure);


        geometric_planning::ArticulatedObjectGeometricPlanningHelper helper(object);

        geometric_planning::ArticulatedObjectDoorHelper::Params doorHelperParams;
        geometric_planning::ArticulatedObjectDoorHelper doorHelper(object, doorHelperParams);

        interactionInfo =
            doorHelper.planInteractionExtended(in.params.objectNodeSet, Pose(tcp->getGlobalPose()));

        parametricPath = std::make_shared<geometric_planning::ParametricPath>(
            helper.getPathForNode(interactionInfo.rns.handle, interactionInfo.rns.joint));


        trajController = std::make_unique<control::TrajectoryController>(tcp, parametricPath);

        // draw();
        const core::Pose global_T_grasp =
            Pose(interactionInfo.rns.handleSurfaceProjection->getGlobalPose()) *
            interactionInfo.handleSurfaceProjection_T_tcp_at_handle;
        const core::Pose global_T_tcp(tcp->getGlobalPose());

        tcp_T_grasp_initial = global_T_tcp.inverse() * global_T_grasp;

        if (not isSimulation)
        {
            activateImpedanceControl(in.params);
        }
        else
        {
            velocityController.reset(new armarx::VelocityControllerHelper(
                ctx.robot.robotUnit(), armRns->getName(), "VelocityController"));
            positionController.reset(new armarx::PositionControllerHelper(
                tcp, velocityController, tcp->getPoseInRootFrame()));

            robot_T_tcp_initial = tcp->getPoseInRootFrame();
        }

        return {.status = armarx::skills::TerminatedSkillStatus::Succeeded};
    }

    void
    ParametricPathExecutor::activateImpedanceControl(
        const arondto::ParametricPathExecutorParams& dto)
    {
        core::Arm arm{};
        fromAron(dto.arm, arm);

        const auto armRns = ctx.robot.arm(arm);
        const auto tcp = armRns->getTCP();

        std::vector<float> desiredJointPositions = armRns->getJointValues();

        const auto initialTcpPose = tcp->getPoseInRootFrame();
        ARMARX_INFO << "Impedance control TCP position "
                    << core::Pose(initialTcpPose).translation();

        // Impedance Control
        const std::vector<float> Kpos = {500, 500, 500};
        const std::vector<float> Kori = {500, 500, 500};
        const std::vector<float> Dpos = {200, 200, 200};
        const std::vector<float> Dori = {200, 200, 200};
        float knullval = 3;
        float dnullval = 1;

        std::vector<float> knull;
        std::vector<float> dnull;

        for (size_t i = 0; i < 8; ++i)
        {
            knull.push_back(knullval);
            dnull.push_back(dnullval);
        }
        armarx::NJointTaskSpaceImpedanceControlConfigPtr rightConfig =
            new armarx::NJointTaskSpaceImpedanceControlConfig;
        rightConfig->nodeSetName = core::ArmNames.to_name(arm);
        rightConfig->desiredPosition = math::Helpers::Position(initialTcpPose);
        rightConfig->desiredOrientation = math::Helpers::Orientation(initialTcpPose);
        rightConfig->Kpos = Eigen::Map<const Eigen::Vector3f>(Kpos.data(), Kpos.size());
        rightConfig->Kori = Eigen::Map<const Eigen::Vector3f>(Kori.data(), Kori.size());
        rightConfig->Dpos = Eigen::Map<const Eigen::Vector3f>(Dpos.data(), Dpos.size());
        rightConfig->Dori = Eigen::Map<const Eigen::Vector3f>(Dori.data(), Dori.size());
        rightConfig->desiredJointPositions = Eigen::Map<const Eigen::VectorXf>(
            desiredJointPositions.data(), desiredJointPositions.size());
        rightConfig->Knull = Eigen::Map<const Eigen::VectorXf>(knull.data(), knull.size());
        rightConfig->Dnull = Eigen::Map<const Eigen::VectorXf>(dnull.data(), dnull.size());
        rightConfig->torqueLimit = 10;

        auto controller = ctx.robot.robotUnit()->createOrReplaceNJointController(
            "NJointTaskSpaceImpedanceController", IMPEDANCE_CONTROLLER_NAME, rightConfig);

        armarx::NJointTaskSpaceImpedanceControlInterfacePrx taskSpaceImpedanceController =
            armarx::NJointTaskSpaceImpedanceControlInterfacePrx::checkedCast(controller);
        ARMARX_CHECK_NOT_NULL(taskSpaceImpedanceController);

        ARMARX_CHECK(not ctx.robot.robotUnit()->isSimulation())
            << "Cannot use impedance control in simulation!";
        // in simulation, this is not possible ...

        taskSpaceImpedanceController->activateController();
    }

    armarx::skills::PeriodicSkill::StepResult
    ParametricPathExecutor::step(const SpecializedMainInput& in)
    {
        ARMARX_CHECK_POSITIVE(in.params.controlVelocity);

        static DateTime lastTimestamp = armarx::Clock::Now();

        const control::TrajectoryController::ControlData controlData = trajController->calculate();

        const Eigen::Matrix3f robot_R_global =
            core::Pose(ctx.robot.robot()->getGlobalPose()).linear().inverse();

        const Pose global_T_robot = core::Pose(ctx.robot.robot()->getGlobalPose());
        const Pose robot_T_global = global_T_robot.inverse();

        const Eigen::Matrix3f robot_R_frame = robot_R_global * controlData.global_T_frame.linear();

        ARMARX_DEBUG << "In frame: " << controlData.desiredVelocityDirection;

        // the orientation should be position controlled.
        Eigen::Vector6f vel = Eigen::Vector6f::Zero();
        vel << 0.01 * Eigen::Vector3f(robot_R_frame * controlData.desiredVelocityDirection),
            Eigen::Vector3f::Zero();

        ARMARX_DEBUG << "vel " << vel;
        //        velHelper->setTargetVelocity(vel);
        ARMARX_DEBUG << "Global T frame " << controlData.global_T_frame.linear();

        const Eigen::Vector3f robot_V_path = robot_R_frame * controlData.desiredVelocityDirection;
        const core::Pose robot_T_frame = robot_T_global * controlData.global_T_frame;

        const auto now = armarx::Clock::Now();
        const Duration dt = now - lastTimestamp;
        ARMARX_CHECK(dt.isPositive());
        lastTimestamp = now;

        const Eigen::Vector3f posDiff =
            0.03 * robot_V_path * in.params.controlVelocity * dt.toSecondsDouble();

        // FIXME this is not completely correct.
        core::Pose robot_T_tcp_desired = robot_T_frame;
        robot_T_tcp_desired.translation() += posDiff;

        if (not isSimulation)
        {
            taskSpaceImpedanceController->setPosition(robot_T_tcp_desired.translation());
            //        taskSpaceImpedanceController->setOrientation(Eigen::Quaternionf(robot_T_frame.linear()));
        }
        else
        {
            // keep TCP orientation fixed.
            ARMARX_CHECK(robot_T_tcp_initial.has_value());
            robot_T_tcp_desired.linear() = robot_T_tcp_initial->linear();

            positionController->setTarget(robot_T_tcp_desired.matrix());
            positionController->update();
        }


        ARMARX_DEBUG << "robot_v_path" << robot_V_path;

        auto layer = ctx.arviz.layer("skills.parametric_path_executor2");

        layer.add(armarx::viz::Arrow("movement_direction3")
                      .fromTo(Eigen::Vector3f(
                                  core::Pose(ctx.robot.robot()->getGlobalPose()).translation()),
                              core::Pose(ctx.robot.robot()->getGlobalPose()) * robot_V_path)
                      //   .pose()
                      .color(simox::Color::red()));

        core::Arm arm{};
        fromAron(in.params.arm, arm);

        const auto& robotArmHelper = ctx.robot.robotArmHelper(arm);
        const auto& armHelper = ctx.robot.armHelper(arm);

        const core::Pose tcp_T_hand_root(robotArmHelper.getTcp2HandRootTransform());

        armarx::viz::Robot hand =
            armarx::viz::Robot("grasp" + std::to_string(layer.size()))
                .file(armHelper.getHandModelPackage(), armHelper.getHandModelPath())
                .pose(core::Pose(global_T_robot * robot_T_tcp_desired * tcp_T_hand_root).matrix())
                .overrideColor(armarx::viz::Color(simox::color::Color::green()));
        //   .joints(fingerJoints);

        layer.add(hand);
        ctx.arviz.commit(layer);

        draw(controlData);

        if (finished())
        {
            return {.status = armarx::skills::ActiveOrTerminatedSkillStatus::Succeeded};
        }

        return {.status = armarx::skills::ActiveOrTerminatedSkillStatus::Running};
    }

    armarx::skills::Skill::ExitResult
    ParametricPathExecutor::exit(const SpecializedExitInput&)
    {
        stop();

        return {.status = armarx::skills::TerminatedSkillStatus::Succeeded};
    }

    void
    ParametricPathExecutor::draw(const control::TrajectoryController::ControlData& controlData)
    {
        auto layer = ctx.arviz.layer("skills.parametric_path_executor");

        ARMARX_DEBUG << "desiredVelDir" << controlData.desiredVelocityDirection;
        layer.add(armarx::viz::Arrow("movement_direction")
                      .fromTo(controlData.global_T_frame.translation(),
                              controlData.global_T_frame * controlData.desiredVelocityDirection));

        layer.add(armarx::viz::Sphere("trajectory_projection")
                      .position(controlData.global_T_frame.translation())
                      .radius(params.sphereVisuRadius)
                      .color(0, 0, 255, params.visuAlpha));

        layer.add(armarx::viz::Pose("trajectory_projection_pose").pose(controlData.global_T_frame));

        const Pose global_T_tcp_at_handle(
            Pose(interactionInfo.rns.handleSurfaceProjection->getGlobalPose()) *
            interactionInfo.handleSurfaceProjection_T_tcp_at_handle);

        layer.add(armarx::viz::Pose("grasp_pose").pose(global_T_tcp_at_handle));
        layer.add(armarx::viz::Pose("joint_pose").pose(interactionInfo.rns.joint->getGlobalPose()));
        layer.add(armarx::viz::Pose("parametric_path").pose(controlData.global_T_path).scale(10));

        ctx.arviz.commit(layer);
    }

    bool
    ParametricPathExecutor::stop()
    {
        ARMARX_INFO << "ParametricPathExecutor::stop";

        if (isSimulation)
        {
            positionController->immediateHardStop();
            velocityController->cleanup();
        }

        return true;
    }

    bool
    ParametricPathExecutor::finished()
    {
        const control::TrajectoryController::ControlData controlData = trajController->calculate();
        return (controlData.param > 0.99F);
    }

} // namespace armarx::manipulation::skills
