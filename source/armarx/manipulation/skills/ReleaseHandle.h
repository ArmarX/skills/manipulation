/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <VirtualRobot/MathTools.h>
#include <ArmarXCore/core/ManagedIceObject.h>
#include <ArmarXCore/statechart/ParameterMapping.h>

#include "RobotAPI/libraries/skills/provider/SpecializedSkill.h"
#include <RobotAPI/components/ArViz/Client/Client.h>
#include <RobotAPI/interface/units/RobotUnit/RobotUnitInterface.h>
#include <RobotAPI/interface/units/RobotUnit/TaskSpaceActiveImpedanceControl.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/ForceTorqueHelper.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/PositionControllerHelper.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/VelocityControllerHelper.h>

#include <armarx/manipulation/core/Robot.h>
#include <armarx/manipulation/core/types.h>
#include <armarx/manipulation/skills/aron/ReleaseHandleParams.aron.generated.h>


namespace armarx
{
    class VelocityControllerHelper;
}

namespace armarx::manipulation
{
    class TrajectoryController;
    class ParametricPath;
} // namespace armarx::manipulation

namespace armarx::manipulation::skills
{
    class ReleaseHandle :
        public armarx::skills::SpecializedSkill<arondto::ReleaseHandleParams>
    {
    public:
        struct Context
        {
            Robot& robot;
            // debugging / visu
            armarx::viz::Client& arviz;
        };

        struct Parameters
        {
            // float movement = 200;
            // float movementY = 200;

            float thresholdOrientationReached = VirtualRobot::MathTools::deg2rad(5); // [rad]
            float thresholdPositionReached = 30; // [mm]

            float linearVelocity = 20; // [mm/s]
            int64_t updateRateMSController = 20; // [ms]

            // visu
            float sphereVisuRadius = 30.F;
            float arrowVisuWidth = 5.F;
            int visuAlpha = 128;
        };

        ReleaseHandle(const Context& c);
        ~ReleaseHandle() override;

    private:
        Skill::MainResult main(const SpecializedMainInput& in) final;

        bool initialize(const arondto::ReleaseHandleParams&);
        bool stop();

        void draw(const core::Pose& robot_T_tcp_desired, Arm arm);

        void activateImpedanceControl(const arondto::ReleaseHandleParams& params);


    private:
        Context ctx;
        Parameters parameters;

        //        std::shared_ptr<armarx::VelocityControllerHelper> velHelper;
        core::Pose tcp_T_grasp_initial = core::Pose::Identity();

        armarx::NJointTaskSpaceImpedanceControlInterfacePrx taskSpaceImpedanceController;

        armarx::VelocityControllerHelperPtr velocityController;
        armarx::PositionControllerHelperPtr positionController;
    };

} // namespace armarx::manipulation::skills
