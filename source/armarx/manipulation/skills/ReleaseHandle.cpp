#include "ReleaseHandle.h"

#include <algorithm>
#include <chrono>
#include <memory>
#include <mutex>
#include <thread>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <IceUtil/Time.h>

#include <SimoxUtility/color/Color.h>
#include <VirtualRobot/MathTools.h>
#include <VirtualRobot/VirtualRobot.h>
#include <VirtualRobot/math/Helpers.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <ArmarXCore/util/CPPUtility/trace.h>

#include <RobotAPI/components/ArViz/Client/Elements.h>
#include <RobotAPI/interface/units/RobotUnit/NJointTaskSpaceDMPController.h>
#include <RobotAPI/interface/units/RobotUnit/TaskSpaceActiveImpedanceControl.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/VelocityControllerHelper.h>
#include <RobotAPI/libraries/armem/core/Time.h>
#include <RobotAPI/libraries/core/CartesianVelocityController.h>
#include <RobotAPI/libraries/core/CartesianWaypointController.h>
#include <RobotAPI/libraries/core/FramedPose.h>

#include "armarx/manipulation/core/arm.h"
#include "armarx/manipulation/core/aron_conversions.h"
#include "armarx/manipulation/skills/config.h"
#include <armarx/manipulation/control/NJointTaskSpaceImpedanceControllerVelocityLimited.h>
#include <armarx/manipulation/control/TrajectoryController.h>
#include <armarx/manipulation/core/types.h>
#include <armarx/manipulation/geometric_planning/ArticulatedObjectGeometricPlanningHelper.h>
#include <armarx/manipulation/geometric_planning/ParametricPath.h>
#include <armarx/manipulation/skills/config.h>


namespace armarx::manipulation::skills
{
    ReleaseHandle::ReleaseHandle(const Context& c) :
            armarx::skills::SpecializedSkill<arondto::ReleaseHandleParams>({"ReleaseHandle", "", {}, armarx::core::time::Duration::MilliSeconds(999999), // ??
                                                                            arondto::ReleaseHandleParams::ToAronType()}),
            ctx(c)
    {
    }

    ReleaseHandle::~ReleaseHandle() = default;


    bool ReleaseHandle::initialize(const arondto::ReleaseHandleParams& params)
    {
        ARMARX_INFO << "initialize";


        if (not ctx.robot.robotUnit()->isSimulation())
        {
            activateImpedanceControl(params);
        } else
        {
            Arm arm{};
            core::fromAron(params.arm, arm);

            const auto armRns = ctx.robot.arm(arm);

            velocityController.reset(new armarx::VelocityControllerHelper(ctx.robot.robotUnit(), armRns->getName(),
                                                                          "VelocityController"));
            positionController.reset(new armarx::PositionControllerHelper(armRns->getTCP(), velocityController,
                                                                          armRns->getTCP()->getPoseInRootFrame()));
        }


        return true;
    }

    inline armarx::NJointTaskSpaceImpedanceControlRuntimeConfig
    toRuntimeConfig(const armarx::NJointTaskSpaceImpedanceControlConfig& cfg)
    {
        return armarx::NJointTaskSpaceImpedanceControlRuntimeConfig{.Kpos = cfg.Kpos, .Kori = cfg.Kori, .Dpos = cfg
                .Dpos, .Dori = cfg.Dori, .desiredJointPositions =
        cfg.desiredJointPositions, .Knull = cfg.Knull, .Dnull = cfg.Dnull, .torqueLimit = cfg.torqueLimit};
    }


    void ReleaseHandle::activateImpedanceControl(const arondto::ReleaseHandleParams& params)
    {
        core::Arm arm{};
        fromAron(params.arm, arm);

        const auto armRns = ctx.robot.arm(arm);
        std::vector<float> desiredJointPositionRight = armRns->getJointValues();

        const auto initialTcpPose = armRns->getTCP()->getPoseInRootFrame();
        ARMARX_INFO << "Impedance control TCP position " << core::Pose(initialTcpPose).translation();

        // Impedance Control
        const std::vector<float> Kpos = {500, 500, 500};
        const std::vector<float> Kori = {500, 500, 500};
        const std::vector<float> Dpos = {200, 200, 200};
        const std::vector<float> Dori = {200, 200, 200};
        float knullval = 3;
        float dnullval = 1;

        std::vector<float> knull;
        std::vector<float> dnull;

        for (size_t i = 0; i < 8; ++i)
        {
            knull.push_back(knullval);
            dnull.push_back(dnullval);
        }
        armarx::NJointTaskSpaceImpedanceControlConfigPtr rightConfig = new armarx::NJointTaskSpaceImpedanceControlConfig;
        rightConfig->nodeSetName = core::ArmNames.to_name(arm);
        rightConfig->desiredPosition = math::Helpers::Position(initialTcpPose);
        rightConfig->desiredOrientation = math::Helpers::Orientation(initialTcpPose);
        rightConfig->Kpos = Eigen::Map<const Eigen::Vector3f>(Kpos.data(), Kpos.size());
        rightConfig->Kori = Eigen::Map<const Eigen::Vector3f>(Kori.data(), Kori.size());
        rightConfig->Dpos = Eigen::Map<const Eigen::Vector3f>(Dpos.data(), Dpos.size());
        rightConfig->Dori = Eigen::Map<const Eigen::Vector3f>(Dori.data(), Dori.size());
        rightConfig->desiredJointPositions = Eigen::Map<const Eigen::VectorXf>(desiredJointPositionRight.data(),
                                                                               desiredJointPositionRight.size());
        rightConfig->Knull = Eigen::Map<const Eigen::VectorXf>(knull.data(), knull.size());
        rightConfig->Dnull = Eigen::Map<const Eigen::VectorXf>(dnull.data(), dnull.size());
        rightConfig->torqueLimit = 10;

        auto controller = ctx.robot.robotUnit()->createOrReplaceNJointController("NJointTaskSpaceImpedanceController",
                                                                                 IMPEDANCE_CONTROLLER_NAME,
                                                                                 rightConfig);

        armarx::NJointTaskSpaceImpedanceControlInterfacePrx taskSpaceImpedanceController = armarx::NJointTaskSpaceImpedanceControlInterfacePrx::checkedCast(
                controller);
        ARMARX_CHECK_NOT_NULL(taskSpaceImpedanceController);

        ARMARX_CHECK(not ctx.robot.robotUnit()->isSimulation()) << "Cannot use impedance control in simulation!";
        // in simulation, this is not possible ...

        taskSpaceImpedanceController->activateController();
    }


    ::armarx::skills::Skill::MainResult ReleaseHandle::main(const SpecializedMainInput& in)
    {
        ARMARX_TRACE;
        initialize(in.params);

        ARMARX_INFO << "Opening hand";

        core::Arm arm{};
        fromAron(in.params.arm, arm);

        // openHand

        const std::map<std::string, float> targetJointAngles{{"Fingers", 0},
                                                             {"Thumb",   0}};
        ctx.robot.handUnit(core::getHand(arm))->setJointAngles(targetJointAngles);

        // wait until hand is open
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));

        // move downwards
        const auto armRns = ctx.robot.arm(arm);
        core::Pose robot_T_tcp_desired(armRns->getTCP()->getPoseInRootFrame());
        robot_T_tcp_desired.translation().z() -= in.params.movement;

        draw(robot_T_tcp_desired, arm);

        if (not ctx.robot.robotUnit()->isSimulation())
        {
            ARMARX_INFO << "Creating impedance velocity controller";

            control::NJointTaskSpaceImpedanceControllerVelocityLimited impedanceControllerVelLimited(
                    taskSpaceImpedanceController, armRns->getTCP(),
                    control::NJointTaskSpaceImpedanceControllerVelocityLimited::Params{.posDiffEps = parameters
                            .thresholdPositionReached, .linearVelocity = parameters
                            .linearVelocity, .updateRateMS = parameters.updateRateMSController});

            impedanceControllerVelLimited.setTarget(robot_T_tcp_desired.translation());
            impedanceControllerVelLimited.run();

            ARMARX_INFO << "Target 1 reached";

            robot_T_tcp_desired.translation().z() -= 50; // FIXME param
            robot_T_tcp_desired.translation().y() -= in.params.movementY;

            draw(robot_T_tcp_desired, arm);

            impedanceControllerVelLimited.setTarget(robot_T_tcp_desired.translation());
            impedanceControllerVelLimited.run();

            ARMARX_INFO << "Target 2 reached";
        } else
        {
            // (1) move downwards
            positionController->setTarget(robot_T_tcp_desired.matrix());
            positionController->update();

            // parameterize position controller (thresholds)
            positionController->thresholdPositionReached = parameters.thresholdPositionReached;
            positionController->thresholdOrientationReached = parameters.thresholdOrientationReached;

            // run controller
            while (not positionController->isFinalTargetReached())
            {
                std::this_thread::sleep_for(std::chrono::milliseconds(10));
                positionController->update();
            }

            ARMARX_INFO << "Goal 1 reached";

            // (2) move the arm closer to the robot + downwards
            robot_T_tcp_desired.translation().z() -= in.params.movement;
            robot_T_tcp_desired.translation().y() -= in.params.movementY;
            positionController->setTarget(robot_T_tcp_desired.matrix());
            positionController->update();

            // parameterize position controller (thresholds)
            positionController->thresholdPositionReached = parameters.thresholdPositionReached;
            positionController->thresholdOrientationReached = parameters.thresholdOrientationReached;

            // run controller
            while (not positionController->isFinalTargetReached())
            {
                std::this_thread::sleep_for(std::chrono::milliseconds(10));
                positionController->update();
            }

            ARMARX_INFO << "Goal 2 reached";
        }


        stop();

        return {.status = armarx::skills::TerminatedSkillStatus::Succeeded};
    }

    void ReleaseHandle::draw(const core::Pose& robot_T_tcp_desired, const Arm arm)
    {
        const core::Pose global_T_robot(ctx.robot.robot()->getGlobalPose());

        auto layer = ctx.arviz.layer("skills.release_handle");

        layer.add(armarx::viz::Sphere("tcp_target").position((global_T_robot * robot_T_tcp_desired).translation())
                                                   .radius(parameters.sphereVisuRadius)
                                                   .color(0, 0, 255, parameters.visuAlpha));
        layer.add(armarx::viz::Pose("tcp_target_pose").pose(global_T_robot * robot_T_tcp_desired)
                                                      .color(0, 0, 255, parameters.visuAlpha));

        layer.add(armarx::viz::Arrow("movement").fromTo(ctx.robot.arm(arm)->getTCP()->getGlobalPosition(),
                                                        (global_T_robot * robot_T_tcp_desired).translation())
                                                .width(parameters.arrowVisuWidth));

        ctx.arviz.commit(layer);
    }

    bool ReleaseHandle::stop()
    {
        ARMARX_INFO << "ReleaseHandle::stop";

        if (ctx.robot.robotUnit()->isSimulation())
        {
            positionController->immediateHardStop();
            velocityController->cleanup();
        }

        return true;
    }


} // namespace armarx::manipulation::skills
