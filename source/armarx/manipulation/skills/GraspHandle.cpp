#include "GraspHandle.h"

#include <memory>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <IceUtil/Time.h>

#include <VirtualRobot/math/Helpers.h>

#include "ArmarXCore/core/time/TimeUtil.h"
#include <ArmarXCore/observers/variant/DatafieldRef.h>
#include <ArmarXCore/util/CPPUtility/trace.h>

#include "RobotAPI/libraries/armem_robot_state/client/common/VirtualRobotReader.h"
#include <RobotAPI/components/ArViz/Client/Elements.h>
#include <RobotAPI/interface/units/RobotUnit/TaskSpaceActiveImpedanceControl.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/VelocityControllerHelper.h>
#include <RobotAPI/libraries/core/CartesianVelocityController.h>
#include <RobotAPI/libraries/core/CartesianWaypointController.h>
#include <RobotAPI/libraries/core/FramedPose.h>

#include <armarx/manipulation/core/arm.h>
#include <armarx/manipulation/core/aron_conversions.h>
#include <armarx/manipulation/core/types.h>
#include <armarx/manipulation/skills/aron/GraspHandleParams.aron.generated.h>
#include <armarx/manipulation/skills/config.h>

namespace armarx::manipulation::skills
{
    GraspHandle::GraspHandle(const Context& c) :
        armarx::skills::SpecializedSkill<arondto::GraspHandleParams>(DefaultSkillDescription()),
        ctx(c)
    {
    }


    armarx::skills::Skill::MainResult
    GraspHandle::main(const SpecializedMainInput& in)
    {
        ARMARX_INFO << "GraspHandle::initialize";

        ARMARX_CHECK(ctx.robot.synchronize());

        if (not ctx.robot.robotUnit()->isSimulation())
        {
            activateImpedanceControl(in.params);
        }

        core::Hand hand{};
        fromAron(in.params.hand, hand);

        const std::map<std::string, float> targetJointAngles{{"Fingers", 1}, {"Thumb", 0}};
        ctx.robot.handUnit(hand)->setJointAngles(targetJointAngles);

        armarx::TimeUtil::SleepMS(
            1000); // TODO event-based: wait until finger position does not change any more
        //  .. the door will likely be opened gently.

        return {.status = armarx::skills::TerminatedSkillStatus::Succeeded};
    }

    void
    GraspHandle::activateImpedanceControl(const arondto::GraspHandleParams& dto)
    {

        core::Hand hand{};
        fromAron(dto.hand, hand);

        const auto armRns = ctx.robot.arm(getArm(hand));
        std::vector<float> desiredJointPositionRight = armRns->getJointValues();

        const auto initialTcpPose = armRns->getTCP()->getPoseInRootFrame();
        ARMARX_INFO << "Impedance control TCP position "
                    << core::Pose(initialTcpPose).translation();

        // Impedance Control
        const std::vector<float> Kpos = {500, 500, 500};
        const std::vector<float> Kori = {500, 500, 500};
        const std::vector<float> Dpos = {200, 200, 200};
        const std::vector<float> Dori = {200, 200, 200};
        float knullval = 3;
        float dnullval = 1;

        std::vector<float> knull;
        std::vector<float> dnull;

        for (size_t i = 0; i < 8; ++i)
        {
            knull.push_back(knullval);
            dnull.push_back(dnullval);
        }
        armarx::NJointTaskSpaceImpedanceControlConfigPtr rightConfig =
            new armarx::NJointTaskSpaceImpedanceControlConfig;
        rightConfig->nodeSetName = core::ArmNames.to_name(getArm(hand));
        rightConfig->desiredPosition = math::Helpers::Position(initialTcpPose);
        rightConfig->desiredOrientation = math::Helpers::Orientation(initialTcpPose);
        rightConfig->Kpos = Eigen::Map<const Eigen::Vector3f>(Kpos.data(), Kpos.size());
        rightConfig->Kori = Eigen::Map<const Eigen::Vector3f>(Kori.data(), Kori.size());
        rightConfig->Dpos = Eigen::Map<const Eigen::Vector3f>(Dpos.data(), Dpos.size());
        rightConfig->Dori = Eigen::Map<const Eigen::Vector3f>(Dori.data(), Dori.size());
        rightConfig->desiredJointPositions = Eigen::Map<const Eigen::VectorXf>(
            desiredJointPositionRight.data(), desiredJointPositionRight.size());
        rightConfig->Knull = Eigen::Map<const Eigen::VectorXf>(knull.data(), knull.size());
        rightConfig->Dnull = Eigen::Map<const Eigen::VectorXf>(dnull.data(), dnull.size());
        rightConfig->torqueLimit = 10;

        auto controller = ctx.robot.robotUnit()->createOrReplaceNJointController(
            "NJointTaskSpaceImpedanceController", IMPEDANCE_CONTROLLER_NAME, rightConfig);

        armarx::NJointTaskSpaceImpedanceControlInterfacePrx taskSpaceImpedanceController =
            armarx::NJointTaskSpaceImpedanceControlInterfacePrx::checkedCast(controller);
        ARMARX_CHECK_NOT_NULL(taskSpaceImpedanceController);

        ARMARX_CHECK(not ctx.robot.robotUnit()->isSimulation())
            << "Cannot use impedance control in simulation!";
        // in simulation, this is not possible ...

        taskSpaceImpedanceController->activateController();
    }


} // namespace armarx::manipulation::skills
