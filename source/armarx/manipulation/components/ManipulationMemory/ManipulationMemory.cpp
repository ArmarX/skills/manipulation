/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_manipulation
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       14.12.21
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <optional>

#include <ArmarXCore/libraries/DecoupledSingleComponent/Decoupled.h>

#include <armarx/manipulation/core/Affordance.h>
#include <armarx/manipulation/core/ActionHypothesis.h>
#include <armarx/manipulation/core/ExecutableAction.h>
#include <armarx/manipulation/core/aron/ExecutableAction.aron.generated.h>
#include <armarx/manipulation/core/aron/ExecutedAction.aron.generated.h>
#include <armarx/manipulation/core/aron/ManipulationProcess.aron.generated.h>
#include <armarx/manipulation/core/SituatedAffordance.h>

#include "ManipulationMemory.h"

namespace armarx::manipulation
{
    ARMARX_DECOUPLED_REGISTER_COMPONENT(ManipulationMemory);
    std::string ManipulationMemory::getDefaultName() const
    {
        return "ManipulationMemory";
    }

    void ManipulationMemory::onInitComponent()
    {
        workingMemory().addCoreSegment("Affordances_SituatedAffordance",
                                       core::arondto::SituatedAffordance::ToAronType());
        workingMemory().addCoreSegment("Actions_Hypothesis", core::arondto::ActionHypothesis::ToAronType());
        workingMemory().addCoreSegment("Actions_Executable", core::arondto::ExecutableAction::ToAronType());
        workingMemory().addCoreSegment("Actions_Executed", core::arondto::ExecutedAction::ToAronType());
        workingMemory().addCoreSegment("Manipulation_Process", core::arondto::ManipulationProcess::ToAronType());
    }

    void ManipulationMemory::onConnectComponent()
    {

    }

    PropertyDefinitionsPtr ManipulationMemory::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr def =
                new ComponentPropertyDefinitions(getConfigIdentifier());
        workingMemory().name() = "Manipulation";
        return def;
    }
}
