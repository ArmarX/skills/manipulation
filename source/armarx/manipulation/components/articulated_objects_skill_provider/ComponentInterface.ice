/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * package    manipulation::ArticulatedObjectsSkillProvider
 * author     Fabian Reister ( fabian dot reister at kit dot edu )
 * date       2022
 * copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *            GNU General Public License
 */


#pragma once

#include <RobotAPI/interface/armem/client/MemoryListenerInterface.ice>
#include <RobotAPI/interface/skills/SkillProviderInterface.ice>

module armarx {  module manipulation {  module components {  module articulated_objects_skill_provider
{

    interface ComponentInterface extends armarx::armem::client::MemoryListenerInterface, skills::provider::dti::SkillProviderInterface
    {
	// Define your interface here.
    };

};};};};
