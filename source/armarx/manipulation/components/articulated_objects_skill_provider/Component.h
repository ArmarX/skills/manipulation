/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    manipulation::ArmarXObjects::articulated_objects_skill_provider
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


#include <ArmarXCore/core/Component.h>

#include "RobotAPI/libraries/armem_objects/client/articulated_object/ArticulatedObjectReader.h"
#include <RobotAPI/libraries/ArmarXObjects/plugins/ObjectPoseClientPlugin.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>
#include <RobotAPI/libraries/armem/client/plugins.h>
#include <RobotAPI/libraries/skills/provider/SkillProviderComponentPlugin.h>

#include "armarx/manipulation/core/Robot.h"
#include <armarx/manipulation/components/articulated_objects_skill_provider/ComponentInterface.h>


namespace armarx::manipulation::components::articulated_objects_skill_provider
{

    class Component :
        virtual public armarx::Component,
        virtual public armarx::manipulation::components::articulated_objects_skill_provider::
            ComponentInterface,
        // , virtual public armarx::DebugObserverComponentPluginUser
        // , virtual public armarx::LightweightRemoteGuiComponentPluginUser
        virtual public armarx::ArVizComponentPluginUser,
        virtual public SkillProviderComponentPluginUser,
        virtual public ObjectPoseClientPluginUser,
        virtual public armarx::armem::client::ComponentPluginUser
    {
    public:
        Component();

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;

        /// Get the component's default name.
        static std::string GetDefaultName();


    protected:
        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;


        /* (Requires armarx::LightweightRemoteGuiComponentPluginUser.)
        /// This function should be called once in onConnect() or when you
        /// need to re-create the Remote GUI tab.
        void createRemoteGuiTab();

        /// After calling `RemoteGui_startRunningTask`, this function is
        /// called periodically in a separate thread. If you update variables,
        /// make sure to synchronize access to them.
        void RemoteGui_update() override;
        */


    private:
        // Private methods go here.

        // Forward declare `Properties` if you used it before its defined.
        // struct Properties;

        /* (Requires the armarx::ArVizComponentPluginUser.)
        /// Draw some boxes in ArViz.
        void drawBoxes(const Properties& p, viz::Client& arviz);
        */


    private:
        static const std::string defaultName;


        // Private member variables go here.


        /// Properties shown in the Scenario GUI.
        struct Properties
        {

            std::string robotName = "Armar6";
        };
        Properties properties;
        /* Use a mutex if you access variables from different threads
         * (e.g. ice functions and RemoteGui_update()).
        std::mutex propertiesMutex;
        */


        /* (Requires the armarx::LightweightRemoteGuiComponentPluginUser.)
        /// Tab shown in the Remote GUI.
        struct RemoteGuiTab : armarx::RemoteGui::Client::Tab
        {
            armarx::RemoteGui::Client::LineEdit boxLayerName;
            armarx::RemoteGui::Client::IntSpinBox numBoxes;

            armarx::RemoteGui::Client::Button drawBoxes;
        };
        RemoteGuiTab tab;
        */


        /* (Requires the armarx::ArVizComponentPluginUser.)
         * When used from different threads, an ArViz client needs to be synchronized.
        /// Protects the arviz client inherited from the ArViz plugin.
        std::mutex arvizMutex;
        */

        core::Robot::Proxies robotProxies;

        std::unique_ptr<core::Robot> robot;

        armarx::armem::articulated_object::ArticulatedObjectReader articulatedObjectReader;
    };

} // namespace armarx::manipulation::components::articulated_objects_skill_provider
