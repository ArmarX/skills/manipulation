/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    manipulation::ArmarXObjects::articulated_objects_skill_provider
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "Component.h"

#include <memory>

#include "ArmarXCore/core/ManagedIceObjectPlugin.h"
#include <ArmarXCore/libraries/DecoupledSingleComponent/Decoupled.h>

#include "RobotAPI/libraries/ArmarXObjects/ObjectID.h"
#include "RobotAPI/libraries/ArmarXObjects/aron_conversions/armarx.h"
#include "RobotAPI/libraries/skills/provider/SkillDescription.h"
#include "RobotAPI/libraries/skills/provider/SkillProviderComponentPlugin.h"

#include "armarx/manipulation/skills/EstablishDoorContact.h"
#include "armarx/manipulation/skills/GraspHandle.h"
#include "armarx/manipulation/skills/MoveTCP.h"
#include "armarx/manipulation/skills/ReleaseHandle.h"
#include "armarx/manipulation/skills/ShapeHand.h"
#include <armarx/manipulation/core/aron/Hand.aron.generated.h>
#include <armarx/manipulation/skills/ApproachHandle.h>
// Include headers you only need in function definitions in the .cpp.

// #include <Eigen/Core>

// #include <SimoxUtility/color/Color.h>

namespace armarx::manipulation::skills
{
    class OpenDishwasherDoor : public armarx::skills::Skill
    {
    public:
        OpenDishwasherDoor();

    private:
        armarx::skills::Skill::MainResult main(const MainInput& in) final;
    };

    OpenDishwasherDoor::OpenDishwasherDoor() :
        Skill(armarx::skills::SkillDescription{"OpenDishwasherDoor",
                                               "Opens the dishwasher door.",
                                               {},
                                               armarx::core::time::Duration::MilliSeconds(60000),
                                               nullptr})
    {
    }

    armarx::skills::Skill::MainResult
    OpenDishwasherDoor::main(const MainInput& in)
    {
        armarx::skills::manager::dto::SkillExecutionRequest exec;
        exec.skillId = {"ActiculatedObjectsSkillProvider", skills::ShapeHand::DefaultSkillDescription().skillName};

        const ObjectID dishwasherObjectID_BO("Kitchen", "mobile-dishwasher", "0");
        
        armarx::arondto::ObjectID dishwasherObjectID;
        toAron(dishwasherObjectID, dishwasherObjectID_BO);

        const std::string dishwasherDoorNodeSetName = "dishwasher_door";

        const auto hand = armarx::manipulation::core::arondto::Hand::Right;

        {
            skills::ShapeHand::ParamType shapeHandParamsInit;
            shapeHandParamsInit.hand = hand;
            shapeHandParamsInit.fingers = 0.6; // sim: 0.5
            shapeHandParamsInit.thumb = 0;
            // shapeHandParams.timeoutMs

            exec.params = shapeHandParamsInit.toAron()->toAronDictDTO();
            manager->executeSkill(exec);
        }

        // {
        //     skills::MoveTCP::ParamType moveTCPParams;
        //     moveTCPParams.hand = hand;
        //     moveTCPParams.velocity = 30.F;

        //     core::Pose tcp_T_tcp_desired = core::Pose::Identity();

        //     const core::Pose dishwasher_T_door_pre(
        //         virtualDishwasher->getRobotNode("dishwasher_door_handle_approach_door_pre")
        //             ->getPoseInRootFrame());

        //     const core::Pose global_T_dishwasher(virtualDishwasher->getGlobalPose());
        //     const core::Pose global_T_robot(robot->getGlobalPose());

        //     tcp_T_tcp_desired.linear() = parameterization.tcp_R_tcp_desired.toRotationMatrix();

        //     // z of door frame is pointing away from door
        //     // we need to rotate this frame
        //     core::Pose frame_T_tcp = core::Pose::Identity();
        //     frame_T_tcp.linear() =
        //         Eigen::AngleAxisf(M_PIf32, Eigen::Vector3f::UnitY()).toRotationMatrix() *
        //         Eigen::AngleAxisf(-M_PI_2f32, Eigen::Vector3f::UnitZ()).toRotationMatrix();

        //     const core::Pose robot_T_tcp =
        //         global_T_robot.inverse() * global_T_dishwasher * dishwasher_T_door_pre;
        //     const core::Pose robot_T_tcp_desired = robot_T_tcp * tcp_T_tcp_desired * frame_T_tcp;


        //     moveTCPParams
        //         .robot_T_tcp_desired

        //             // moveTCPParams...
        //             exec.params = moveTCPParams.toAron()->toAronDictDTO();
        //     ownerManager->executeSkill(exec);
        // }

        {
            skills::EstablishDoorContact::ParamType establishDoorContactParams;

            establishDoorContactParams.approachVelocity = 30;
            establishDoorContactParams.contactForceThreshold = 5; // sim: 10
            establishDoorContactParams.object = dishwasherObjectID;
            establishDoorContactParams.objectNodeSet = dishwasherDoorNodeSetName;

            exec.skillId.skillName = skills::EstablishDoorContact::DefaultSkillDescription().skillName;
            exec.params = establishDoorContactParams.toAron()->toAronDictDTO();
            manager->executeSkill(exec);
        }

        {
            skills::ShapeHand::ParamType shapeHandParamsContact;
            shapeHandParamsContact.hand = hand;
            shapeHandParamsContact.fingers = 0.4; // sim: 0.3
            shapeHandParamsContact.thumb = 0;

            exec.skillId.skillName = skills::ShapeHand::DefaultSkillDescription().skillName;
            exec.params = shapeHandParamsContact.toAron()->toAronDictDTO();
            manager->executeSkill(exec);
        }

        {
            skills::ApproachHandle::ParamType approachHandleParams;
            approachHandleParams.approachVelocity =
                approachHandleParams.contactForceThreshold; // sim 50
            approachHandleParams.positionLimit = 1000; // unused
            approachHandleParams.hand = hand;

            approachHandleParams.object = dishwasherObjectID;
            approachHandleParams.objectNodeSet = dishwasherDoorNodeSetName;


            // approachHandleParams.hand = hand;
            exec.skillId.skillName = skills::ApproachHandle::DefaultSkillDescription().skillName;
            exec.params = approachHandleParams.toAron()->toAronDictDTO();
            manager->executeSkill(exec);
        }

        {
            skills::GraspHandle::ParamType graspHandleParams;
            graspHandleParams.hand = hand;

            // graspHandleParams.handle = //??
            graspHandleParams.object = dishwasherObjectID;

            exec.skillId.skillName = skills::GraspHandle::DefaultSkillDescription().skillName;
            exec.params = graspHandleParams.toAron()->toAronDictDTO();
            manager->executeSkill(exec);
        }


        return {.status = armarx::skills::TerminatedSkillStatus::Succeeded};
    }
} // namespace armarx::manipulation::skills

namespace armarx::manipulation::components::articulated_objects_skill_provider
{
    Component::Component() : articulatedObjectReader(memoryNameSystem())
    {
    }


    const std::string Component::defaultName = "ActiculatedObjectsSkillProvider";


    armarx::PropertyDefinitionsPtr
    Component::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr def =
            new armarx::ComponentPropertyDefinitions(getConfigIdentifier());

        robotProxies.registerPropertyDefinitions(def);

        // Publish to a topic (passing the TopicListenerPrx).
        // def->topic(myTopicListener);

        // Subscribe to a topic (passing the topic name).
        // def->topic<PlatformUnitListener>("MyTopic");

        // Use (and depend on) another component (passing the ComponentInterfacePrx).
        // def->component(myComponentProxy)


        // Add a required property. (The component won't start without a value being set.)
        // def->required(properties.boxLayerName, "p.box.LayerName", "Name of the box layer in ArViz.");

        // Add an optionalproperty.
        // def->optional(
        //     properties.boxLayerName, "p.box.LayerName", "Name of the box layer in ArViz.");
        // def->optional(properties.numBoxes, "p.box.Number", "Number of boxes to draw in ArViz.");

        return def;
    }


    void
    Component::onInitComponent()
    {
        // Topics and properties defined above are automagically registered.

        // Keep debug observer data until calling `sendDebugObserverBatch()`.
        // (Requies the armarx::DebugObserverComponentPluginUser.)
        // setDebugObserverBatchModeEnabled(true);


        // Skill component plugin
        skillImplementations.clear();
    }


    void
    Component::onConnectComponent()
    {

        articulatedObjectReader.connect();

        core::Robot::InjectedServices injectedServices{.proxies = robotProxies,
                                                       .memoryNameSystem = memoryNameSystem()};

        robot = std::make_unique<core::Robot>(injectedServices, properties.robotName);

        //
        // Private skills
        //

        addSkill(std::make_unique<skills::ShapeHand>(skills::ShapeHand::Context{.robot = *robot}));

        addSkill(std::make_unique<skills::MoveTCP>(
            skills::MoveTCP::Context{.robot = *robot, .arviz = arviz}));

        addSkill(
            std::make_unique<skills::EstablishDoorContact>(skills::EstablishDoorContact::Context{
                .robot = *robot,
                .articulatedObjectReader = articulatedObjectReader,
                .arviz = arviz}));

        addSkill(std::make_unique<skills::ApproachHandle>(
            skills::ApproachHandle::Context{.robot = *robot, .arviz = arviz}));

        addSkill(
            std::make_unique<skills::GraspHandle>(skills::GraspHandle::Context{.robot = *robot}));

        addSkill(std::make_unique<skills::ReleaseHandle>(
            skills::ReleaseHandle::Context{.robot = *robot, .arviz = arviz}));

        //
        // Public skills
        //

        addSkill(std::make_unique<skills::OpenDishwasherDoor>());


        // Do things after connecting to topics and components.

        /* (Requies the armarx::DebugObserverComponentPluginUser.)
        // Use the debug observer to log data over time.
        // The data can be viewed in the ObserverView and the LivePlotter.
        // (Before starting any threads, we don't need to lock mutexes.)
        {
            setDebugObserverDatafield("numBoxes", properties.numBoxes);
            setDebugObserverDatafield("boxLayerName", properties.boxLayerName);
            sendDebugObserverBatch();
        }
        */

        /* (Requires the armarx::ArVizComponentPluginUser.)
        // Draw boxes in ArViz.
        // (Before starting any threads, we don't need to lock mutexes.)
        drawBoxes(properties, arviz);
        */

        /* (Requires the armarx::LightweightRemoteGuiComponentPluginUser.)
        // Setup the remote GUI.
        {
            createRemoteGuiTab();
            RemoteGui_startRunningTask();
        }
        */
    }


    void
    Component::onDisconnectComponent()
    {
    }


    void
    Component::onExitComponent()
    {
    }


    std::string
    Component::getDefaultName() const
    {
        return Component::defaultName;
    }


    std::string
    Component::GetDefaultName()
    {
        return Component::defaultName;
    }


    /* (Requires the armarx::LightweightRemoteGuiComponentPluginUser.)
    void
    Component::createRemoteGuiTab()
    {
        using namespace armarx::RemoteGui::Client;

        // Setup the widgets.

        tab.boxLayerName.setValue(properties.boxLayerName);

        tab.numBoxes.setValue(properties.numBoxes);
        tab.numBoxes.setRange(0, 100);

        tab.drawBoxes.setLabel("Draw Boxes");

        // Setup the layout.

        GridLayout grid;
        int row = 0;
        {
            grid.add(Label("Box Layer"), {row, 0}).add(tab.boxLayerName, {row, 1});
            ++row;

            grid.add(Label("Num Boxes"), {row, 0}).add(tab.numBoxes, {row, 1});
            ++row;

            grid.add(tab.drawBoxes, {row, 0}, {2, 1});
            ++row;
        }

        VBoxLayout root = {grid, VSpacer()};
        RemoteGui_createTab(getName(), root, &tab);
    }


    void
    Component::RemoteGui_update()
    {
        if (tab.boxLayerName.hasValueChanged() || tab.numBoxes.hasValueChanged())
        {
            std::scoped_lock lock(propertiesMutex);
            properties.boxLayerName = tab.boxLayerName.getValue();
            properties.numBoxes = tab.numBoxes.getValue();

            {
                setDebugObserverDatafield("numBoxes", properties.numBoxes);
                setDebugObserverDatafield("boxLayerName", properties.boxLayerName);
                sendDebugObserverBatch();
            }
        }
        if (tab.drawBoxes.wasClicked())
        {
            // Lock shared variables in methods running in seperate threads
            // and pass them to functions. This way, the called functions do
            // not need to think about locking.
            std::scoped_lock lock(propertiesMutex, arvizMutex);
            drawBoxes(properties, arviz);
        }
    }
    */


    /* (Requires the armarx::ArVizComponentPluginUser.)
    void
    Component::drawBoxes(const Component::Properties& p, viz::Client& arviz)
    {
        // Draw something in ArViz (requires the armarx::ArVizComponentPluginUser.
        // See the ArVizExample in RobotAPI for more examples.

        viz::Layer layer = arviz.layer(p.boxLayerName);
        for (int i = 0; i < p.numBoxes; ++i)
        {
            layer.add(viz::Box("box_" + std::to_string(i))
                      .position(Eigen::Vector3f(i * 100, 0, 0))
                      .size(20).color(simox::Color::blue()));
        }
        arviz.commit(layer);
    }
    */


    ARMARX_REGISTER_COMPONENT_EXECUTABLE(Component, Component::GetDefaultName());

} // namespace armarx::manipulation::components::articulated_objects_skill_provider
