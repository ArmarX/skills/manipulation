/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    manipulation::ArmarXObjects::articulated_object_augmented_visualization
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "Component.h"

#include <algorithm>
#include <iterator>

#include <VirtualRobot/Robot.h>
#include <VirtualRobot/RobotNodeSet.h>
#include <VirtualRobot/VirtualRobot.h>
#include <VirtualRobot/XML/RobotIO.h>

#include "ArmarXCore/core/time/Clock.h"
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/libraries/DecoupledSingleComponent/Decoupled.h>

#include "RobotAPI/components/ArViz/Client/Elements.h"
#include "RobotAPI/libraries/ArmarXObjects/ObjectID.h"
#include "RobotAPI/libraries/ArmarXObjects/forward_declarations.h"
#include "RobotAPI/libraries/ArmarXObjects/plugins/ObjectPoseClientPlugin.h"

#include "armarx/manipulation/geometric_planning/ArVizDrawer.h"
#include "armarx/manipulation/geometric_planning/util.h"


namespace armarx::manipulation::components::articulated_object_augmented_visualization
{
    Component::Component()
    {
        addPlugin(articulatedObjectReaderPlugin);
    }


    const std::string Component::defaultName = "articulated_object_augmented_visualization";


    armarx::PropertyDefinitionsPtr
    Component::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr def =
            new armarx::ComponentPropertyDefinitions(getConfigIdentifier());

        def->optional(properties.drawPaths, "p.drawPaths", "");
        def->optional(properties.drawJointCoordinateSystems, "p.drawJointCoordinateSystems", "");

        return def;
    }


    void
    Component::onInitComponent()
    {
    }


    void
    Component::onConnectComponent()
    {
        ARMARX_INFO << "Visualizing paths for nodes";
        visualizeAllParametricPathsForNodes("handle");
    }


    void
    Component::onDisconnectComponent()
    {
    }


    void
    Component::onExitComponent()
    {
    }

    // the following could be used to obtain all classes of articulated objects from the database

    // std::vector<VirtualRobot::RobotPtr>
    // Component::getAllArticulatedObjects()
    // {
    //     auto& articulatedObjectReader = articulatedObjectReaderPlugin->get();

    //     const auto descriptions = articulatedObjectReader.queryDescriptions(armem::Time::Now());

    //     ARMARX_INFO << "Found " << descriptions.size() << " articulated object descriptions";

    //     for (const auto& description : descriptions)
    //     {
    //         ARMARX_INFO << "- " << description.name;
    //     }

    //     std::vector<VirtualRobot::RobotPtr> articulatedObjects;
    //     std::transform(
    //         descriptions.begin(),
    //         descriptions.end(),
    //         std::back_inserter(articulatedObjects),
    //         [](const armem::articulated_object::ArticulatedObjectDescription& description)
    //         {
    //             const auto robot =  VirtualRobot::RobotIO::loadRobot(
    //                 ArmarXDataPath::resolvePath(description.xml.serialize().path),
    //                 VirtualRobot::RobotIO::eStructure);
    //             ARMARX_CHECK_NOT_NULL(robot) << description.name << ", " << description.xml << " could not be loaded!";

    //             return robot;
    //         });

    //     return articulatedObjects;
    // }

    std::vector<VirtualRobot::RobotPtr>
    Component::getAllArticulatedObjectsInScene()
    {
        auto& articulatedObjectReader = articulatedObjectReaderPlugin->get();

        const auto objectPoses = ObjectPoseClientPluginUser::getObjectPoses();

        const auto descriptions = articulatedObjectReader.queryDescriptions(armem::Time::Now());
        ARMARX_INFO << "Found " << descriptions.size() << " articulated object descriptions";
        for (const auto& description : descriptions)
        {
            ARMARX_INFO << "- " << description.name;
        }

        std::vector<armarx::objpose::ObjectPose> articulatedObjectPoses;
        for (const auto& objectPose : objectPoses)
        {
            if (std::find_if(
                    descriptions.begin(),
                    descriptions.end(),
                    [&objectPose](
                        const armem::articulated_object::ArticulatedObjectDescription& desc) -> bool
                    { return objectPose.objectID.equalClass(armarx::ObjectID(desc.name)); }) !=
                descriptions.end())

            // if (objectPose.articulatedSimoxXmlPath.has_value())
            {
                articulatedObjectPoses.push_back(objectPose);
            }
        }


        ARMARX_INFO << "Found " << articulatedObjectPoses.size()
                    << " articulated objects in the scene";

        for (const auto& objectPose : articulatedObjectPoses)
        {
            ARMARX_INFO << "- " << objectPose.objectID;
        }

        const auto now = Clock::Now();

        std::vector<VirtualRobot::RobotPtr> articulatedObjects;
        std::transform(
            articulatedObjectPoses.begin(),
            articulatedObjectPoses.end(),
            std::back_inserter(articulatedObjects),
            [&articulatedObjectReader, &now](const armarx::objpose::ObjectPose& objectPose)
            {
                const armarx::ObjectID& objectID = objectPose.objectID;
                const std::string name = objectID.dataset() + "/" + objectID.className();

                const auto robot = articulatedObjectReader.getArticulatedObject(name, now);
                ARMARX_CHECK_NOT_NULL(robot) << name << " could not be loaded!";

                robot->setGlobalPose(objectPose.objectPoseGlobal);

                return robot;
            });

        return articulatedObjects;
    }


    std::string
    Component::getDefaultName() const
    {
        return Component::defaultName;
    }


    std::string
    Component::GetDefaultName()
    {
        return Component::defaultName;
    }


    void
    Component::visualizeAllParametricPathsForNodes(const std::string& identifier)
    {
        const std::vector<VirtualRobot::RobotPtr> articulatedObjects =
            getAllArticulatedObjectsInScene();

        geometric_planning::ArVizDrawer arvizDrawer(arviz);

        for (const std::shared_ptr<VirtualRobot::Robot>& articulatedObject : articulatedObjects)
        {
            if (properties.drawPaths)
            {
                arvizDrawer.drawPaths(articulatedObject, identifier);
            }

            if (properties.drawJointCoordinateSystems)
            {
                auto l = arviz.layer("joint_coordinate_systems_" + articulatedObject->getType() +
                                     "_" + articulatedObject->getName());
                const auto handleNames = geometric_planning::nodesMatching(
                    articulatedObject->getRobotNodeNames(), "handle");

                for (const auto& handleName : handleNames)
                {
                    l.add(
                        viz::Sphere("handle" + handleName)
                            .position(
                                articulatedObject->getRobotNode(handleName)->getGlobalPosition()));

                    const auto referenceJointNode =
                        articulatedObject->getRobotNode(handleName + "_joint_reference");

                    if (referenceJointNode)
                    {
                        l.add(viz::Pose("handle_joint_ref" + handleName)
                                  .pose(referenceJointNode->getGlobalPose())
                                  .scale(10));
                    }
                }

                arviz.commit(l);
            }
        }
    }


    ARMARX_REGISTER_COMPONENT_EXECUTABLE(Component, Component::GetDefaultName());

} // namespace armarx::manipulation::components::articulated_object_augmented_visualization
