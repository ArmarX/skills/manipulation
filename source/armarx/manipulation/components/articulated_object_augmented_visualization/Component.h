/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    manipulation::ArmarXObjects::articulated_object_augmented_visualization
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// #include <mutex>

#include <VirtualRobot/VirtualRobot.h>

#include <ArmarXCore/core/Component.h>

#include "RobotAPI/libraries/ArmarXObjects/plugins/ObjectPoseClientPlugin.h"
#include "RobotAPI/libraries/armem/client/plugins/ReaderWriterPlugin.h"
#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>
#include <RobotAPI/libraries/armem_objects/client/articulated_object/ArticulatedObjectReader.h>


namespace armarx::manipulation::components::articulated_object_augmented_visualization
{

    class Component :
        virtual public armarx::Component,
        virtual public armarx::ArVizComponentPluginUser,
        virtual public armarx::ObjectPoseClientPluginUser
    {
    public:
        Component();

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;

        /// Get the component's default name.
        static std::string GetDefaultName();


    protected:
        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;


    private:
        std::vector<VirtualRobot::RobotPtr> getAllArticulatedObjectsInScene();

        void visualizeAllParametricPathsForNodes(const std::string& identifier);


    private:
        static const std::string defaultName;


        /// Properties shown in the Scenario GUI.
        struct Properties
        {
            bool drawPaths = true;
            bool drawJointCoordinateSystems = false;
        };
        Properties properties;
       

        armem::client::plugins::ReaderWriterPlugin<
            ::armarx::armem::articulated_object::ArticulatedObjectReader>*
            articulatedObjectReaderPlugin = nullptr;
    };

} // namespace armarx::manipulation::components::articulated_object_augmented_visualization
