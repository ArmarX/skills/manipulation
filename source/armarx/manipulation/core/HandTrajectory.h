/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_manipulation
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       21.02.22
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <map>
#include <utility>
#include <vector>

#include <RobotAPI/libraries/core/FramedPose.h>
#include <RobotAPI/libraries/armem/core/forward_declarations.h>
#include <RobotAPI/libraries/armem/core/Time.h>


namespace armarx
{
    namespace manipulation::core
    {
        namespace arondto
        {
            class HandTrajectoryKeypoint;

            class HandTrajectory;
        }

        struct HandTrajectoryKeypoint
        {
            HandTrajectoryKeypoint(const armarx::FramedPose& pose, std::map<std::string, float> finger_values) : pose(
                    pose), fingerValues(std::move(finger_values))
            {
            };

            HandTrajectoryKeypoint(const Eigen::Matrix4f& pose, const std::string& frame,
                                   std::map<std::string, float> finger_values) : pose(pose, frame, ""),
                                                                                 fingerValues(std::move(finger_values))
            {
            };
            armarx::FramedPose pose;
            std::map<std::string, float> fingerValues;
        };

        struct HandTrajectory
        {
            std::vector<HandTrajectoryKeypoint> keypoints;
            armem::Duration duration;
            HandTrajectory getTransformed(Eigen::Affine3f transformation) const;
        };
    }
}
