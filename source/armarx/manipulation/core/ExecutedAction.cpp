/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_manipulation
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       08.02.22
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <utility>

#include <armarx/manipulation/core/ExecutableAction.h>

#include "ExecutedAction.h"


armarx::manipulation::core::ExecutedAction::ExecutedAction(const ExecutableAction& action,
                                                           armarx::armem::Time starttime, std::optional<armarx::armem::Time> endtime,
                                                           std::optional<bool> success, std::optional<std::string> outcome,
                                                           std::optional<std::string> comment,
                                                           std::optional<std::string> debugOutput) : action(
        std::make_unique<ExecutableAction>(action)), startTime(std::make_unique<armem::Time>(starttime)),
                                                                                                     outcome(std::move(
                                                                                                             outcome)),
                                                                                                     comment(std::move(
                                                                                                             comment)),
                                                                                                     debugOutput(
                                                                                                             std::move(
                                                                                                                     debugOutput))
{
 endTime = endtime ? std::make_unique<armem::Time>(endtime.value()) : nullptr;
 this->success = success.has_value() && success.value();
}

armarx::manipulation::core::ExecutedAction::ExecutedAction(const armarx::manipulation::core::ExecutedAction& exAction)
        : action(std::make_unique<ExecutableAction>(*exAction.action)),
          startTime(std::make_unique<armem::Time>(*exAction.startTime)), success(exAction.success),
          outcome(exAction.outcome), comment(exAction.comment), debugOutput(exAction.debugOutput)
{
    endTime = exAction.endTime ? std::make_unique<armem::Time>(*exAction.endTime) : nullptr;
}

armarx::manipulation::core::ExecutedAction&
armarx::manipulation::core::ExecutedAction::operator=(const armarx::manipulation::core::ExecutedAction& exAction)
{
    *action = *exAction.action;
    *startTime = *exAction.startTime;
    *endTime = *exAction.endTime;
    success = exAction.success;
    outcome = exAction.outcome;
    comment = exAction.comment;
    debugOutput = exAction.debugOutput;
    return *this;
}

armarx::manipulation::core::ExecutedAction::ExecutedAction(armarx::manipulation::core::ExecutedAction&&) = default;


armarx::manipulation::core::ExecutedAction&
armarx::manipulation::core::ExecutedAction::operator=(armarx::manipulation::core::ExecutedAction&&) = default;

armarx::manipulation::core::ExecutedAction::~ExecutedAction() = default;
