/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_manipulation
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       09.12.21
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ExecutableAction.h"

namespace armarx::manipulation::core
{
    ExecutableAction::ExecutableAction(std::vector<Unimanual> eefSpecific, ExecutableAction::Common shared) :
            common(std::move(shared)),
            unimanual(std::move(eefSpecific))
    {
    }

    ExecutableAction::ExecutableAction(const ExecutableAction::Unimanual& eefSpecific, ExecutableAction::Common shared) :
            common(std::move(shared)), unimanual{eefSpecific}
    {
    }

    ExecutableAction::Unimanual& ExecutableAction::Unimanual::operator=(const ExecutableAction::Unimanual& um)
    {
        if (um.hypothesis.has_value())
        {
            hypothesis.emplace(um.hypothesis.value());
        } else hypothesis = std::nullopt;
        handedness = um.handedness;
        actionTrajectory = um.actionTrajectory;
        execPose = um.execPose;
        prePose = um.prePose;
        retractPose = um.retractPose;
        return *this;
    }
}
