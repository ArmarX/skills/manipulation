/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_manipulation
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       25.11.21
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


// test includes and other stuff
#define BOOST_TEST_MODULE Manipulation::ArmarXLibraries::core
#define ARMARX_BOOST_TEST

#include <armarx/manipulation/Test.h>
#include <armarx/manipulation/core/Affordance.h>
#include <armarx/manipulation/core/AffordanceTypes.h>
#include <armarx/manipulation/core/aron_conversions.h>
#include <armarx/manipulation/core/aron/Affordance.aron.generated.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include <RobotAPI/libraries/armem/core/aron_conversions.h>
#include <RobotAPI/libraries/armem_robot/aron_conversions.h>
#include <RobotAPI/libraries/aron/common/aron_conversions/armarx.h>
#include <RobotAPI/libraries/armem_robot/aron/RobotDescription.aron.generated.h>
#include <RobotAPI/libraries/core/FramedPose.h>
#include <armarx/manipulation/core/FramedUncertainPose.h>
#include <armarx/manipulation/core/aron/FramedUncertainPose.aron.generated.h>
#include <armarx/manipulation/core/ActionHypothesis.h>
#include <armarx/manipulation/core/ExecutableAction.h>
#include <armarx/manipulation/core/ExecutedAction.h>
#include <armarx/manipulation/core/SituatedAffordance.h>
#include <armarx/manipulation/core/aron/ExecutableAction.aron.generated.h>
#include <armarx/manipulation/core/aron/ExecutedAction.aron.generated.h>
#include <armarx/manipulation/core/aron/SituatedAffordance.aron.generated.h>
#include <armarx/manipulation/core/aron/ManipulationProcess.aron.generated.h>
#include <armarx/manipulation/core/HandTrajectory.h>
#include <armarx/manipulation/core/ManipulationProcess.h>

namespace armarx::manipulation::core
{
    enum class TestTypes
    {
        A, B, C, D
    };
    enum class Test2DTypes
    {
        E, F, G, H
    };

    struct TestFactory : Factory<TestFactory, TestTypes>
    {
        explicit TestFactory(Key)
        {
        };

        virtual ~TestFactory() = default;

    };

    struct TestFactory2 : Factory<TestFactory2, TestTypes>
    {
        explicit TestFactory2(Key)
        {
        };

        virtual ~TestFactory2() = default;
    };

    template <TestTypes Type>
    struct TemplateFactory : TestFactory2::Registrar<TemplateFactory<Type>>
    {
        using RegistrarT = TestFactory2::Registrar<TemplateFactory<Type>>;

        TemplateFactory() : RegistrarT(typename RegistrarT::Registration{})
        {
        };
        static constexpr TestTypes id = Type;
    };

    template
    struct TemplateFactory<TestTypes::A>;
    template
    struct TemplateFactory<TestTypes::B>;

    struct AType : TestFactory::Registrar<AType>
    {
        using RegistrarT = TestFactory::Registrar<AType>;

        AType() : RegistrarT(Registration{})
        {
        };

        static constexpr TestTypes id = TestTypes::A;

        ~AType() override = default;
    };

    struct BType : TestFactory::Registrar<BType>
    {
        using RegistrarT = TestFactory::Registrar<BType>;

        BType() : RegistrarT(Registration{})
        {
        };

        static constexpr TestTypes id = TestTypes::B;

        ~BType() override = default;
    };

    struct CType : TestFactory::Registrar<CType>
    {
        using RegistrarT = TestFactory::Registrar<CType>;

        CType() : RegistrarT(Registration{})
        {
        };

        static constexpr TestTypes id = TestTypes::C;

        ~CType() override = default;
    };

    struct DType : TestFactory::Registrar<DType>
    {
        using RegistrarT = TestFactory::Registrar<DType>;

        DType() : RegistrarT(Registration{})
        {
        };

        static constexpr TestTypes id = TestTypes::D;

        ~DType() override = default;
    };

    struct Test2DFactory : Factory2D<Test2DFactory, TestTypes, Test2DTypes>
    {
        explicit Test2DFactory(Key)
        {
        };

        virtual ~Test2DFactory() = default;
    };

    template <TestTypes Type1, Test2DTypes Type2>
    struct Test2DPoduct : Test2DFactory::Registrar<Test2DPoduct<Type1, Type2>>
    {
        using RegistrarT = Test2DFactory::Registrar<Test2DPoduct<Type1, Type2>>;

        Test2DPoduct() : RegistrarT(typename RegistrarT::Registration{})
        {
        };
        static constexpr TestTypes idDimOne = Type1;
        static constexpr Test2DTypes idDimTwo = Type2;
    };

    template
    struct Test2DPoduct<TestTypes::A, Test2DTypes::E>;
    template
    struct Test2DPoduct<TestTypes::B, Test2DTypes::F>;
    template
    struct Test2DPoduct<TestTypes::C, Test2DTypes::G>;
    template
    struct Test2DPoduct<TestTypes::D, Test2DTypes::H>;

}
BOOST_AUTO_TEST_CASE(enumFactoryTest)
{
    using namespace armarx::manipulation::core;
    auto a = TestFactory::make(TestTypes::A);
    auto b = TestFactory::make(TestTypes::B);
    auto c = TestFactory::make(TestTypes::C);
    auto d = TestFactory::make(TestTypes::D);
    BOOST_CHECK(dynamic_cast<AType*>(a.get()));
    BOOST_CHECK(dynamic_cast<BType*>(b.get()));
    BOOST_CHECK(dynamic_cast<CType*>(c.get()));
    BOOST_CHECK(dynamic_cast<DType*>(d.get()));
    BOOST_CHECK(!dynamic_cast<AType*>(b.get()));

    auto templ_prod_a = TestFactory2::make(TestTypes::A);
    auto templ_prod_b = TestFactory2::make(TestTypes::B);
    BOOST_CHECK(dynamic_cast<TemplateFactory<TestTypes::A>*>(templ_prod_a.get()));
    BOOST_CHECK(dynamic_cast<TemplateFactory<TestTypes::B>*>(templ_prod_b.get()));
    BOOST_CHECK_THROW(TestFactory2::make(TestTypes::C), armarx::LocalException);
}

BOOST_AUTO_TEST_CASE(Factory2DTest)
{
    using namespace armarx::manipulation::core;
    using AEType = Test2DPoduct<TestTypes::A, Test2DTypes::E>;
    using BFType = Test2DPoduct<TestTypes::B, Test2DTypes::F>;
    using CGType = Test2DPoduct<TestTypes::C, Test2DTypes::G>;
    using DHType = Test2DPoduct<TestTypes::D, Test2DTypes::H>;
    auto a = Test2DFactory::make(TestTypes::A, Test2DTypes::E);
    auto b = Test2DFactory::make(TestTypes::B, Test2DTypes::F);
    auto c = Test2DFactory::make(TestTypes::C, Test2DTypes::G);
    auto d = Test2DFactory::make(TestTypes::D, Test2DTypes::H);
    BOOST_CHECK(dynamic_cast<AEType*>(a.get()));
    BOOST_CHECK(dynamic_cast<BFType*>(b.get()));
    BOOST_CHECK(dynamic_cast<CGType*>(c.get()));
    BOOST_CHECK(dynamic_cast<DHType*>(d.get()));
    BOOST_CHECK(!dynamic_cast<AType*>(b.get()));
    BOOST_CHECK(!dynamic_cast<AEType*>(b.get()));

    BOOST_CHECK_THROW(Test2DFactory::make(TestTypes::C, Test2DTypes::E), armarx::LocalException);
}

BOOST_AUTO_TEST_CASE(affordanceFactoryTest)
{
    using namespace armarx::manipulation::core;
    auto grasp = AffordanceTypeFactory::make("Grasp");
    auto push = AffordanceTypeFactory::make("Push");
    bool same_types = std::is_same_v<decltype(grasp), BaseAffordanceTypePtr>;
    BOOST_CHECK(dynamic_cast<GraspAffordanceType*>(grasp.get()));
    BOOST_CHECK(dynamic_cast<PushAffordanceType*>(push.get()));
    BOOST_CHECK(same_types);
    BOOST_CHECK(grasp->getID() == "Grasp");
    BOOST_CHECK(push->getID() == "Push");
}

BOOST_AUTO_TEST_CASE(affordanceAronConversions)
{
    using namespace armarx::manipulation::core;
    armarx::CMakePackageFinder project("armar6_rt");
    std::string xml_robot_file = project.getDataDir() + "/armar6_rt/robotmodel/Armar6-SH/Armar6-SH.xml";
    armarx::PackagePath xml_robot_file_path{"armar6_rt", "robotmodel/Armar6-SH/Armar6-SH.xml"};
    std::string action_type = "Grasp";
    std::string action_type2 = "Push";
    armarx::armem::MemoryID memory_id;
    armarx::armem::robot::RobotDescription robot_description{"Armar6", xml_robot_file_path};

    arondto::Affordance dto;
    dto.robotDescription.name = "Armar6";
    toAron(dto.robotDescription.xml, xml_robot_file_path);
    dto.actionType = action_type;
    armarx::armem::arondto::RobotDescription robot_description_dto;
    toAron(robot_description_dto, robot_description);

    Affordance bo(action_type, robot_description);
    Affordance bo2(action_type2, robot_description);
    Affordance empty_bo = fromAron(dto);
    armarx::armem::arondto::RobotDescription temp_rd;
    toAron(temp_rd, empty_bo.getRobotDescription());
    BOOST_CHECK(robot_description_dto == dto.robotDescription);
    BOOST_CHECK(temp_rd == robot_description_dto);
    BOOST_CHECK_NE(empty_bo.robot_name_helper, nullptr);
    BOOST_CHECK_EQUAL(empty_bo.affordance_type->getID(), bo.affordance_type->getID());
    BOOST_CHECK_EQUAL(empty_bo.affordance_type->getID(), action_type);
    BOOST_CHECK_EQUAL(empty_bo.getAffordanceID(), action_type);
    BOOST_CHECK_EQUAL(bo2.getAffordanceID(), action_type2);

    BOOST_CHECK(bo.getTCPName(arondto::Handedness::Right) == "Hand R TCP");
    BOOST_CHECK(bo.getTCPName(arondto::Handedness::Left) == "Hand L TCP");
    BOOST_CHECK(bo.getHandRootNode(arondto::Handedness::Right) == "Hand R Root");
    BOOST_CHECK(bo.getHandRootNode(arondto::Handedness::Left) == "Hand L Root");
    BOOST_CHECK(bo.getKinematicChainName(arondto::Handedness::Right) == "RightArm");
    BOOST_CHECK(bo.getKinematicChainName(arondto::Handedness::Left) == "LeftArm");
    BOOST_CHECK(bo.getRobotFilePath() == xml_robot_file);

    arondto::Affordance empty_dto;
    arondto::Affordance empty_dto2;
    toAron(empty_dto, bo);
    toAron(empty_dto2, bo2);
    BOOST_CHECK(empty_dto.robotDescription == robot_description_dto);
    BOOST_CHECK_EQUAL(empty_dto.actionType, dto.actionType);
    BOOST_CHECK_EQUAL(empty_dto.actionType, action_type);
    BOOST_CHECK(empty_dto2.robotDescription == robot_description_dto);
    BOOST_CHECK_EQUAL(empty_dto2.actionType, action_type2);
}

BOOST_AUTO_TEST_CASE(fupAronConversions)
{
    using namespace armarx::manipulation::core;
    armarx::FramedPose pose(Eigen::Matrix4f::Identity(), armarx::GlobalFrame, "Armar6");
    FramedUncertainPose bo_w_cov(pose, Eigen::Matrix6f::Identity());
    FramedUncertainPose bo_wo_cov(pose);
    arondto::FramedUncertainPose dto_w_cov = toAron(bo_w_cov);
    arondto::FramedUncertainPose dto_wo_cov = toAron(bo_wo_cov);
    arondto::FramedUncertainPose dto_w_cov_empty;
    arondto::FramedUncertainPose dto_wo_cov_empty;

    toAron(dto_w_cov_empty, bo_w_cov);
    toAron(dto_wo_cov_empty, bo_wo_cov);

    BOOST_CHECK(dto_w_cov == dto_w_cov_empty);
    BOOST_CHECK(dto_wo_cov == dto_wo_cov_empty);

    auto bo_w_cov_empty = fromAron(dto_w_cov);
    auto bo_wo_cov_empty = fromAron(dto_wo_cov);

    // frame
    BOOST_CHECK_EQUAL(bo_w_cov.pose->getFrame(), bo_w_cov_empty.pose->getFrame());
    BOOST_CHECK_EQUAL(bo_w_cov_empty.pose->getFrame(), armarx::GlobalFrame);
    // ref frame
    BOOST_CHECK_EQUAL(bo_w_cov.pose->agent, bo_w_cov_empty.pose->agent);
    BOOST_CHECK_EQUAL(bo_w_cov_empty.pose->agent, "Armar6");
    // pose
    BOOST_CHECK(bo_w_cov.pose->toEigen().isApprox(bo_w_cov_empty.pose->toEigen()));
    BOOST_CHECK(bo_w_cov.pose->toEigen().isApprox(Eigen::Matrix4f::Identity()));
    // cov
    BOOST_CHECK(bo_w_cov.covariance.value().isApprox(bo_w_cov_empty.covariance.value()));
    BOOST_CHECK(bo_w_cov.covariance.value().isApprox(Eigen::Matrix6f::Identity()));

    // frame
    BOOST_CHECK_EQUAL(bo_wo_cov.pose->getFrame(), bo_wo_cov_empty.pose->getFrame());
    BOOST_CHECK_EQUAL(bo_wo_cov_empty.pose->getFrame(), armarx::GlobalFrame);
    // ref frame
    BOOST_CHECK_EQUAL(bo_wo_cov.pose->agent, bo_wo_cov_empty.pose->agent);
    BOOST_CHECK_EQUAL(bo_wo_cov_empty.pose->agent, "Armar6");
    // pose
    BOOST_CHECK(bo_wo_cov.pose->toEigen().isApprox(bo_wo_cov_empty.pose->toEigen()));
    BOOST_CHECK(bo_wo_cov.pose->toEigen().isApprox(Eigen::Matrix4f::Identity()));
    // cov
    BOOST_CHECK(!bo_wo_cov.covariance.has_value());
    BOOST_CHECK(!bo_wo_cov_empty.covariance.has_value());

}

BOOST_AUTO_TEST_CASE(ahAronConversions)
{
    using namespace armarx::manipulation::core;
    armarx::CMakePackageFinder project("armar6_rt");
    armarx::PackagePath xml_robot_file_path{project.getName(), "robotmodel/Armar6-SH/Armar6-SH.xml"};
    armarx::armem::robot::RobotDescription robot_description{"Armar6", xml_robot_file_path};
    std::string action_type = "Grasp";
    Affordance affordance(action_type, robot_description);
    armarx::FramedPose pose(Eigen::Matrix4f::Identity(), armarx::GlobalFrame, "Armar6");
    FramedUncertainPose fp_w_cov(pose, Eigen::Matrix6f::Identity());

    ActionHypothesis bo_basic(affordance, fp_w_cov, 0);
    ActionHypothesis bo_w_hndnss(affordance, fp_w_cov, 0, arondto::Handedness::Right);
    ActionHypothesis bo_w_ec(affordance, fp_w_cov, 0, std::nullopt, 0.5);
    ActionHypothesis bo_w_full(affordance, fp_w_cov, 0, arondto::Handedness::Right, 0.5);

    arondto::ActionHypothesis dto_basic = toAron(bo_basic);
    arondto::ActionHypothesis dto_w_hndnss = toAron(bo_w_hndnss);
    arondto::ActionHypothesis dto_w_ec = toAron(bo_w_ec);
    arondto::ActionHypothesis dto_w_full = toAron(bo_w_full);

    ActionHypothesis bo_basic_empty = fromAron(dto_basic);
    ActionHypothesis bo_w_hndnss_empty = fromAron(dto_w_hndnss);
    ActionHypothesis bo_w_ec_empty = fromAron(dto_w_ec);
    ActionHypothesis bo_w_full_empty = fromAron(dto_w_full);


    BOOST_CHECK(!dto_basic.handedness.has_value());
    BOOST_CHECK(!dto_basic.existenceCertainty.has_value());
    BOOST_CHECK(dto_basic.framedPose == toAron(fp_w_cov));
    BOOST_CHECK(dto_basic.affordance == toAron(affordance));
    BOOST_CHECK(dto_basic.observationTime == 0);

    BOOST_CHECK(!dto_w_hndnss.existenceCertainty.has_value());
    BOOST_CHECK(dto_w_hndnss.handedness.value() == arondto::Handedness::Right);
    BOOST_CHECK(dto_w_hndnss.framedPose == toAron(fp_w_cov));
    BOOST_CHECK(dto_w_hndnss.affordance == toAron(affordance));
    BOOST_CHECK(dto_w_hndnss.observationTime == 0);

    BOOST_CHECK(!dto_w_ec.handedness.has_value());
    BOOST_CHECK(dto_w_ec.existenceCertainty.value() == 0.5);
    BOOST_CHECK(dto_w_ec.framedPose == toAron(fp_w_cov));
    BOOST_CHECK(dto_w_ec.affordance == toAron(affordance));
    BOOST_CHECK(dto_w_ec.observationTime == 0);

    BOOST_CHECK(dto_w_full.handedness.value() == arondto::Handedness::Right);
    BOOST_CHECK(dto_w_full.existenceCertainty.value() == 0.5);
    BOOST_CHECK(dto_w_full.framedPose == toAron(fp_w_cov));
    BOOST_CHECK(dto_w_full.affordance == toAron(affordance));
    BOOST_CHECK(dto_w_full.observationTime == 0);

    /// todo check bo
    {
        auto dto = toAron(bo_basic);
        auto bo_as = bo_basic;
        auto bo_cp(bo_basic);
        auto bo_mv(std::move(bo_basic));
        ARMARX_CHECK(toAron(bo_as) == dto);
        ARMARX_CHECK(toAron(bo_cp) == dto);
        ARMARX_CHECK(toAron(bo_mv) == dto);
    }
    {
        auto dto = toAron(bo_w_hndnss);
        auto bo_as = bo_w_hndnss;
        auto bo_cp(bo_w_hndnss);
        auto bo_mv(std::move(bo_w_hndnss));
        ARMARX_CHECK(toAron(bo_as) == dto);
        ARMARX_CHECK(toAron(bo_cp) == dto);
        ARMARX_CHECK(toAron(bo_mv) == dto);
    }
    {
        auto dto = toAron(bo_w_ec);
        auto bo_as = bo_w_ec;
        auto bo_cp(bo_w_ec);
        auto bo_mv(std::move(bo_w_ec));
        ARMARX_CHECK(toAron(bo_as) == dto);
        ARMARX_CHECK(toAron(bo_cp) == dto);
        ARMARX_CHECK(toAron(bo_mv) == dto);
    }
    {
        auto dto = toAron(bo_w_full);
        auto bo_as = bo_w_full;
        auto bo_cp(bo_w_full);
        auto bo_mv(std::move(bo_w_full));
        ARMARX_CHECK(toAron(bo_as) == dto);
        ARMARX_CHECK(toAron(bo_cp) == dto);
        ARMARX_CHECK(toAron(bo_mv) == dto);
    }
}

BOOST_AUTO_TEST_CASE(eaAronConversions)
{
    using namespace armarx::manipulation::core;
    armarx::CMakePackageFinder project("armar6_rt");
    armarx::PackagePath xml_robot_file_path{project.getName(), "robotmodel/Armar6-SH/Armar6-SH.xml"};
    armarx::armem::robot::RobotDescription robot_description{"Armar6", xml_robot_file_path};
    std::string action_type = "Grasp";
    Affordance affordance(action_type, robot_description);
    armarx::FramedPose pose(Eigen::Matrix4f::Identity(), armarx::GlobalFrame, "Armar6");
    armarx::FramedPose pose2(Eigen::Matrix4f::Identity(), "root", "Armar6");
    FramedUncertainPose fp_w_cov(pose, Eigen::Matrix6f::Identity());
    ActionHypothesis hypothesis(affordance, fp_w_cov, 0);
    HandTrajectory trajectory;

    ExecutableAction bo_basic(
            ExecutableAction::Unimanual{.hypothesis=hypothesis, .actionTrajectory=trajectory, .handedness=arondto::Handedness::Right},
            {.platformPose=pose, .platformPlanningStrategy="global"});
    ExecutableAction bo_basic2(
            ExecutableAction::Unimanual{.hypothesis=hypothesis, .actionTrajectory=trajectory, .handedness=arondto::Handedness::Left},
            {.platformPose=pose2, .platformPlanningStrategy="direct"});
    auto dto_basic = toAron(bo_basic);
    auto dto_basic2 = toAron(bo_basic2);

    BOOST_CHECK(dto_basic.unimanual.front().actionHypothesis == toAron(hypothesis));
    BOOST_CHECK(dto_basic.common.platformPose.value().toEigen().isApprox(Eigen::Matrix4f::Identity()));
    BOOST_CHECK(dto_basic.common.platformPose.value().frame == "Global");
    BOOST_CHECK(dto_basic2.common.platformPose.value().frame == "root");
    BOOST_CHECK(dto_basic.unimanual.front().handedness.value() == arondto::Handedness::Right);
    BOOST_CHECK(dto_basic2.unimanual.front().handedness.value() == arondto::Handedness::Left);

    auto bo2 = bo_basic;
    auto bo3(bo_basic);
    auto bo4(std::move(bo_basic));
    BOOST_CHECK(toAron(bo2) == dto_basic);
    BOOST_CHECK(toAron(bo3) == dto_basic);
    BOOST_CHECK(toAron(bo4) == dto_basic);

}

BOOST_AUTO_TEST_CASE(saAronConversions)
{
    using namespace armarx::manipulation::core;
    armarx::arondto::FrameID frame;
    frame.frame = "root";
    frame.agent = "Armar6";
    armarx::CMakePackageFinder project("armar6_rt");
    armarx::PackagePath xml_robot_file_path{project.getName(), "robotmodel/Armar6-SH/Armar6-SH.xml"};
    armarx::armem::robot::RobotDescription robot_description{"Armar6", xml_robot_file_path};
    std::string action_type = "Grasp";
    Affordance affordance(action_type, robot_description);
    auto pose = (Eigen::Quaternionf::UnitRandom() * Eigen::Translation<float, 3>(Eigen::Vector3f::Random())).matrix();
    pcl::PointCloud<pcl::PointXYZRGB> cloud;
    SituatedAffordance bo_1{frame, affordance, pose, cloud};
    SituatedAffordance bo_2{frame, std::string{GraspAffordanceType::id}, robot_description};
    auto dto_1 = toAron(bo_1);
    auto dto_2 = toAron(bo_2);

    // Check bo_1 and bo_2 are equal
    BOOST_CHECK(toAron(bo_1.affordance) == toAron(bo_2.affordance));
    BOOST_CHECK(bo_1.frame == bo_2.frame);
    BOOST_CHECK(bo_1.pointCloud->empty() && !bo_2.pointCloud.has_value());
    BOOST_CHECK(bo_1.keyPoint.value().toEigen().isApprox(pose) && !bo_2.keyPoint.has_value());
    BOOST_CHECK(dto_1.keyPoint.value().isApprox(pose) && !dto_2.keyPoint.has_value());

    BOOST_CHECK(fromAron(dto_1).pointCloud.value().empty() && !fromAron(dto_2).pointCloud.has_value());
    BOOST_CHECK(fromAron(dto_1).frame == fromAron(dto_2).frame);
    auto test_bo = fromAron(dto_1);
    BOOST_CHECK(test_bo.keyPoint.value().toEigen().isApprox(pose));
    BOOST_CHECK(!fromAron(dto_2).keyPoint.has_value());
    BOOST_CHECK(fromAron(dto_1).affordance.getAffordanceID() == fromAron(dto_2).affordance.getAffordanceID());

    auto bo2 = bo_1;
    auto bo3(bo_1);
    auto bo4(std::move(bo_1));
    BOOST_CHECK(toAron(bo2) == dto_1);
    BOOST_CHECK(toAron(bo3) == dto_1);
    BOOST_CHECK(toAron(bo4) == dto_1);

    auto bo5 = bo_2;
    auto dto5 = toAron(bo5);
    auto bo6(bo_2);
    auto bo7(std::move(bo_2));
    BOOST_CHECK(dto5.affordance == dto_2.affordance);
    BOOST_CHECK(dto5.frame == dto_2.frame);
    BOOST_CHECK(!dto5.pointCloud.has_value() && !dto_2.pointCloud.has_value());
    BOOST_CHECK(toAron(bo6) == dto_2);
    BOOST_CHECK(toAron(bo7) == dto_2);
}
// TODO: HandTrajectory tests
BOOST_AUTO_TEST_CASE(executedActionAronConversions)
{
    using namespace armarx::manipulation::core;
    armarx::CMakePackageFinder project("armar6_rt");
    armarx::PackagePath xml_robot_file_path{project.getName(), "robotmodel/Armar6-SH/Armar6-SH.xml"};
    armarx::armem::robot::RobotDescription robot_description{"Armar6", xml_robot_file_path};
    std::string action_type = "Grasp";
    Affordance affordance(action_type, robot_description);
    armarx::FramedPose pose(Eigen::Matrix4f::Identity(), armarx::GlobalFrame, "Armar6");
    FramedUncertainPose fp_w_cov(pose, Eigen::Matrix6f::Identity());
    ActionHypothesis hypothesis(affordance, fp_w_cov, 0);
    HandTrajectory trajectory;
    // TODO: check with non-empty trajectory
    ExecutableAction exe(
            ExecutableAction::Unimanual{.hypothesis=hypothesis, .actionTrajectory=trajectory, .handedness=arondto::Handedness::Right},
            {.platformPose=pose, .platformPlanningStrategy=std::nullopt});
    auto cur_time = armarx::DateTime::Now();
    ExecutedAction bo(exe, armarx::Duration::MicroSeconds(100), cur_time, true, "grasped", "test", "01234");
    auto dto = toAron(bo);
    arondto::ExecutedAction dto_manual;
    dto_manual.action = toAron(exe);
    BOOST_CHECK(dto_manual.action.common.platformPlanningStrategy == std::nullopt);
    dto_manual.startTime = armarx::DateTime(armarx::Duration::MicroSeconds(100));
    dto_manual.endTime = cur_time;
    dto_manual.success = true;
    dto_manual.outcome = "grasped";
    dto_manual.comment = "test";
    bool not_equal = dto_manual == dto;
    BOOST_CHECK(!not_equal);
    dto_manual.debugOutput = "01234";
    std::cout << dto.action.common.platformPose.value().toEigen();
    std::cout << dto_manual.action.common.platformPose.value().toEigen();
    BOOST_CHECK(dto == dto_manual);
    auto bo2 = fromAron(dto_manual);
    BOOST_CHECK(toAron(bo2) == dto);

    ExecutedAction bo_empty(exe, cur_time);
    auto dto_empty = toAron(bo_empty);
    ExecutedAction bo3 = bo_empty; // copy assignment;
    ExecutedAction bo4(bo_empty); // copy constructor;
    BOOST_CHECK(toAron(bo3) == dto_empty);
    BOOST_CHECK(toAron(bo4) == dto_empty);
    ExecutedAction bo5 = bo; // copy assignment;
    ExecutedAction bo6(bo); // copy constructor;
    BOOST_CHECK(toAron(bo5) == dto);
    BOOST_CHECK(toAron(bo6) == dto);
    ExecutedAction bo7(std::move(bo)); // move constructor
    ExecutedAction bo8(std::move(bo_empty)); // move constructor
    BOOST_CHECK(toAron(bo7) == dto);
    BOOST_CHECK(toAron(bo8) == dto_empty);

}

BOOST_AUTO_TEST_CASE(manipulationProcessAronConversions)
{
    using namespace armarx::manipulation::core;
    armarx::CMakePackageFinder project("armar6_rt");
    armarx::PackagePath xml_robot_file_path{project.getName(), "robotmodel/Armar6-SH/Armar6-SH.xml"};
    armarx::armem::robot::RobotDescription robot_description{"Armar6", xml_robot_file_path};
    std::string action_type = "Grasp";
    Affordance affordance(action_type, robot_description);
    armarx::FramedPose pose(Eigen::Matrix4f::Identity(), armarx::GlobalFrame, "Armar6");
    FramedUncertainPose fp_w_cov(pose, Eigen::Matrix6f::Identity());
    ActionHypothesis hypothesis(affordance, fp_w_cov, 0);
    ActionHypothesis hypothesis2(affordance, fp_w_cov, 100, arondto::Handedness::Left);
    HandTrajectory trajectory;
    ExecutableAction exe(
            ExecutableAction::Unimanual{.hypothesis=hypothesis, .actionTrajectory=trajectory, .handedness=arondto::Handedness::Right},
            {.platformPose=pose, .platformPlanningStrategy=std::nullopt});
    auto cur_time = armarx::DateTime::Now();
    ManipulationProcess bo{.actionHypotheses={hypothesis, hypothesis2}, .executedAction=ExecutedAction(exe,
                                                                                                       armarx::Duration::MicroSeconds(
                                                                                                               100),
                                                                                                       cur_time, true,
                                                                                                       "grasped",
                                                                                                       "test",
                                                                                                       "01234"), .pointCloud=std::nullopt};
    arondto::ManipulationProcess dto;
    dto.actionHypotheses = {toAron(hypothesis), toAron(hypothesis2)};
    dto.executedAction = toAron(
            ExecutedAction(exe, armarx::Duration::MicroSeconds(100), cur_time, true, "grasped", "test", "01234"));
    dto.pointCloud = std::nullopt;
    BOOST_CHECK(toAron(bo) == dto);
    auto bo2 = fromAron(dto);
    BOOST_CHECK(toAron(bo2) == dto);
}