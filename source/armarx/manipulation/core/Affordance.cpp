/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_manipulation
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       25.11.21
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <optional>


#include <RobotAPI/libraries/armem_robot/types.h>
#include <armarx/manipulation/core/aron/ActionHypothesis.aron.generated.h>
#include "AffordanceTypes.h"

#include "Affordance.h"

namespace armarx::manipulation::core
{


    Affordance::Affordance(const std::string& actionType, const armarx::armem::robot::RobotDescription& description) :
            robotDescription(std::make_unique<armem::robot::RobotDescription>(description))
    {
        affordance_type = AffordanceTypeFactory::make(actionType);
        robot_name_helper = RobotNameHelper::Create(robotDescription->xml.toSystemPath());
    }

    const armarx::armem::robot::RobotDescription& Affordance::getRobotDescription() const
    {
        return *robotDescription;
    }

    Affordance::Affordance(const Affordance& affordance) :
            robot_name_helper(affordance.robot_name_helper),
            affordance_type(affordance.affordance_type->clone()),
            robotDescription(std::make_unique<armem::robot::RobotDescription>(*affordance.robotDescription))
    {
    }

    Affordance& Affordance::operator=(const Affordance& affordance)
    {
        robot_name_helper = std::make_unique<RobotNameHelper>(*affordance.robot_name_helper); // deep copy
        affordance_type = affordance_type->clone();
        robotDescription = std::make_unique<armem::robot::RobotDescription>(*affordance.robotDescription);
        return *this;
    }

    RobotNameHelper::Arm Affordance::getArmNameHelper(const std::string& side)
    {
        return robot_name_helper->getArm(side);
    }

    std::optional<RobotNameHelper::Arm> Affordance::getArmNameHelper(const arondto::Handedness& handedness)
    {
        if (handedness == arondto::Handedness::Right)
        {
            return getArmNameHelper(robot_name_helper->LocationRight);
        } else if (handedness == arondto::Handedness::Left)
        {
            return getArmNameHelper(robot_name_helper->LocationLeft);
        } else
        {
            return std::nullopt;
        }
    }

    std::string Affordance::getTCPName(const std::string& side)
    {
        return getArmNameHelper(side).getTCP();
    }

    std::string Affordance::getKinematicChainName(const std::string& side)
    {
        return getArmNameHelper(side).getKinematicChain();
    }

    std::string Affordance::getHandRootNode(const std::string& side)
    {
        return getArmNameHelper(side).getHandRootNode();
    }

    std::optional<std::string> Affordance::getTCPName(const arondto::Handedness& handedness)
    {
        auto anh = getArmNameHelper(handedness);
        if (anh)
        {
            return anh->getTCP();
        } else
        {
            return std::nullopt;
        }
    }

    std::optional<std::string> Affordance::getKinematicChainName(const arondto::Handedness& handedness)
    {
        auto anh = getArmNameHelper(handedness);
        if (anh)
        {
            return anh->getKinematicChain();
        } else
        {
            return std::nullopt;
        }
    }

    std::optional<std::string> Affordance::getHandRootNode(const arondto::Handedness& handedness)
    {
        auto anh = getArmNameHelper(handedness);
        if (anh)
        {
            return anh->getHandRootNode();
        } else
        {
            return std::nullopt;
        }
    }

    std::string Affordance::getRobotFilePath()
    {
        return robotDescription->xml.toSystemPath().string();
    }

    std::string Affordance::getAffordanceID() const
    {
        return std::string(affordance_type->getID());
    }

    arondto::KnownAffordances Affordance::getAffordanceType() const
    {
        auto id = affordance_type->getID();
        return arondto::KnownAffordances().StringToEnumMap.at(std::string(id));
    }

    Affordance::Affordance(Affordance&&) noexcept = default;

    Affordance& Affordance::operator=(Affordance&&) noexcept = default;

    Affordance::~Affordance() = default;
}