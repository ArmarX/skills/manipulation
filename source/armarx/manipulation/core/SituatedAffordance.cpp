/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_manipulation
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       13.12.21
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <RobotAPI/libraries/armem_robot/types.h>

#include "SituatedAffordance.h"

namespace armarx::manipulation::core
{

    SituatedAffordance::SituatedAffordance(::armarx::arondto::FrameID framedHeader, const std::string& affordanceType,
                                           const armem::robot::RobotDescription& robotDescription,
                                           const std::optional<armem::MemoryID>& robotDescriptionID) : frame(
            std::move(framedHeader)), affordance(affordanceType, robotDescription)
    {
    }

    SituatedAffordance::SituatedAffordance(const ::armarx::arondto::FrameID& framedHeader, Affordance aff,
                                           std::optional<Eigen::Matrix4f> pose, std::optional<PointCloudT> cloud) : frame(framedHeader),
                                                                                                                    affordance(std::move(aff)),
                                                                                                                    pointCloud(std::move(cloud))
    {
        if (pose)
        {
            keyPoint = std::make_optional<FramedPose>(pose.value(), framedHeader.frame,
                                                      framedHeader.agent);
        }
    }
}