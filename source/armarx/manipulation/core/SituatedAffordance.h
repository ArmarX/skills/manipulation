/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_manipulation
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       13.12.21
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <utility>

#include <RobotAPI/libraries/core/FramedPose.h>
#include <RobotAPI/libraries/aron/common/aron/framed.aron.generated.h>
#include <armarx/manipulation/core/aron/SituatedAffordance.aron.generated.h>
#include <armarx/manipulation/core/Affordance.h>

namespace armarx::manipulation::core
{
    struct SituatedAffordance
    {
    public:
        using PointCloudT = pcl::PointCloud<pcl::PointXYZRGB>;

        SituatedAffordance(const ::armarx::arondto::FrameID& framedHeader, Affordance aff,
                           std::optional<Eigen::Matrix4f> pose = std::nullopt,
                           std::optional<PointCloudT> cloud = std::nullopt);

        SituatedAffordance(::armarx::arondto::FrameID framedHeader, const std::string& affordanceType,
                           const armem::robot::RobotDescription& robotDescription,
                           const std::optional<armem::MemoryID>& robotDescriptionID = std::nullopt);

        ::armarx::arondto::FrameID frame;
        Affordance affordance;
        std::optional<armarx::FramedPose> keyPoint;
        std::optional<PointCloudT> pointCloud;

    };
}


