/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Robot.h"

namespace armarx::manipulation::core
{

    Robot::Robot(const InjectedServices& services, const std::string& name) :
        services(services), robotName(name), robotReader(services.memoryNameSystem)
    {
        robotReader.connect();

        const auto timestamp = Clock::Now();
        virtualRobot = robotReader.getSynchronizedRobot(name, timestamp);

        syncTime = timestamp;

        nameHelper = armarx::RobotNameHelper::Create(virtualRobot->getFilename(), nullptr);

        leftArmHelper = nameHelper->getArm(SideNames.to_name(Side::Left));
        rightArmHelper = nameHelper->getArm(SideNames.to_name(Side::Right));

        leftRobotArmHelper = leftArmHelper.addRobot(virtualRobot);
        rightRobotArmHelper = rightArmHelper.addRobot(virtualRobot);
    }



    std::optional<Robot::ForceTorque>
    Robot::getForceTorqueInHand(const Hand hand, const armarx::core::time::DateTime& timestamp)
    {
        const armem::robot::RobotDescription desc{.name = robotName};

        const std::optional<
            std::map<armem::robot_state::RobotReader::Hand, armem::robot::ForceTorque>>
            ft = robotReader.queryForceTorque(desc, timestamp);

        if (not ft.has_value())
        {
            return std::nullopt;
        }

        armem::robot::ForceTorque ftVal;
        std::string frame;

        switch (hand)
        {
            case Hand::Left:
                ftVal = ft->at(armem::robot_state::RobotReader::Hand::Left);
                frame = leftArmHelper.getForceTorqueSensorFrame();
                break;
            case Hand::Right:
                ftVal = ft->at(armem::robot_state::RobotReader::Hand::Right);
                frame = rightArmHelper.getForceTorqueSensorFrame();
                break;
        }

        return ForceTorque{
            .force = armarx::FramedDirection(ftVal.force, frame, robotName),
            .torque = armarx::FramedDirection(ftVal.torque, frame, robotName),
        };
    }


    RobotUnitInterfacePrx
    Robot::robotUnit() const
    {
        return services.proxies.robotUnitPrx;
    }


    HandUnitInterfacePrx
    Robot::handUnit(Hand hand) const
    {
        armarx::HandUnitInterfacePrx handUnit;

        switch (hand)
        {
            case Hand::Left:
                handUnit = services.proxies.handUnitLeft;
                break;
            case Hand::Right:
                handUnit = services.proxies.handUnitRight;
                break;
        }

        return handUnit;
    }


    armem::client::MemoryNameSystem&
    Robot::memoryNameSystem()
    {
        return services.memoryNameSystem;
    }


    const std::string&
    Robot::name() const
    {
        return robotName;
    }


    const armarx::core::time::DateTime&
    Robot::synchronizationTime() const
    {
        return syncTime;
    }


    bool
    Robot::synchronize(const armarx::core::time::DateTime& timestamp)
    {
        if (robotReader.synchronizeRobot(*virtualRobot, timestamp))
        {
            syncTime = timestamp;
            return true;
        }

        return false;
    }


    const VirtualRobot::RobotPtr&
    Robot::robot() const
    {
        return virtualRobot;
    }


    VirtualRobot::RobotNodeSetPtr
    Robot::arm(const Arm arm) const
    {
        return virtualRobot->getRobotNodeSet(ArmNames.to_name(arm));
    }

    const armarx::RobotNameHelper::RobotArm&
    Robot::robotArmHelper(Arm arm) const
    {
        const armarx::RobotNameHelper::RobotArm* robotArm = nullptr;

        switch (arm)
        {
            case Arm::Left:
                robotArm = &leftRobotArmHelper;
                break;
            case Arm::Right:
                robotArm = &rightRobotArmHelper;
                break;
        }

        ARMARX_CHECK_NOT_NULL(robotArm);

        return *robotArm;
    }
    
    const armarx::RobotNameHelper::Arm& Robot::armHelper(Arm arm) const
    {
        const armarx::RobotNameHelper::Arm* armHelper = nullptr;

        switch (arm)
        {
            case Arm::Left:
                armHelper = &leftArmHelper;
                break;
            case Arm::Right:
                armHelper = &rightArmHelper;
                break;
        }

        ARMARX_CHECK_NOT_NULL(armHelper);

        return *armHelper;
    }


} // namespace armarx::manipulation::core
