/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <optional>
#include <string>

#include <VirtualRobot/VirtualRobot.h>

#include "ArmarXCore/core/application/properties/forward_declarations.h"
#include "ArmarXCore/core/exceptions/LocalException.h"
#include "ArmarXCore/core/time/Clock.h"
#include "ArmarXCore/core/time/DateTime.h"
#include "ArmarXCore/core/time/ice_conversions.h"

#include "RobotAPI/interface/units/HandUnitInterface.h"
#include "RobotAPI/interface/units/RobotUnit/RobotUnitInterface.h"
#include "RobotAPI/libraries/RobotStatechartHelpers/RobotNameHelper.h"
#include "RobotAPI/libraries/armem/client/MemoryNameSystem.h"
#include "RobotAPI/libraries/armem/core/Time.h"
#include "RobotAPI/libraries/armem_robot_state/client/common/VirtualRobotReader.h"
#include "RobotAPI/libraries/core/FramedPose.h"

#include <armarx/manipulation/core/arm.h>


namespace armarx::manipulation::core
{
    class Robot
    {
    public:
        struct Proxies
        {
            armarx::RobotUnitInterfacePrx robotUnitPrx;

            armarx::HandUnitInterfacePrx handUnitLeft;
            armarx::HandUnitInterfacePrx handUnitRight;

            void
            registerPropertyDefinitions(const PropertyDefinitionsPtr& def)
            {
                def->component(robotUnitPrx);

                def->component(handUnitLeft, "LeftHandUnit", "LeftHandUnit");
                def->component(handUnitRight, "RightHandUnit", "RightHandUnit");
            }
        };

        struct InjectedServices
        {
            Proxies proxies;
            armem::client::MemoryNameSystem& memoryNameSystem;
        };

        Robot(const InjectedServices& services, const std::string& name);

        struct ForceTorque
        {
            armarx::FramedDirection force;
            armarx::FramedDirection torque;
        };

        std::optional<ForceTorque>
        getForceTorqueInHand(Hand hand, const armarx::DateTime& timestamp = armarx::Clock::Now());


        armarx::RobotUnitInterfacePrx robotUnit() const;
        armarx::HandUnitInterfacePrx handUnit(Hand hand) const;
        armem::client::MemoryNameSystem& memoryNameSystem();

        const std::string& name() const;

        const armarx::DateTime& synchronizationTime() const;
        [[nodiscard]] bool synchronize(const armarx::DateTime& timestamp = armarx::Clock::Now());

        const VirtualRobot::RobotPtr& robot() const;

        VirtualRobot::RobotNodeSetPtr arm(Arm arm) const;

        const armarx::RobotNameHelper::RobotArm& robotArmHelper(Arm arm) const; 
        const armarx::RobotNameHelper::Arm& armHelper(Arm arm) const; 

    private:
        InjectedServices services;
        const std::string robotName;

        armarx::DateTime syncTime;

        VirtualRobot::RobotPtr virtualRobot;

        armem::robot_state::VirtualRobotReader robotReader;

        armarx::RobotNameHelperPtr nameHelper;

        armarx::RobotNameHelper::Arm leftArmHelper;
        armarx::RobotNameHelper::Arm rightArmHelper;

        armarx::RobotNameHelper::RobotArm leftRobotArmHelper;
        armarx::RobotNameHelper::RobotArm rightRobotArmHelper;
    };
} // namespace armarx::manipulation::core


namespace armarx::manipulation
{
    using core::Robot;
}
