/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_manipulation
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       02.12.21
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ActionHypothesis.h"
#include "aron_conversions.h"

#include <armarx/manipulation/core/Affordance.h>
#include <armarx/manipulation/core/FramedUncertainPose.h>
#include <armarx/manipulation/core/aron/ActionHypothesis.aron.generated.h>
#include <RobotAPI/libraries/armem_robot/types.h>
#include <RobotAPI/libraries/core/FramedPose.h>

#include <memory>

namespace armarx::manipulation::core
{
    ActionHypothesis::ActionHypothesis(const Affordance& aff, const FramedUncertainPose& pose, long obsTime,
                                       std::optional<arondto::Handedness> handed, std::optional<float> existCert) :
            affordance(std::make_unique<Affordance>(aff)),
            framedPose(std::make_unique<FramedUncertainPose>(pose)),
            handedness(std::move(handed)),
            existenceCertainty(existCert),
            observationTime(obsTime)
    {
    }

    ActionHypothesis::ActionHypothesis(const std::string& actionType,
                                       const armem::robot::RobotDescription& robotDescription, const FramedPose& pose,
                                       long obsTime, std::optional<Eigen::Matrix6f> poseUncertainty,
                                       std::optional<arondto::Handedness> handed, std::optional<float> existCert) :
            affordance(std::make_unique<Affordance>(actionType, robotDescription)),
            framedPose(std::make_unique<FramedUncertainPose>(pose, std::move(poseUncertainty))),
            handedness(std::move(handed)),
            existenceCertainty(existCert),
            observationTime(obsTime)
    {
    }

    ActionHypothesis::ActionHypothesis(const ActionHypothesis& hypothesis) :
            affordance(std::make_unique<Affordance>(*hypothesis.affordance)),
            framedPose(std::make_unique<FramedUncertainPose>(*hypothesis.framedPose)),
            handedness(hypothesis.handedness),
            existenceCertainty(hypothesis.existenceCertainty),
            observationTime(hypothesis.observationTime)
    {
    }

    ActionHypothesis& ActionHypothesis::operator=(const ActionHypothesis& hypothesis)
    {
        affordance = std::make_unique<Affordance>(*hypothesis.affordance);
        framedPose = std::make_unique<FramedUncertainPose>(*hypothesis.framedPose);
        handedness = hypothesis.handedness;
        existenceCertainty = hypothesis.existenceCertainty;
        observationTime = hypothesis.observationTime;
        return *this;
    }

    std::string ActionHypothesis::getTCPName() const
    {
        return affordance->getTCPName(*handedness).value();
    }

    std::string ActionHypothesis::getKinematicChainName() const
    {
        return affordance->getKinematicChainName(*handedness).value();
    }

    std::string ActionHypothesis::getHandRootNode() const
    {
        return affordance->getHandRootNode(*handedness).value();
    }

    std::string ActionHypothesis::getAffordanceID() const
    {
        return affordance->getAffordanceID();
    }

    arondto::KnownAffordances ActionHypothesis::getAffordanceType() const
    {
        return affordance->getAffordanceType();
    }

    std::string ActionHypothesis::getRobotFilePath() const
    {
        return affordance->getRobotFilePath();
    }

    armarx::FramedPose ActionHypothesis::getExecutionPose() const
    {
        return toFramedPose(*framedPose);
    }

    ActionHypothesis& ActionHypothesis::operator=(ActionHypothesis&&) = default;

    ActionHypothesis::ActionHypothesis(ActionHypothesis&&) = default;

    ActionHypothesis::~ActionHypothesis() = default;
}