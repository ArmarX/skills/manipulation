/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_manipulation
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       25.11.21
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <memory>
#include <map>
#include <iostream>

#include <armarx/manipulation/core/aron/Affordance.aron.generated.h>

#include <ArmarXCore/util/CPPUtility/SelfRegisteringFactory.h>

namespace armarx::manipulation::core
{
    // TODO: maybe rename to ActionTypes?
    namespace detail
    {
        template <arondto::KnownAffordances::ImplEnum T>
        struct toString
        {
        };
        template <>
        struct toString<arondto::KnownAffordances::ImplEnum::Grasp>
        {
            static constexpr std::string_view value{"Grasp"};
        };
        template <>
        struct toString<arondto::KnownAffordances::ImplEnum::Push>
        {
            static constexpr std::string_view value{"Push"};
        };
        template <>
        struct toString<arondto::KnownAffordances::ImplEnum::Pull>
        {
            static constexpr std::string_view value{"Pull"};
        };
        template <>
        struct toString<arondto::KnownAffordances::ImplEnum::Place>
        {
            static constexpr std::string_view value{"Place"};
        };
    }
    struct AffordanceTypeFactory : Factory<AffordanceTypeFactory, std::string_view>
    {
        explicit AffordanceTypeFactory(Key)
        {
        };

        virtual std::unique_ptr<AffordanceTypeFactory> clone() const = 0;

        virtual ~AffordanceTypeFactory() = default;

    };

    using BaseAffordanceType = AffordanceTypeFactory;
    using BaseAffordanceTypePtr = std::unique_ptr<BaseAffordanceType>;

    template <arondto::KnownAffordances::ImplEnum AffordanceT>
    struct AffordanceTypeProduct : AffordanceTypeFactory::Registrar<AffordanceTypeProduct<AffordanceT>>
    {
        using RegistrarT = AffordanceTypeFactory::Registrar<AffordanceTypeProduct<AffordanceT>>;

        AffordanceTypeProduct() : RegistrarT(typename RegistrarT::Registration{})
        {
        };

        [[nodiscard]] std::unique_ptr<AffordanceTypeFactory> clone() const override
        {
            return std::make_unique<AffordanceTypeProduct<AffordanceT>>(*this);
        }

        static constexpr std::string_view id = detail::toString<AffordanceT>::value;


        ~AffordanceTypeProduct() override = default;
    };

    using GraspAffordanceType = AffordanceTypeProduct<arondto::KnownAffordances::Grasp>;
    using PushAffordanceType = AffordanceTypeProduct<arondto::KnownAffordances::Push>;
    using PlaceAffordanceType = AffordanceTypeProduct<arondto::KnownAffordances::Place>;
    using PullAffordanceType = AffordanceTypeProduct<arondto::KnownAffordances::Pull>;
    extern template
    struct AffordanceTypeProduct<arondto::KnownAffordances::Grasp>;
    extern template
    struct AffordanceTypeProduct<arondto::KnownAffordances::Place>;
    extern template
    struct AffordanceTypeProduct<arondto::KnownAffordances::Pull>;
    extern template
    struct AffordanceTypeProduct<arondto::KnownAffordances::Push>;

    //    struct GraspAffordanceType : AffordanceTypeFactory::Registrar<GraspAffordanceType>
    //    {
    //        using RegistrarT = AffordanceTypeFactory::Registrar<GraspAffordanceType>;
    //        GraspAffordanceType() : RegistrarT(Registration{}) {};
    //        [[nodiscard]] std::unique_ptr<AffordanceTypeFactory> clone() const override
    //        {
    //            return std::make_unique<GraspAffordanceType>( *this );
    //        }
    //        static constexpr char id[] = "Grasp";
    //
    //        ~GraspAffordanceType() override = default;
    //    };
    //
    //    struct PushAffordanceType : AffordanceTypeFactory::Registrar<PushAffordanceType>
    //    {
    //        using RegistrarT = AffordanceTypeFactory::Registrar<PushAffordanceType>;
    //        PushAffordanceType() : RegistrarT(Registration{}) {};
    //        [[nodiscard]] std::unique_ptr<AffordanceTypeFactory> clone() const override
    //        {
    //            return std::make_unique<PushAffordanceType>( *this );
    //        }
    //        static constexpr char id[] = "Push";
    //
    //        ~PushAffordanceType() override = default;
    //    };
    //
    //    struct PullAffordanceType : AffordanceTypeFactory::Registrar<PullAffordanceType>
    //    {
    //        using RegistrarT = AffordanceTypeFactory::Registrar<PullAffordanceType>;
    //        PullAffordanceType() : RegistrarT(Registration{}) {};
    //        [[nodiscard]] std::unique_ptr<AffordanceTypeFactory> clone() const override
    //        {
    //            return std::make_unique<PullAffordanceType>( *this );
    //        }
    //        static constexpr char id[] = "Pull";
    //
    //        ~PullAffordanceType() override = default;
    //    };
    //
    //    struct PlaceAffordanceType : AffordanceTypeFactory::Registrar<PlaceAffordanceType>
    //    {
    //        using RegistrarT = AffordanceTypeFactory::Registrar<PlaceAffordanceType>;
    //        PlaceAffordanceType() : RegistrarT(Registration{}) {};
    //        [[nodiscard]] std::unique_ptr<AffordanceTypeFactory> clone() const override
    //        {
    //            return std::make_unique<PlaceAffordanceType>( *this );
    //        }
    //        static constexpr char id[] = "Pull";
    //
    //        ~PlaceAffordanceType() override = default;
    //    };
}

