/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_manipulation
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       25.11.21
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <optional>
#include <RobotAPI/libraries/core/FramedPose.h>
#include "FramedUncertainPose.h"

namespace armarx::manipulation::core
{
    FramedUncertainPose::FramedUncertainPose(const FramedPose& framedPose,
                                             std::optional<Eigen::Matrix6f> poseUncertainty) : pose(
            std::make_unique<FramedPose>(framedPose)), covariance(std::move(poseUncertainty))
    {
    }

    FramedUncertainPose::FramedUncertainPose(const FramedUncertainPose& framedPose) : pose(
            std::make_unique<FramedPose>(*framedPose.pose)), covariance(framedPose.covariance) {}


    FramedUncertainPose& FramedUncertainPose::operator=(const FramedUncertainPose& framedPose)
    {
        pose = std::make_unique<FramedPose>(*framedPose.pose);
        covariance = framedPose.covariance;
        return *this;
    }

    FramedUncertainPose::FramedUncertainPose(FramedUncertainPose&&) = default;

    FramedUncertainPose& FramedUncertainPose::operator=(FramedUncertainPose&&) = default;

    FramedUncertainPose::~FramedUncertainPose() = default;
}