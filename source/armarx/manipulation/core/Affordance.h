/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_manipulation
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       25.11.21
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <utility>
#include <optional>

#include <RobotAPI/libraries/armem/core/MemoryID.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/RobotNameHelper.h>

namespace armarx::armem::robot
{
    struct RobotDescription;
}

namespace armarx::manipulation::core
{
    namespace arondto
    {
        class Handedness;

        class KnownAffordances;
    }

    struct AffordanceTypeFactory;
    using BaseAffordanceTypePtr = std::unique_ptr<AffordanceTypeFactory>;

    class Affordance
    {
    public:
        RobotNameHelperPtr robot_name_helper;
        BaseAffordanceTypePtr affordance_type;

        Affordance(const std::string& actionType, const armem::robot::RobotDescription& description);

        Affordance(const Affordance&);

        Affordance& operator=(const Affordance&);

        Affordance(Affordance&&) noexcept;

        Affordance& operator=(Affordance&&) noexcept;

        ~Affordance();

        const armem::robot::RobotDescription& getRobotDescription() const;

        RobotNameHelper::Arm getArmNameHelper(const std::string& side);

        std::optional<RobotNameHelper::Arm> getArmNameHelper(const arondto::Handedness& handedness);

        std::string getTCPName(const std::string& side);

        std::string getKinematicChainName(const std::string& side);

        std::string getHandRootNode(const std::string& side);

        std::optional<std::string> getTCPName(const arondto::Handedness& handedness);

        std::optional<std::string> getKinematicChainName(const arondto::Handedness& handedness);

        std::optional<std::string> getHandRootNode(const arondto::Handedness& handedness);

        std::string getRobotFilePath();

        std::string getAffordanceID() const;

        arondto::KnownAffordances getAffordanceType() const;

        // TODO: Execution Prototype?
    private:
        std::unique_ptr<armem::robot::RobotDescription> robotDescription;
    };
}

