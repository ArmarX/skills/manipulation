/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_manipulation
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       25.11.21
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <armarx/manipulation/core/forward_declarations.h>

namespace armarx::manipulation::core
{

    armarx::FramedPose toFramedPose(const arondto::FramedUncertainPose& pose);
    armarx::FramedPose toFramedPose(const FramedUncertainPose& pose);

    arondto::Affordance toAron(const Affordance& bo);
    void toAron(arondto::Affordance& dto, const Affordance& bo);
    Affordance fromAron(const arondto::Affordance& dto);
    void fromAron(const arondto::Affordance& dto, Affordance& bo);

    arondto::FramedUncertainPose toAron(const FramedUncertainPose& bo);
    void toAron(arondto::FramedUncertainPose& dto, const FramedUncertainPose& bo);
    FramedUncertainPose fromAron(const arondto::FramedUncertainPose& dto);
    void fromAron(const arondto::FramedUncertainPose& dto, FramedUncertainPose& bo);

    arondto::ActionHypothesis toAron(const ActionHypothesis& bo);
    void toAron(arondto::ActionHypothesis& dto, const ActionHypothesis& bo);
    ActionHypothesis fromAron(const arondto::ActionHypothesis& dto);
    void fromAron(const arondto::ActionHypothesis& dto, ActionHypothesis& bo);

    arondto::ExecutableAction toAron(const ExecutableAction& bo);
    void toAron(arondto::ExecutableAction& dto, const ExecutableAction& bo);
    ExecutableAction fromAron(const arondto::ExecutableAction& dto);
    void fromAron(const arondto::ExecutableAction& dto, ExecutableAction& bo);

    arondto::ExecutedAction toAron(const ExecutedAction& bo);
    void toAron(arondto::ExecutedAction& dto, const ExecutedAction& bo);
    ExecutedAction fromAron(const arondto::ExecutedAction& dto);
    void fromAron(const arondto::ExecutedAction& dto, ExecutedAction& bo);

    arondto::HandTrajectoryKeypoint toAron(const HandTrajectoryKeypoint& bo);
    void toAron(arondto::HandTrajectoryKeypoint& dto, const HandTrajectoryKeypoint& bo);
    HandTrajectoryKeypoint fromAron(const arondto::HandTrajectoryKeypoint& dto);
    void fromAron(const arondto::HandTrajectoryKeypoint& dto, HandTrajectoryKeypoint& bo);
    
    arondto::HandTrajectory toAron(const HandTrajectory& bo);
    void toAron(arondto::HandTrajectory& dto, const HandTrajectory& bo);
    HandTrajectory fromAron(const arondto::HandTrajectory& dto);
    void fromAron(const arondto::HandTrajectory& dto, HandTrajectory& bo);
    
    arondto::SituatedAffordance toAron(const SituatedAffordance& bo);
    void toAron(arondto::SituatedAffordance& dto, const SituatedAffordance& bo);
    SituatedAffordance fromAron(const arondto::SituatedAffordance& dto);
    void fromAron(const arondto::SituatedAffordance& dto, SituatedAffordance& bo);

    arondto::ManipulationProcess toAron(const ManipulationProcess& bo);
    void toAron(arondto::ManipulationProcess& dto, const ManipulationProcess& bo);
    ManipulationProcess fromAron(const arondto::ManipulationProcess& dto);
    void fromAron(const arondto::ManipulationProcess& dto, ManipulationProcess& bo);

    void fromAron(const arondto::Arm& dto, Arm& bo);
    void fromAron(const arondto::Hand& dto, Hand& bo);

    namespace arondto
    {
        using ::armarx::manipulation::core::fromAron;
    }
}
