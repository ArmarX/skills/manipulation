/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "arm.h"

namespace armarx::manipulation::core
{

    // hand

    Hand
    otherHand(const Hand hand)
    {
        Hand otherHand{};
        switch (hand)
        {
            case Hand::Left:
                otherHand = Hand::Right;
                break;
            case Hand::Right:
                otherHand = Hand::Left;
                break;
        }

        return otherHand;
    }
    
    Side otherSide(Side hand) 
    {
         Side otherSide{};
        switch (hand)
        {
            case Side::Left:
                otherSide = Side::Right;
                break;
            case Side::Right:
                otherSide = Side::Left;
                break;
        }

        return otherSide;
    }


    // arm

    std::string
    getArmName(const Hand hand)
    {
        return HandNames.to_name(hand) + "Arm";
    }

    std::string
    getEndeffectorName(const Hand hand)
    {
        std::string eefName;

        switch (hand)
        {
            case Hand::Left:
                eefName = "Hand_L_EEF";
                break;
            case Hand::Right:
                eefName = "Hand_R_EEF";
                break;
        }

        return eefName;
    }
    
    Hand getHand(Arm arm) 
    {
        Hand hand{};
        switch (arm)
        {
            case Arm::Left:
                hand = Hand::Right;
                break;
            case Arm::Right:
                hand = Hand::Left;
                break;
        }

        return hand;
    }
    
    Arm getArm(Hand hand) 
    {
        Arm arm{};
        switch (hand)
        {
            case Hand::Left:
                arm = Arm::Right;
                break;
            case Hand::Right:
                arm = Arm::Left;
                break;
        }

        return arm;
    }


} // namespace armarx::manipulation::core
