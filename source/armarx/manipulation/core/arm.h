/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <SimoxUtility/meta/enum/EnumNames.hpp>


namespace armarx::manipulation::core
{

    // hand
    enum class Hand
    {
        Left,
        Right
    };

    Hand otherHand(Hand hand);


    inline const simox::meta::EnumNames<Hand> HandNames = {{Hand::Left, "Left"},
                                                           {Hand::Right, "Right"}};


    enum class Side
    {
        Left,
        Right
    };

    Side otherSide(Side hand);


    inline const simox::meta::EnumNames<Side> SideNames = {{Side::Left, "Left"},
                                                           {Side::Right, "Right"}};


    // arm
    enum class Arm
    {
        Left,
        Right
    };

    inline const simox::meta::EnumNames<Arm> ArmNames = {{Arm::Left, "LeftArm"},
                                                           {Arm::Right, "RightArm"}};


    std::string getArmName(Hand hand);
    
    std::string getEndeffectorName(Hand hand);

    Hand getHand(Arm arm);
    Arm getArm(Hand hand);


} // namespace armarx::manipulation::core

namespace armarx::manipulation
{
    using core::Arm;
    using core::Hand;
    using core::HandNames;
    using core::otherHand;
    using core::getArmName;
    using core::getEndeffectorName;
    using core::getHand;
    using core::getArm;

} // namespace armarx::manipulation
