/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_manipulation
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       25.11.21
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <RobotAPI/libraries/armem_robot/types.h>
#include <RobotAPI/libraries/armem_robot/aron_conversions.h>
#include <RobotAPI/libraries/aron/common/aron_conversions.h>
#include <armarx/manipulation/core/aron/Affordance.aron.generated.h>
#include <armarx/manipulation/core/aron/ActionHypothesis.aron.generated.h>
#include <armarx/manipulation/core/aron/ExecutableAction.aron.generated.h>
#include <armarx/manipulation/core/aron/ExecutedAction.aron.generated.h>
#include <armarx/manipulation/core/aron/FramedUncertainPose.aron.generated.h>
#include <armarx/manipulation/core/aron/HandTrajectory.aron.generated.h>
#include <armarx/manipulation/core/aron/SituatedAffordance.aron.generated.h>
#include <armarx/manipulation/core/aron/Arm.aron.generated.h>
#include <armarx/manipulation/core/aron/Hand.aron.generated.h>
#include <armarx/manipulation/core/aron/ManipulationProcess.aron.generated.h>
#include <armarx/manipulation/core/Affordance.h>
#include <armarx/manipulation/core/ActionHypothesis.h>
#include <armarx/manipulation/core/AffordanceTypes.h>
#include <armarx/manipulation/core/ExecutableAction.h>
#include <armarx/manipulation/core/ExecutedAction.h>
#include <armarx/manipulation/core/FramedUncertainPose.h>
#include <armarx/manipulation/core/HandTrajectory.h>
#include <armarx/manipulation/core/SituatedAffordance.h>
#include <armarx/manipulation/core/ManipulationProcess.h>
#include <armarx/manipulation/core/arm.h>
#include <armarx/manipulation/meta/transform_container.h>

#include "aron_conversions.h"

namespace armarx::manipulation::core
{
    // namespace aron_helpers
    //    {
    armarx::FramedPose toFramedPose(const arondto::FramedUncertainPose& pose)
    {
        // TODO: should agent be reference frame. In FramedPose agent is not used for anything (as far as I can tell)
        return {pose.uncertainPose.pose, pose.header.frame, pose.header.agent};
    }

    armarx::FramedPose toFramedPose(const FramedUncertainPose& pose)
    {
        return toFramedPose(toAron(pose));
    }

    arondto::Affordance toAron(const Affordance& bo)
    {
        arondto::Affordance aff;
        toAron(aff, bo);
        return aff;
    }

    Affordance fromAron(const arondto::Affordance& dto)
    {
        armem::robot::RobotDescription description;
        armem::robot::fromAron(dto.robotDescription, description);
        return {dto.actionType, description};
    }

    arondto::FramedUncertainPose toAron(const FramedUncertainPose& bo)
    {
        arondto::FramedUncertainPose pose;
        toAron(pose, bo);
        return pose;
    }

    FramedUncertainPose fromAron(const arondto::FramedUncertainPose& dto)
    {
        return {toFramedPose(dto), dto.uncertainPose.covariance};
    }

    arondto::ActionHypothesis toAron(const ActionHypothesis& bo)
    {
        arondto::ActionHypothesis dto;
        toAron(dto, bo);
        return dto;
    }

    ActionHypothesis fromAron(const arondto::ActionHypothesis& dto)
    {
        return {fromAron(dto.affordance), fromAron(dto.framedPose), dto.observationTime, dto.handedness,
                dto.existenceCertainty};
    }

    arondto::ExecutableAction toAron(const ExecutableAction& bo)
    {
        arondto::ExecutableAction dto;
        toAron(dto, bo);
        return dto;
    }

    ExecutableAction fromAron(const arondto::ExecutableAction& dto)
    {
        std::optional<armarx::FramedPose> platformPose;

        if (dto.common.platformPose)
        {
            armarx::FramedPose tempPose;
            fromAron(dto.common.platformPose.value(), tempPose);
            // TODO: this is not efficient at all
            platformPose = tempPose;
        }
        ARMARX_CHECK_NOT_EMPTY(dto.unimanual);
        std::vector<ExecutableAction::Unimanual> unimanuals;
        for (const auto& um: dto.unimanual)
        {
            std::optional<armarx::FramedPose> execPose;
            std::optional<armarx::FramedPose> prePose;
            std::optional<armarx::FramedPose> retractPose;
            std::optional<HandTrajectory> trajectory;
            std::optional<ActionHypothesis> hypothesis;
            std::optional<arondto::Handedness> handedness;
            if (um.actionHypothesis)
            {
                hypothesis = fromAron(um.actionHypothesis.value());
            }
            if (um.execPose)
            {
                armarx::FramedPose tempPose;
                fromAron(um.execPose.value(), tempPose);
                execPose = tempPose;
            }
            if (um.prePose)
            {
                armarx::FramedPose tempPose;
                fromAron(um.prePose.value(), tempPose);
                prePose = tempPose;
            }
            if (um.retractPose)
            {
                armarx::FramedPose tempPose;
                fromAron(um.retractPose.value(), tempPose);
                retractPose = tempPose;
            }
            if (um.actionTrajectory.has_value())
            {
                trajectory = fromAron(um.actionTrajectory.value());
            }
            if (um.handedness.has_value())
            {
                handedness = um.handedness.value();
            }
            // TODO: this shold work under c++20
            //            unimanuals.emplace_back(hypothesis, prePose, retractPose, trajectory);
            unimanuals.push_back(
                    {.hypothesis=hypothesis, .execPose=execPose, .prePose=prePose, .retractPose=retractPose, .actionTrajectory=trajectory, .handedness=handedness});
        }

        return {unimanuals,
                {.platformPose=platformPose, .platformPlanningStrategy=dto.common.platformPlanningStrategy}};
    }

    arondto::SituatedAffordance toAron(const SituatedAffordance& bo)
    {
        arondto::SituatedAffordance dto;
        toAron(dto, bo);
        return dto;
    }

    SituatedAffordance fromAron(const arondto::SituatedAffordance& dto)
    {
        return {dto.frame, fromAron(dto.affordance), dto.keyPoint, dto.pointCloud};
    }
    //    }

    void toAron(arondto::Affordance& dto, const Affordance& bo)
    {
        dto.actionType = bo.affordance_type->getID();
        toAron(dto.robotDescription, bo.getRobotDescription());
    }

    void fromAron(const arondto::Affordance& dto, Affordance& bo)
    {
        armem::robot::RobotDescription id;
        fromAron(dto.robotDescription, id);
        // TODO: how to construct bo from dto without connection to memory?
        bo = Affordance(dto.actionType, id);
    }

    void toAron(arondto::FramedUncertainPose& dto, const FramedUncertainPose& bo)
    {
        dto.header.frame = bo.pose->getFrame();
        dto.header.agent = bo.pose->agent;
        dto.uncertainPose.pose = bo.pose->toEigen();
        dto.uncertainPose.covariance = bo.covariance;
    }

    void fromAron(const arondto::FramedUncertainPose& dto, FramedUncertainPose& bo)
    {
        bo = fromAron(dto);
    }

    void toAron(arondto::ActionHypothesis& dto, const ActionHypothesis& bo)
    {
        toAron(dto.affordance, *bo.affordance);
        toAron(dto.framedPose, *bo.framedPose);
        dto.existenceCertainty = bo.existenceCertainty;
        dto.observationTime = bo.observationTime;
        dto.handedness = bo.handedness;
    }

    void fromAron(const arondto::ActionHypothesis& dto, ActionHypothesis& bo)
    {
        bo = fromAron(dto);
    }

    void toAron(arondto::ExecutableAction& dto, const ExecutableAction& bo)
    {
        for (const auto& um: bo.unimanual)
        {
            auto& dto_um = dto.unimanual.emplace_back();
            if (um.hypothesis.has_value())
            {
                dto_um.actionHypothesis = toAron(um.hypothesis.value());
            }
            dto_um.execPose = um.execPose;
            dto_um.prePose = um.prePose;
            dto_um.retractPose = um.retractPose;
            if (um.actionTrajectory.has_value())
            {
                dto_um.actionTrajectory = toAron(um.actionTrajectory.value());
            }
            dto_um.handedness = um.handedness;
        }

        dto.common.platformPose = bo.common.platformPose;
        dto.common.platformPlanningStrategy = bo.common.platformPlanningStrategy;


    }

    void fromAron(const arondto::ExecutableAction& dto, ExecutableAction& bo)
    {
        bo = fromAron(dto);
    }

    void toAron(arondto::SituatedAffordance& dto, const SituatedAffordance& bo)
    {
        dto.frame = bo.frame;
        dto.pointCloud = bo.pointCloud;
        if (bo.keyPoint)
        {
            dto.keyPoint = bo.keyPoint.value().toEigen();
        }
        toAron(dto.affordance, bo.affordance);
    }

    void fromAron(const arondto::SituatedAffordance& dto, SituatedAffordance& bo)
    {
        bo = fromAron(dto);
    }

    arondto::ExecutedAction toAron(const ExecutedAction& bo)
    {
        arondto::ExecutedAction exAction;
        toAron(exAction, bo);
        return exAction;
    }

    void toAron(arondto::ExecutedAction& dto, const ExecutedAction& bo)
    {
        dto.action = toAron(*bo.action);
        dto.startTime = *bo.startTime;
        if (bo.endTime)
        {
            // TODO: we don't want the case where this is a nullptr. It should always be defined
            dto.endTime = *bo.endTime;
        }
        dto.success = bo.success;
        dto.outcome = bo.outcome;
        dto.comment = bo.comment;
        dto.debugOutput = bo.debugOutput;
    }

    ExecutedAction fromAron(const arondto::ExecutedAction& dto)
    {
        return {fromAron(dto.action), dto.startTime, dto.endTime, dto.success, dto.outcome, dto.comment,
                dto.debugOutput};
    }

    void fromAron(const arondto::ExecutedAction& dto, ExecutedAction& bo)
    {
        bo = fromAron(dto);
    }

    arondto::HandTrajectoryKeypoint toAron(const HandTrajectoryKeypoint& bo)
    {
        auto kp = arondto::HandTrajectoryKeypoint();
        toAron(kp, bo);
        return kp;
    }

    void toAron(arondto::HandTrajectoryKeypoint& dto, const HandTrajectoryKeypoint& bo)
    {
        dto.pose = bo.pose.toEigen();
        dto.frame.frame = bo.pose.getFrame();
        dto.frame.agent = bo.pose.agent;
        dto.fingerValues = bo.fingerValues;
    }

    HandTrajectoryKeypoint fromAron(const arondto::HandTrajectoryKeypoint& dto)
    {
        return {dto.pose, dto.frame.frame, dto.fingerValues};
    }

    void fromAron(const arondto::HandTrajectoryKeypoint& dto, HandTrajectoryKeypoint& bo)
    {
        bo = fromAron(dto);
    }

    arondto::HandTrajectory toAron(const HandTrajectory& bo)
    {
        auto dto = arondto::HandTrajectory();
        toAron(dto, bo);
        return dto;
    }

    void toAron(arondto::HandTrajectory& dto, const HandTrajectory& bo)
    {
        std::transform(bo.keypoints.begin(), bo.keypoints.end(), std::back_inserter(dto.keypoints),
                       [](const auto& kp) -> arondto::HandTrajectoryKeypoint
                       {
                           return toAron(kp);
                       });
        dto.duration = bo.duration;
    }

    HandTrajectory fromAron(const arondto::HandTrajectory& dto)
    {
        auto traj = HandTrajectory();
        traj.duration = dto.duration;
        std::transform(dto.keypoints.begin(), dto.keypoints.end(), std::back_inserter(traj.keypoints),
                       [](const auto& kp) -> HandTrajectoryKeypoint
                       {
                           return fromAron(kp);
                       });
        return traj;
    }

    void fromAron(const arondto::HandTrajectory& dto, HandTrajectory& bo)
    {
        bo = fromAron(dto);
    }


    void fromAron(const arondto::Arm& dto, Arm& bo)
    {
        switch (dto.value)
        {
            case arondto::Arm::Left:
                bo = Arm::Left;
                break;
            case arondto::Arm::Right:
                bo = Arm::Right;
                break;
        }
    }

    void fromAron(const arondto::Hand& dto, Hand& bo)
    {
        switch (dto.value)
        {
            case arondto::Hand::Left:
                bo = Hand::Left;
                break;
            case arondto::Hand::Right:
                bo = Hand::Right;
                break;
        }
    }

    arondto::ManipulationProcess toAron(const ManipulationProcess& bo)
    {
        auto dto = arondto::ManipulationProcess();
        toAron(dto, bo);
        return dto;
    }

    void toAron(arondto::ManipulationProcess& dto, const ManipulationProcess& bo)
    {
        dto.actionHypotheses = meta::transform_container(bo.actionHypotheses, [](const auto& el)
        {
            return toAron(el);
        });
        dto.executedAction = toAron(*bo.executedAction);
        dto.pointCloud = bo.pointCloud;
    }

    ManipulationProcess fromAron(const arondto::ManipulationProcess& dto)
    {
        std::optional<armarx::manipulation::core::ExecutedAction> action;
        if (dto.executedAction.has_value()) action = fromAron(dto.executedAction.value());
        return ManipulationProcess{.actionHypotheses=meta::transform_container(dto.actionHypotheses, [](const auto& el)
        {
            return fromAron(el);
        }), .executedAction=action, .pointCloud=dto.pointCloud};
    }

    void fromAron(const arondto::ManipulationProcess& dto, ManipulationProcess& bo)
    {
        bo = fromAron(dto);
    }
}
