/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_manipulation
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       09.12.21
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <memory>
#include <optional>
#include <RobotAPI/libraries/core/FramedPose.h>
#include <armarx/manipulation/core/HandTrajectory.h>
#include <armarx/manipulation/core/ActionHypothesis.h>

namespace armarx::manipulation::core
{
    struct ExecutableAction
    {
    public:
        struct Common
        {
            std::optional<armarx::FramedPose> platformPose;
            std::optional<std::string> platformPlanningStrategy;
        };
        struct Unimanual
        {
            // Hypothesis only holds information about what was extracted, which should not be changed anymore
            std::optional<const ActionHypothesis> hypothesis = std::nullopt;
            std::optional<armarx::FramedPose> execPose = std::nullopt;
            std::optional<armarx::FramedPose> prePose = std::nullopt;
            std::optional<armarx::FramedPose> retractPose = std::nullopt;
            std::optional<HandTrajectory> actionTrajectory = std::nullopt;
            std::optional<arondto::Handedness> handedness = std::nullopt;

            Unimanual& operator=(const Unimanual&);
        };

        ExecutableAction(std::vector<Unimanual> eefSpecific, Common shared);
        ExecutableAction(const Unimanual& eefSpecific, Common shared);

        Common common;
        std::vector<Unimanual> unimanual;
    };
}