/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_manipulation
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       02.12.21
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <utility>
#include <optional>
#include <armarx/manipulation/core/aron/ActionHypothesis.aron.generated.h>

namespace Eigen {
    using Matrix6f = Eigen::Matrix<::Ice::Float,  6,  6>;
}
namespace armarx
{
    class FramedPose;
    namespace armem::robot
    {
        struct RobotDescription;
    }
    namespace manipulation::core
    {
        class Affordance;

        struct FramedUncertainPose;

        class ActionHypothesis
        {
        public:
            // TODO: what about the robotDescriptionID
            ActionHypothesis(const std::string& actionType, const armem::robot::RobotDescription& robotDescription,
                             const FramedPose& pose, long obsTime,
                             std::optional<Eigen::Matrix6f> poseUncertainty = std::nullopt,
                             std::optional<arondto::Handedness> handed = std::nullopt,
                             std::optional<float> existCert = std::nullopt);

            ActionHypothesis(const Affordance& aff, const FramedUncertainPose& pose, long obsTime,
                             std::optional<arondto::Handedness> handed = std::nullopt,
                             std::optional<float> existCert = std::nullopt);
            ActionHypothesis(const ActionHypothesis&);
            ActionHypothesis& operator=(const ActionHypothesis&);
            ActionHypothesis(ActionHypothesis&&);
            ActionHypothesis& operator=(ActionHypothesis&&) ;
            ~ActionHypothesis();

            std::unique_ptr<Affordance> affordance;
            std::unique_ptr<FramedUncertainPose> framedPose;
            std::optional<arondto::Handedness> handedness;
            std::optional<float> existenceCertainty;

            std::string getTCPName() const;
            std::string getKinematicChainName() const;
            std::string getHandRootNode() const;

            std::string getAffordanceID() const;
            arondto::KnownAffordances getAffordanceType() const;

            std::string getRobotFilePath() const;

            armarx::FramedPose getExecutionPose() const;


            long observationTime;
        };
    }

}

