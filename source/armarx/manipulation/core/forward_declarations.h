/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_manipulation
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       10.01.22
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

namespace armarx
{
    class FramedPose;
}

namespace armarx::manipulation::core
{
    class Affordance;
    struct FramedUncertainPose;
    class ActionHypothesis;
    struct ExecutableAction;
    class ExecutedAction;
    struct HandTrajectoryKeypoint;
    struct HandTrajectory;
    class SituatedAffordance;
    struct ManipulationProcess;
    enum class Arm;
    enum class Hand;

    namespace arondto
    {
        class Affordance;
        class FramedUncertainPose;
        class ActionHypothesis;
        class ExecutableAction;
        class ExecutedAction;
        class Handedness;
        class HandTrajectoryKeypoint;
        class HandTrajectory;
        class SituatedAffordance;
        class Arm;
        class Hand;
        class ManipulationProcess;
    }
}
