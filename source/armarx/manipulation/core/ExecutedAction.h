/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_manipulation
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       08.02.22
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <memory>
#include <optional>

#include <RobotAPI/libraries/armem/core/forward_declarations.h>
#include <RobotAPI/libraries/armem/core/Time.h>


namespace armarx::manipulation::core
{

    struct ExecutableAction;
    namespace arondto
    {
        class ExecutedAction;
    }

    class ExecutedAction
    {
    public:
        ExecutedAction(const ExecutableAction& action, armarx::armem::Time starttime,
                       std::optional<armarx::armem::Time> endtime = std::nullopt,
                       std::optional<bool> success = std::nullopt, std::optional<std::string> outcome = std::nullopt,
                       std::optional<std::string> comment = std::nullopt,
                       std::optional<std::string> debugOutput = std::nullopt);

        ExecutedAction(const ExecutedAction&);

        ExecutedAction& operator=(const ExecutedAction&);

        ExecutedAction(ExecutedAction&&);

        ExecutedAction& operator=(ExecutedAction&&);

        ~ExecutedAction();

        std::unique_ptr<ExecutableAction> action;
        std::unique_ptr<armem::Time> startTime;
        std::unique_ptr<armem::Time> endTime;
        bool success;
        std::optional<std::string> outcome;
        std::optional<std::string> comment;
        std::optional<std::string> debugOutput;

        friend void toAron(arondto::ExecutedAction&, const ExecutedAction&);
    };
}
