
#include "CircleSegment.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/logging/Logging.h>


namespace armarx::manipulation::geometric_planning
{

    CircleSegment::CircleSegment(const float radius, const ParameterRange& range) :
        Circle(radius), range(range)
    {
        ARMARX_DEBUG << "Circle parameter range " << Circle::parameterRange().min << " "
                     << Circle::parameterRange().max;

        ARMARX_CHECK(Circle::parameterRange().isInRange(range.min));
        ARMARX_CHECK(Circle::parameterRange().isInRange(range.max));
    }

    ParameterRange
    CircleSegment::parameterRange() const
    {
        return range;
    }

} // namespace armarx::manipulation::geometric_planning
