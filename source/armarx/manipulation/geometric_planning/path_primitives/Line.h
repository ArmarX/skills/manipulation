#pragma once

#include <Eigen/Geometry>

#include <armarx/manipulation/core/types.h>

#include "PathPrimitive.h"

namespace armarx::manipulation::geometric_planning
{

    class Line : virtual public PathPrimitive
    {
    public:
        Line(const ParameterRange& parameterRange);

        Eigen::Vector3f getPosition(float t) const override;
        Eigen::Vector3f getPositionDerivative(float t) const override;
        Eigen::Quaternionf getOrientation(float t) const override;
        Eigen::Vector3f getOrientationDerivative(float t) const override;

        ParameterRange parameterRange() const override;

        float parameter(const Pose& pose) const override;

    private:
        ParameterRange range;
    };

} // namespace armarx::manipulation::geometric_planning
