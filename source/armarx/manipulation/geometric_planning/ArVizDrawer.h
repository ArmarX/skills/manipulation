#pragma once

#include <VirtualRobot/VirtualRobot.h>

#include "RobotAPI/components/ArViz/Client/Client.h"
#include <RobotAPI/components/ArViz/Client/ScopedClient.h>

#include <armarx/manipulation/core/types.h>

namespace armarx::viz
{
    struct Layer;
}

namespace armarx::manipulation::geometric_planning
{

    class Line;
    class CircleSegment;
    class ParametricPath;


    class ArVizDrawer
    {
    public:
        ArVizDrawer(armarx::viz::Client& arviz): arviz(arviz){}

        void drawPaths(const VirtualRobot::RobotPtr& articulatedObject,
                       const std::string& nodeIdentifier = "handle");

        // void draw(const std::vector<const geometric_planning::ParametricPath*>& paths);


    private:
        void draw(armarx::viz::Layer& layer, const geometric_planning::ParametricPath& path, const std::string& name);

        void draw(armarx::viz::Layer& layer,
                  const geometric_planning::Line& line,
                  const core::Pose& frame, const std::string& name);
        void draw(armarx::viz::Layer& layer,
                  const geometric_planning::CircleSegment& circleSegment,
                  const core::Pose& frame, const std::string& name);

        viz::Client& arviz;
    };


} // namespace armarx::manipulation::geometric_planning
