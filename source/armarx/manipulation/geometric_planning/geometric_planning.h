/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx::manipulation::ArmarXObjects::geometric_planning
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


namespace armarx::manipulation::geometric_planning
{
    /**
    * @defgroup Library-geometric_planning geometric_planning
    * @ingroup mobile_manipulation
    * A description of the library geometric_planning.
    *
    * @class geometric_planning
    * @ingroup Library-geometric_planning
    * @brief Brief description of class geometric_planning.
    *
    * Detailed description of class geometric_planning.
    */
    class geometric_planning
    {
    public:
    };

}  // namespace armarx::manipulation::geometric_planning
