/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <algorithm>
#include <vector>

#include <SimoxUtility/algorithm/string/string_tools.h>
namespace armarx::manipulation::geometric_planning
{

    inline std::vector<std::string>
    nodesMatching(const std::vector<std::string>& allNodeNames, const std::string& identifier)
    {

        std::vector<std::string> handleNodeNames;
        std::copy_if(allNodeNames.begin(),
                     allNodeNames.end(),
                     std::back_inserter(handleNodeNames),
                     [&identifier](const auto& nodeName)
                     {
                         // the second check is a bit hacky: we can define transformation nodes in URDF which might match, e.g. "parent->child"
                         return simox::alg::ends_with(nodeName, identifier) and
                                not simox::alg::contains(nodeName, '>');
                     });

        return handleNodeNames;
    }


} // namespace armarx::manipulation::geometric_planning
