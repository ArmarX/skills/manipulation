#include "ArVizDrawer.h"

#include <SimoxUtility/algorithm/string/string_tools.h>
#include <VirtualRobot/Robot.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/logging/Logging.h>

#include "RobotAPI/components/ArViz/Client/Elements.h"
#include "RobotAPI/components/ArViz/Client/elements/Color.h"
#include <RobotAPI/components/ArViz/Client/Layer.h>

#include "armarx/manipulation/geometric_planning/util.h"
#include <armarx/manipulation/geometric_planning/ArticulatedObjectGeometricPlanningHelper.h>
#include <armarx/manipulation/geometric_planning/ParametricPath.h>
#include <armarx/manipulation/geometric_planning/path_primitives/CircleSegment.h>
#include <armarx/manipulation/geometric_planning/path_primitives/Line.h>

namespace armarx::manipulation::geometric_planning
{
    void
    ArVizDrawer::drawPaths(const VirtualRobot::RobotPtr& articulatedObject,
                           const std::string& handleIdentifier)
    {
        ARMARX_VERBOSE << "Visualizing all paths for object " << articulatedObject->getType()
                       << " / " << articulatedObject->getName();


        const std::vector<std::string> handleNodeNames = nodesMatching(articulatedObject->getRobotNodeNames(), handleIdentifier);

        ARMARX_VERBOSE << "Found " << handleNodeNames.size() << " nodes matching `"
                       << handleIdentifier << "`.";
        for (const auto& handle : handleNodeNames)
        {
            ARMARX_VERBOSE << "- " << handle;
        }

        ArticulatedObjectGeometricPlanningHelper helper(articulatedObject);

        auto l = arviz.layer("parametric_paths__" + articulatedObject->getType() + "_" +
                             articulatedObject->getName() + "__" + handleIdentifier);

        for (const auto& handleNodeName : handleNodeNames)
        {
            const auto parametricPath = helper.getPathForNode(handleNodeName);
            draw(l, parametricPath, handleNodeName);
        }

        arviz.commit(l);
    }

    // void
    // ArVizDrawer::draw(const std::vector<const geometric_planning::ParametricPath*>& paths)
    // {
    //     auto l = arviz.layer("parametric_paths");

    //     std::for_each(paths.begin(),
    //                   paths.end(),
    //                   [&](const auto* path)
    //                   {
    //                       ARMARX_CHECK_NOT_NULL(path);
    //                       draw(l, *path);
    //                   });

    //     arviz.commit(l);
    // }

    std::vector<Eigen::Vector3f>
    to3D(const std::vector<Eigen::Vector2f>& points2d)
    {
        std::vector<Eigen::Vector3f> points3d;
        points3d.reserve(points2d.size());

        std::transform(points2d.begin(),
                       points2d.end(),
                       std::back_inserter(points3d),
                       [](const Eigen::Vector2f& pt) {
                           return Eigen::Vector3f{pt.x(), pt.y(), 0.F};
                       });

        return points3d;
    }


    void
    ArVizDrawer::draw(armarx::viz::Layer& layer,
                      const geometric_planning::Line& line,
                      const core::Pose& frame,
                      const std::string& name)
    {

        const Eigen::Vector3f from = frame * line.getPosition(line.parameterRange().min);
        const Eigen::Vector3f to = frame * line.getPosition(line.parameterRange().max);

        layer.add(viz::Sphere(name + "_from").scale(2).color(viz::Color::red()).position(from));
        layer.add(viz::Sphere(name + "_to").scale(2).color(viz::Color::green()).position(to));

        // Eigen::Vector3f normal =
        //     (core::Pose(virtualDishwasher->getGlobalPose()) * p.frame).translation();

        ARMARX_DEBUG << "from " << from;
        ARMARX_DEBUG << "to " << to;

        ARMARX_DEBUG << "Parameter range is " << line.parameterRange().min << ", "
                     << line.parameterRange().max;

        auto arrow = armarx::viz::Arrow(name + "_path" + std::to_string(layer.size()))
                         .fromTo(from, to)
                         .color(armarx::viz::Color::fromRGBA(0, 128, 255, 128));
        layer.add(arrow);

        // layer.add(viz::Sphere("line_from").position(from).radius(50).color(
        //               viz::Color::fromRGBA(0, 255, 0, 50)));
        // layer.add(viz::Sphere("line_to").position(to).radius(50).color(
        //               viz::Color::fromRGBA(255, 0, 0, 50)));
    }


    void
    ArVizDrawer::draw(armarx::viz::Layer& layer,
                      const geometric_planning::CircleSegment& circleSegment,
                      const core::Pose& frame,
                      const std::string& name)
    {

        const Eigen::Vector3f from =
            frame * circleSegment.getPosition(circleSegment.parameterRange().min);
        const Eigen::Vector3f to =
            frame * circleSegment.getPosition(circleSegment.parameterRange().max);

        layer.add(viz::Sphere(name + "_from").scale(2).color(viz::Color::red()).position(from));
        layer.add(viz::Sphere(name + "_to").scale(2).color(viz::Color::green()).position(to));

        ARMARX_DEBUG << "from " << from;
        ARMARX_DEBUG << "to " << to;

        ARMARX_DEBUG << "Parameter range is " << circleSegment.parameterRange().min << ", "
                     << circleSegment.parameterRange().max;

        // auto polygon = viz::Arrow("path" + std::to_string(++ii))
        //                    .fromTo(from, to)
        //                    .color(viz::Color::fromRGBA(0, 128, 255, 128));

        const float completion =
            (circleSegment.parameterRange().max - circleSegment.parameterRange().min) / (2 * M_PI);

        auto arrow = armarx::viz::ArrowCircle(name + "_path" + std::to_string(layer.size()))
                         .pose(frame)
                         .radius(circleSegment.getRadius())
                         .completion(completion)
                         .color(armarx::viz::Color::fromRGBA(0, 128, 255, 128));

        // layer.add(viz::Sphere("circle_from").position(from).radius(50).color(
        //               viz::Color::fromRGBA(0, 255, 0, 50)));
        // layer.add(
        //     viz::Sphere("circle_to").position(to).radius(50).color(viz::Color::fromRGBA(255, 0, 0, 50)));

        layer.add(arrow);
    }


    void
    ArVizDrawer::draw(armarx::viz::Layer& layer,
                      const geometric_planning::ParametricPath& path,
                      const std::string& name)
    {
        const auto* circleSegment =
            dynamic_cast<geometric_planning::CircleSegment*>(path.path.get());

        layer.add(viz::Sphere(name + "_path_frame")
                      .scale(4)
                      .color(viz::Color::blue())
                      .position((core::Pose(path.frame->getGlobalPose())).translation()));
        layer.add(viz::Sphere(name + "_pos0")
                      .scale(4)
                      .color(viz::Color::magenta())
                      .position(path.getPosition(0)));


        if (circleSegment != nullptr)
        {
            ARMARX_INFO << "Circle";
            draw(layer, *circleSegment, core::Pose(path.frame->getGlobalPose()), name);
            return;
        }

        const auto* line = dynamic_cast<geometric_planning::Line*>(path.path.get());
        if (line != nullptr)
        {
            ARMARX_INFO << "Line";

            // ARMARX_DEBUG << "global root pose" << path.global_T_root.matrix();
            // ARMARX_DEBUG << "frame" << path.root_T_frame.matrix();
            draw(layer, *line, core::Pose(path.frame->getGlobalPose()), name);
            return;
        }

        ARMARX_WARNING << "Unknown PathPrimitive!";
    }


} // namespace armarx::manipulation::geometric_planning
