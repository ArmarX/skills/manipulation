/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_manipulation
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       07.07.22
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 *
 *
 * from: https://stackoverflow.com/questions/23871757/the-stdtransform-like-function-that-returns-transformed-container
 */

#pragma once

#include <type_traits>
#include <algorithm>

namespace armarx::manipulation::meta
{
    template <typename K, typename ...>
    struct replace_type
    {
        using type = K;
    };

    template <typename T, typename U>
    struct replace_type<T, T, U>
    {
        using type = U;
    };

    template <template <typename... Ks> class K, typename T, typename U, typename... Ks>
    struct replace_type<K<Ks...>, T, U>
    {
        using type = K<typename replace_type<Ks, T, U>::type ...>;
    };

    template <template <typename T, typename... Ts> class Container, typename Functor, typename T,
                                                    typename U = typename std::result_of<Functor(T)>::type,
                                                    typename... Ts,
                                                    typename Result = typename replace_type<Container<T, Ts...>, T,
                                                                                            U>::type>
    Result transform_container(const Container<T, Ts...>& c, Functor&& f)
    {
        Result ret;
        std::transform(std::begin(c), std::end(c), std::inserter(ret, std::end(ret)), f);
        return ret;
    }
}