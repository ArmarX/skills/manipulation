/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_manipulation
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       24.02.22
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
// from https://stackoverflow.com/questions/7728478/c-template-class-function-with-arbitrary-container-type-how-to-define-it
#pragma once

#include <type_traits>

namespace armarx::manipulation::meta
{
    template<typename T>
    struct has_const_iterator
    {
    private:
        typedef char                      yes;
        typedef struct { char array[2]; } no;

        template<typename C> static constexpr yes test(typename C::const_iterator*);
        template<typename C> static constexpr no  test(...);
    public:
        static constexpr bool value = sizeof(test<T>(0)) == sizeof(yes);
        typedef T type;
    };

    template <typename T>
    struct has_begin_end
    {
        template<typename C> static char (&f(typename std::enable_if<
                std::is_same<decltype(static_cast<typename C::const_iterator (C::*)() const>(&C::begin)),
                typename C::const_iterator(C::*)() const>::value, void>::type*))[1];

        template<typename C> static constexpr char (&f(...))[2];

        template<typename C> static constexpr char (&g(typename std::enable_if<
                std::is_same<decltype(static_cast<typename C::const_iterator (C::*)() const>(&C::end)),
                typename C::const_iterator(C::*)() const>::value, void>::type*))[1];

        template<typename C> static constexpr char (&g(...))[2];

        static bool constexpr beg_value = sizeof(f<T>(0)) == 1;
        static bool constexpr end_value = sizeof(g<T>(0)) == 1;
    };

    template<typename T>
    struct is_stl_container : std::integral_constant<bool, has_const_iterator<T>::value && has_begin_end<T>::beg_value && has_begin_end<T>::end_value>
    { };
}