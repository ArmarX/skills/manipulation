#include "NJointTaskSpaceImpedanceControllerVelocityLimited.h"

#include <thread>

#include <VirtualRobot/Nodes/RobotNode.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/logging/Logging.h>

namespace armarx::manipulation::control
{

    NJointTaskSpaceImpedanceControllerVelocityLimited::
        NJointTaskSpaceImpedanceControllerVelocityLimited(
            const armarx::NJointTaskSpaceImpedanceControlInterfacePrx& impedanceController,
            const VirtualRobot::RobotNodePtr& tcp,
            const Params& params) :
        impedanceController(impedanceController), tcp(tcp), params(params)
    {
        ARMARX_CHECK_GREATER(params.posDiffEps, 0);
        ARMARX_CHECK_GREATER(params.linearVelocity, 0);
        ARMARX_CHECK_GREATER(params.updateRateMS, 0);

        ARMARX_CHECK_NOT_NULL(tcp);
        ARMARX_CHECK_NOT_NULL(impedanceController);
    }

    void
    NJointTaskSpaceImpedanceControllerVelocityLimited::setTarget(const core::Position& target)
    {
        std::lock_guard g{mtx};

        this->robot_P_tcp_target = target;
        lastUpdate = armarx::DateTime::Now();
    }

    

    bool
    NJointTaskSpaceImpedanceControllerVelocityLimited::targetReached() const
    {
        std::lock_guard g{mtx};

        const core::Pose robot_T_tcp_current(tcp->getPoseInRootFrame());

        const Eigen::Vector3f posDiff = robot_P_tcp_target - robot_T_tcp_current.translation();

        const bool reached = posDiff.norm() < params.posDiffEps;

        ARMARX_INFO << "Reached: " << reached << ", distance " << posDiff.norm();

        return reached;
    }

    void
    NJointTaskSpaceImpedanceControllerVelocityLimited::run(const std::atomic_bool& stop)
    {
        while (not targetReached() and not stop.load())
        {
            const core::Pose robot_T_tcp_current(tcp->getPoseInRootFrame());

            // thread-safe update of desired movement
            const Eigen::Vector3f tcp_current_V_target = [&]()
            {
                std::lock_guard g{mtx};
                return Eigen::Vector3f(robot_P_tcp_target - robot_T_tcp_current.translation());
            }();

            const float dt = params.updateRateMS / 10.F; // [s] // FIXME 1000

            // saturize pos diff
            const float posDiffClamped =
                std::clamp(tcp_current_V_target.norm(), params.minGoalDistance, params.linearVelocity * dt); // [mm]

            const core::Position robot_P_current_tcp_target =
                robot_T_tcp_current.translation() +
                posDiffClamped * tcp_current_V_target.normalized();

            impedanceController->setPosition(robot_P_current_tcp_target);
            //impedanceController->setPosition(robot_P_tcp_target);


            std::this_thread::sleep_for(std::chrono::milliseconds(params.updateRateMS));
        }

        ARMARX_INFO << "Target reached.";
    }


} // namespace armarx::manipulation::control
