#include "TrajectoryController.h"

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <VirtualRobot/Nodes/RobotNode.h>

#include <ArmarXCore/core/logging/Logging.h>

#include <armarx/manipulation/core/types.h>

namespace armarx::manipulation::control
{
    TrajectoryController::ControlData
    TrajectoryController::calculate()
    {

        // the position of the door handle in the robot's frame
        // => add this as a pose relative to the robot's tcp
        //  => new tcp

        const float param = path->parameter(core::Pose(tcp->getGlobalPose()));

        ARMARX_DEBUG << "Param" << param;

        // projection onto the trajectory / path
        const core::Pose global_T_path_proj = path->getPose(param);

        Eigen::Quaternionf global_R_tcp(tcp->getGlobalOrientation());

        const core::Pose path_proj_T_global = global_T_path_proj.inverse();
        const Eigen::Matrix3f path_proj_R_global = path_proj_T_global.linear();

        const Eigen::Matrix3f oriDir = global_T_path_proj.linear() * global_R_tcp.inverse();
        Eigen::AngleAxisf aa(oriDir);
        const Eigen::Vector3f oriDiff = aa.axis() * aa.angle();

        return ControlData{
            .global_T_frame = global_T_path_proj,
            .global_T_path = core::Pose(path->frame->getGlobalPose()),

            // all relative to pathFrame

            .desiredVelocityDirection = path_proj_R_global * path->getPositionDerivative(param),
            .desiredVelocityOrientation =
                path_proj_R_global * path->getOrientationDerivative(param),

            .positionDiff =
                path_proj_R_global * (tcp->getGlobalPosition() - path_proj_T_global.translation()),
            .orientationDiff = oriDiff,
            .param = param};

        // // position controller erwartet ParametricPath im roboter-frame!!!

        // Eigen::Vector3f current_position = tcp->getPositionInRootFrame();
        // stateT = std::atan2(-(current_position - circle->reference).y(), (circle->radius - (current_position - circle->reference).x())) * circle->T * 2 / circle->Pi;

        // math::Helpers::Position(goal) = circle->GetPosition(stateT);
        // posHelper.setTarget(goal);

        // //tangent_velocity = pos_controller.calculate(motion, state, VirtualRobot::IKSolver::CartesianSelection::Position);
        // tangent_velocity = motion->GetPositionDerivative(stateT);
        // if (stateT < circle->T && stateT >= -0.5)
        // {
        //     velocity = pos_controller.calculate(motion, stateT, VirtualRobot::IKSolver::CartesianSelection::Position);
        // }
        // else
        // {
        //     velocity = Eigen::Vector3f(0.0f, 0.0f, 0.0f);
        // }
        // Eigen::Vector3f velOri(0.0f, 0.0f, 0.0f);


        // Eigen::Vector3f force_tan = tangent_velocity.normalized() * tangent_velocity.normalized().dot(force);
        // Eigen::Vector3f force_rad = force - force_tan;
        // vel_rad = force_rad * state.compliance;

        // posHelper.setFeedForwardVelocity(velocity + vel_rad, velOri); //

        // robotLayer.add(viz::Sphere("current_goal")
        //                .pose(goal)
        //                .radius(40.0f)
        //                .colorGlasbeyLUT(4));
        // robotViz.joints(rns->getJointValueMap());
        // arviz.commit(robotLayer);

        // const Eigen::Vector3f robot_V_tcp_handle = approachDirection();
        // // ARMARX_INFO << "Direction is" << robot_V_tcp_handle;

        // Eigen::Vector6f velocity;
        // velocity << robot_V_tcp_handle.x(), robot_V_tcp_handle.y(), robot_V_tcp_handle.z(), 0, 0, 0;
    }

} // namespace armarx::manipulation::control
