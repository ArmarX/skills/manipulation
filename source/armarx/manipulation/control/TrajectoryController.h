#pragma once

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <VirtualRobot/VirtualRobot.h>

#include <armarx/manipulation/core/types.h>
#include <armarx/manipulation/geometric_planning/ParametricPath.h>

namespace armarx::manipulation::control
{

    class TrajectoryController
    {
    public:
        TrajectoryController(const VirtualRobot::RobotNodePtr& tcp,
                             const geometric_planning::ParametricPathPtr& path) :
            tcp(tcp), path(path)
        {
        }

        struct ControlData
        {
            core::Pose global_T_frame;
            core::Pose global_T_path;

            // all relative to pathFrame
            Eigen::Vector3f desiredVelocityDirection;
            Eigen::Vector3f desiredVelocityOrientation;

            Eigen::Vector3f positionDiff;
            Eigen::Vector3f orientationDiff;

            float param;
        };

        ControlData calculate();

    private:
        VirtualRobot::RobotNodePtr tcp;
        geometric_planning::ParametricPathPtr path;
    };

} // namespace armarx::manipulation::control
