#pragma once

#include <mutex>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <VirtualRobot/VirtualRobot.h>

#include <RobotAPI/interface/units/RobotUnit/TaskSpaceActiveImpedanceControl.h>
#include <RobotAPI/libraries/armem/core/Time.h>

#include <armarx/manipulation/core/types.h>

namespace armarx::manipulation::control
{


    class NJointTaskSpaceImpedanceControllerVelocityLimited
    {
    public:
        struct Params
        {
            // threshold to check if target is reached
            float posDiffEps; // [mm]

            // max linear velocity
            float linearVelocity; // [mm/s]

            // update rate of the controller
            int64_t updateRateMS; // [ms]

            // experimental. ask Fabian R.
            float minGoalDistance = 0; // [mm]

        };

        NJointTaskSpaceImpedanceControllerVelocityLimited(
            const armarx::NJointTaskSpaceImpedanceControlInterfacePrx& impedanceController,
            const VirtualRobot::RobotNodePtr& tcp,
            const Params& params);

        

        void setTarget(const core::Position& target);

        bool targetReached() const;

        void run(const std::atomic_bool& stop = false);

    private:
        armarx::NJointTaskSpaceImpedanceControlInterfacePrx impedanceController;

        mutable std::mutex mtx;
        core::Position robot_P_tcp_target;

        VirtualRobot::RobotNodePtr tcp;

        armarx::armem::Time lastUpdate;

        const Params params;
    };

} // namespace armarx::manipulation::control
